/*by zhuhy*/
summerready = function() {
	initPage();
}
//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}
};

//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}

var vue = null;

function initPage() {
	vue = new Vue({
		el : '#index',
		data : {
			popupVisibleAmount : false,
			popupVisibleDayAmount : false,
			listDatas : [],
			timeOutEvent : 0,
			module_id : summer.pageParam.module_id,
			record_id : summer.pageParam.record_id,
			category : eval(summer.pageParam.category),
			selectedSubCate : 0,
			amount : "",
			dayAmount : "",
			amounts : [{
				values : [{
					"text" : "1"
				}, {
					"text" : "2"
				}]
			}],
			dayAmounts : [{
				values : [{
					"text" : "1"
				}, {
					"text" : "2"
				}, {
					"text" : "3"
				}]
			}],
		},
		methods : {
			//初始化数据库
			/*子表: inspectcategory
			 pk_row_id: 主键 (类型ID)
			 pk_father_row_id: 父主键(上个界面传的record_id)
			 sub_cate_id: 联合主键(获取当前子类型ID)*/
			initDB : function() {
				var sql = "create table if not exists inspectcategory (pk_row_id INTEGER,pk_father_row_id INTEGER,sub_cate_id INTEGER,amount INTEGER,day_amount INTEGER,fill_completion INTEGER,PRIMARY KEY (pk_row_id,pk_father_row_id,sub_cate_id))"
				var param = {
					"db" : 'hzlqzw.db',
					"sql" : sql
				};
				summer.UMSqlite.execSql(param);

				//插入数据
				var sql1 = "insert or ignore into inspectcategory(pk_row_id,pk_father_row_id,sub_cate_id,fill_completion) values('" + this.category["value"] + "','" + this.record_id + "','" + this.selectedSubCate + "','0');"
				var param1 = {
					"db" : 'hzlqzw.db',
					"sql" : sql1
				};
				summer.UMSqlite.execSql(param1);
			},
			// picker view 滚动时触发
			onValueChange : function(picker, value) {
			},
			confirmChangeAmount : function() {
				this.amount = this.$refs.amountPicker.getValues()[0]['text'];
				this.popupVisibleAmount = false;
			},
			confirmChangeDayAmount : function() {
				this.dayAmount = this.$refs.dayAmountPicker.getValues()[0]['text'];
				this.popupVisibleDayAmount = false;
			},
			//初始化表单
			fillPage : function() {
				var sql = "select * from inspectcategory where pk_row_id='" + this.category["value"] + "' and pk_father_row_id='" + this.record_id + "' and sub_cate_id='" + this.selectedSubCate + "'";
				var param = {
					"db" : 'hzlqzw.db', //数据库名称
					"sql" : sql, //查询条件
					"startIndex" : 0, //可选参数 起始记录索引号(含)
				}
				var list = summer.UMSqlite.query(param);
				var list1 = eval(list);

				if (list1 != null) {
					var obj = list1[0];
					if (!objIsEmpty(obj["amount"]))
						this.amount = obj["amount"];
					if (!objIsEmpty(obj["day_amount"]))
						this.dayAmount = obj["day_amount"];
				}
			},
			//查询已有的信息
			dbQuery : function() {
				var sql = "select pk_row_id,detail_description,fill_completion from inspectdetail where pk_father_row_id='" + this.category["value"] + "' and pk_grandfather_row_id='" + this.record_id + "' and sub_cate_id='" + this.selectedSubCate + "'";
				var param = {
					"db" : 'hzlqzw.db', //数据库名称
					"sql" : sql, //查询条件
					"startIndex" : 0, //可选参数 起始记录索引号(含)
				}
				var list = summer.UMSqlite.query(param);
				var jsonArray = eval(list);
				//jsonArray分析处理
				if (jsonArray != null && jsonArray != undefined && jsonArray.length != 0) {
					var myDate = new Date();
					var mytime = myDate.toLocaleDateString();
					mytime = mytime.toString().trim().replace(/\//g, "");
					$.each(jsonArray, function(i, obj) {
						//过滤此条信息如果过期
						if (((obj.pk_row_id).split("i"))[0] != mytime) {
							var sql = "delete from inspectdetail";
							var param = {
								"db" : 'hzlqzw.db',
								"sql" : sql
							};
							summer.UMSqlite.execSql(param);
							jsonArray.splice(i--, 1);
						}
					});
					this.listDatas = jsonArray;
				}
			},
			subCateClick : function(index) {
				var fillCompletion = (isEmpty(this.amount) ? 0 : 1) + (isEmpty(this.dayAmount) ? 0 : 1) + (this.listDatas.length > 0 ? 1 : 0);
				if (fillCompletion) {
					UM.confirm({
						title : "切换巡查类别",
						text : "建议先暂存当前已填写内容；否则，已填写内容将丢失。",
						btnText : ["取消", "直接切换"],
						overlay : true,
						duration : 2000,
						cancle : function() {
						},
						ok : function() {
							vue.selectedSubCate = index;
							vue.initDB();
							vue.amount = "";
							vue.dayAmount = "";
							vue.listDatas = [];
							vue.fillPage();
							vue.dbQuery();
						}
					});
				} else {
					vue.selectedSubCate = index;
					vue.initDB();
					vue.amount = "";
					vue.dayAmount = "";
					vue.listDatas = [];
					vue.fillPage();
					vue.dbQuery();
				}
			},
			//添加新的泵站操作信息
			addItem : function() {
				roads.openWin("maintain", "inspectRiverDetail", "inspectRiver/inspectRiverDetail/inspectRiverDetail.html", {
					"module_id" : this.module_id,
					"record_id" : this.record_id,
					"category_id" : this.category["value"],
					"category_title" : this.category["text"],
					"subcate_id" : this.selectedSubCate,
					"subcat_title" : this.category["subCate"][this.selectedSubCate],
					"realid" : "new"
				});
			},
			//已有的泵站操作信息点击修改事件
			itemClick : function(index) {
				var item = this.listDatas[index];
				roads.openWin("maintain", "inspectRiverDetail", "inspectRiver/inspectRiverDetail/inspectRiverDetail.html", {
					"module_id" : this.module_id,
					"record_id" : this.record_id,
					"category_id" : this.category["value"],
					"category_title" : this.category["text"],
					"subcate_id" : this.selectedSubCate,
					"subcat_title" : this.category["subCate"][this.selectedSubCate],
					"realid" : item["pk_row_id"]
				});
			},
			tapHold : function(index) {
				// 这里编辑长按列表项逻辑
				var ev = event || window.event;
				var touches = ev.targetTouches;
				if (touches.length > 1) {
					return;
				}
				this.timeOutEvent = setTimeout(function() {
					this.timeOutEvent = 0;
				}, 2000);
			},
			cancelTapHold : function() {
				// 取消长按
				clearTimeout(this.timeOutEvent);
				this.timeOutEvent = 0;
			},
			//已有的泵站操作信息滑动删除事件
			deleteItem : function(index) {
				var item = this.listDatas[index];
				UM.confirm({
					title : "确认删除",
					text : "确认要删除本条详情操作信息吗？删除后将无法撤销。",
					btnText : ["取消", "确定"],
					overlay : true,
					duration : 2000,
					cancle : function() {
					},
					ok : function() {
						var sql = "delete from inspectdetail where pk_row_id='" + item["pk_row_id"] + "' and pk_father_row_id='" + vue.category["value"] + "' and pk_grandfather_row_id='" + vue.record_id + "' and sub_cate_id='" + vue.selectedSubCate + "'";
						var param = {
							"db" : 'hzlqzw.db', //数据库名称
							"sql" : sql, //查询条件
							"startIndex" : 0, //可选参数 起始记录索引号(含)
						}
						summer.UMSqlite.execSql(param);
						vue.listDatas.pop();
					}
				});
			},
			// 暂存表单
			saveForm : function() {
				var fillCompletion = (isEmpty(this.amount) ? 0 : 1) + (isEmpty(this.dayAmount) ? 0 : 1) + (this.listDatas.length > 0 ? 1 : 0);
				if (fillCompletion) {
					var sql = null;
					fillCompletion = (fillCompletion == 3 ? 1 : 0);
					sql = "update inspectcategory set amount='" + this.amount + "',day_amount='" + this.dayAmount + "',fill_completion='" + fillCompletion + "' where pk_row_id='" + this.category["value"] + "' and pk_father_row_id='" + this.record_id + "' and sub_cate_id='" + this.selectedSubCate + "'";

					var param = {
						"db" : 'hzlqzw.db',
						"sql" : sql
					}
					summer.UMSqlite.execSql(param);
					roads.execScript("inspectRiver", "vue.backfrCategory", {
						category_id : vue.category["value"],
						sub_cate_id : vue.selectedSubCate,
						completion : fillCompletion
					});
					summer.closeWin();
				} else {
					UM.toast({
						title : '抱歉:',
						text : '没有填写任何的数据,无法执行操作。',
						duration : 3000
					});
				}
			},
			// 提交表单
			submitForm : function() {
				var self = this;
				var data = JSON.parse(JSON.stringify(self.$data));
				self.$toast({
					message : data,
					position : "center",
					duration : 3000
				});
			},
			// 返回
			goback : function() {
				roads.confirmClose();
			}
		},
		mounted : function() {
			this.$nextTick(function() {
				//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//初始化数据库
				this.initDB();
				//初始化报表
				this.fillPage();
				//查询数据库中现有的过船信息
				this.dbQuery();
			})
		}
	});
}