/*by zhuhy*/
summerready = function() {
	initPage();
}
//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}
};

//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}

var vue = null;

function initPage() {
	vue = new Vue({
		el : '#index',
		data : {
			listDatas : [],
			module_id : parseInt(summer.pageParam.module_id),
			record_id : parseInt(summer.pageParam.record_id),
			category_id : summer.pageParam.category_id,
			category_title : summer.pageParam.category_title,
			subcate_id : summer.pageParam.subcate_id,
			subcat_title : summer.pageParam.subcat_title,
			realid : summer.pageParam.realid,
			detailDescription : "",
			picNum : 0,
			picPaths : [],
		},
		methods : {
			//初始化数据库
			/*子表: inspectdetail
			 pk_row_id: 主键 (时间ID)
			 pk_father_row_id: 父主键(上个界面传的类型ID)
			 pk_grandfather_row_id: 祖父主键(上个界面传的record_id)
			 sub_cate_id: 联合主键(上个界面传的子类型ID)*/
			initDB : function() {
				var sql = "create table if not exists inspectdetail (pk_row_id VARCHAR PRIMARY KEY,pk_father_row_id INTEGER,pk_grandfather_row_id INTEGER,sub_cate_id INTEGER,detail_description VARCHAR,pic_num INTEGER,pic_paths VARCHAR,fill_completion INTEGER)"
				var param = {
					"db" : 'hzlqzw.db',
					"sql" : sql
				};
				summer.UMSqlite.execSql(param);
			},
			//前往图片总览页
			goPicCollection : function() {
				roads.openWin("util", "picCollection", "picCollection/picCollection.html", {
					ownerTag : 0,
					picPaths : this.picPaths,
					amountLimit : 1,
					module_id : this.module_id,
					module_title : "",
					record_id : this.record_id,
					win_id : "inspectRiverDetail"
				});
			},
			//从图片收集页返回
			backfrCollection : function(jsonStr) {
				this.picPaths = jsonStr["picPaths"];
				this.picNum = jsonStr["picPaths"].length;
			},
			//生成时间ID
			timeId : function() {
				var iter;
				var myDate = new Date();
				//生成时间戳并去除其中的斜杠
				var mytime = myDate.toLocaleDateString().toString().trim().replace(/\//g, "");
				if (isEmpty(summer.getStorage('inspect_r_iter')))
					iter = "001";
				else {
					//去零再累加
					iter = summer.getStorage('inspect_r_iter').replace(/\b(0+)/gi, "") + 1;
					var l = iter.toString().length;
					if (iter.length < 3) {
						//循环补零
						for (var i = 0; i < 3 - l; i++)
							iter = "0" + iter.toString().trim();
					}
				}
				//把累加器存回手机存储
				summer.setStorage('inspect_r_iter', iter);
				//'i'用来分隔时间戳和累加器
				return (mytime + 'i' + iter);
			},
			//获取真id判别是新增还是修改
			fillPage : function() {
				if (this.realid + "" != "new") {
					//更新之前的信息&原表单填充
					var sql = "select * from inspectdetail where pk_row_id='" + this.realid + "' and pk_father_row_id='" + this.category_id + "' and pk_grandfather_row_id='" + this.record_id + "' and sub_cate_id='" + this.subcate_id + "'";
					var param = {
						"db" : 'hzlqzw.db', //数据库名称
						"sql" : sql, //查询条件
						"startIndex" : 0, //可选参数 起始记录索引号(含)
					}

					var list = summer.UMSqlite.query(param);
					var list1 = eval(list);

					if (list1 != null && list1.length != 0) {
						var obj = list1[0];
						if (!objIsEmpty(obj["detail_description"]))
							this.detailDescription = obj["detail_description"];
						if (!objIsEmpty(obj["pic_num"]))
							this.picNum = obj["pic_num"];
						if (!objIsEmpty(obj["pic_paths"]))
							this.picPaths = eval(obj["pic_paths"]);
					}
				} else {
					//新增信息
				}
			},
			// 暂存表单
			saveForm : function() {
				//根据完成度来做暂存
				var fillCompletion = (isEmpty(this.detailDescription) ? 0 : 1) + (this.picNum > 0 ? 1 : 0);
				if (fillCompletion) {
					var sql = null;
					if (this.realid == "new") {
						//新增-->插入新数据
						//生成新的时间id作为真id
						sql = "insert into inspectdetail (pk_row_id,pk_father_row_id,pk_grandfather_row_id,sub_cate_id,detail_description,pic_num,pic_paths,fill_completion) values ('" + this.timeId() + "','" + this.category_id + "','" + this.record_id + "','" + this.subcate_id + "','" + this.detailDescription + "','" + this.picNum + "','" + JSON.stringify(this.picPaths) + "','" + (fillCompletion == 2 ? 1 : 0) + "')";
					} else {
						//修改-->更新老数据
						sql = "update inspectdetail set detail_description='" + this.detailDescription + "',pic_num='" + this.picNum + "',pic_paths='" + JSON.stringify(this.picPaths) + "',fill_completion='" + (fillCompletion == 2 ? 1 : 0) + "' where pk_row_id='" + this.realid + "' and pk_father_row_id='" + this.category_id + "' and pk_grandfather_row_id='" + this.record_id + "' and sub_cate_id='" + this.subcate_id + "'";
					}
					var param = {
						"db" : 'hzlqzw.db',
						"sql" : sql
					}
					summer.UMSqlite.execSql(param);
					roads.execScript("inspectRiverCategory", "vue.dbQuery", "");
					roads.closeWin();
				} else {
					UM.toast({
						title : '友情提示:',
						text : '没有任何填写的数据需保存',
						duration : 3000
					});
				}
			},
			// 返回
			goback : function() {
				roads.confirmClose();
			}
		},
		mounted : function() {
			this.$nextTick(function() {
				//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//初始化数据库
				this.initDB();
				//初始化报表
				this.fillPage();
			})
		}
	});
}