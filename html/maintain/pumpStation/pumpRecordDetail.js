/*by zhuhy*/
summerready = function() {
	initPage();
}
//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}
};

//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}

var vue = null;

function initPage() {
	vue = new Vue({
		el : '#index',
		data : {
			status : false,
			oppoStatus : null,
			operationTime : null,
			timePickerValue : null,
			startHour : 0,
			endHour : 0,
			meterRead : null,
			shift : parseInt(summer.pageParam.shift),
			pumpID : parseInt(summer.pageParam.pumpID),
			record_id : summer.pageParam.record_id
		},
		methods : {
			//根据当前班次加载时间选择器
			loadTimePicker : function() {
				switch(this.shift+"") {
				case '0':
					this.startHour = 0;
					this.endHour = 8;
					break;
				case '1':
					this.startHour = 9;
					this.endHour = 16;
					break;
				case '2':
					this.startHour = 17;
					this.endHour = 23;
					break;
				default:
					this.startHour = 0;
					this.endHour = 0;
					break;
				}
			},
			//获取真id判别是新增还是修改
			fillPage : function() {
				//更新之前的泵站操作信息&原表单填充
				var sql = "select * from pumprecord where pump_id='" + this.pumpID + "' and pk_father_row_id='" + this.record_id + "'";
				var param = {
					"db" : 'hzlqzw.db', //数据库名称
					"sql" : sql, //查询条件
					"startIndex" : 0, //可选参数 起始记录索引号(含)
				}

				var list = summer.UMSqlite.query(param);
				var list1 = eval(list);

				if (list1 != null && list1.length != 0) {
					this.status = ((list1[list1.length-1]["status"]) == 1) ? true : false;
				}
				this.oppoStatus = this.status ? false : true;
			},
			//生成时间ID
			timeId : function() {
				var iter;
				var myDate = new Date();
				//生成时间戳并去除其中的斜杠
				var mytime = myDate.toLocaleDateString().toString().trim().replace(/\//g, "");
				if (isEmpty(summer.getStorage('pump_iter')))
					iter = "001";
				else {
					//去零再累加
					iter = summer.getStorage('pump_iter').replace(/\b(0+)/gi, "") + 1;
					var l = iter.toString().length;
					if (iter.length < 3) {
						//循环补零
						for (var i = 0; i < 3 - l; i++)
							iter = "0" + iter.toString().trim();
					}
				}
				//把累加器存回手机存储
				summer.setStorage('pump_iter', iter);
				//'i'用来分隔时间戳和累加器
				return (mytime + 'i' + iter);
			},
			onValueChange : function(picker, value) {
			},
			//时间选择器确认方法
			handleTimeConfirm : function(value) {
				this.operationTime = value;
			},
			open : function(picker) {
				this.$refs[picker].open();
			},
			// 返回
			goback : function() {
				roads.confirmClose();
			},
			// 暂存表单
			saveForm : function() {
				//根据完成度来做暂存
				var fillCompletion = (isEmpty(this.operationTime) ? 0 : 1) + (isEmpty(this.meterRead) ? 0 : 1);
				if (fillCompletion) {
					var sql = "insert into pumprecord (pk_row_id,pk_father_row_id,status,operation_time,meter_read,pump_id,shift,fill_completion) values ('" + this.timeId() + "','" + this.record_id + "','" + (this.status ? 1 : 0) + "','" + this.operationTime + "','" + this.meterRead + "','" + this.pumpID + "','" + this.shift + "','" + (fillCompletion == 2 ? 1 : 0) + "')";
					var param = {
						"db" : 'hzlqzw.db',
						"sql" : sql
					}
					summer.UMSqlite.execSql(param);
					roads.execScript("pumpRecord", "vue.dbQuery", "");
					roads.closeWin();
				} else {
					UM.toast({
						title : '友情提示:',
						text : '没有任何填写的数据需保存',
						duration : 3000
					});
				}
			}
		},
		watch : {
			status : function(val) {
				if (val != this.oppoStatus) {
					this.operationTime = "";
					this.meterRead = "";
				}
			}
		},
		mounted : function() {
			// 加载数据...
			this.$nextTick(function() {
				//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//加载时间选择器
				this.loadTimePicker();
				//初始化报表
				this.fillPage();
			})
		}
	})
}