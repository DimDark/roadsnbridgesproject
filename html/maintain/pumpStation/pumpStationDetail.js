/*by zhuhy*/
summerready = function() {
	initPage();
}
//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}
};

//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}

var vue = null;

function initPage() {
	vue = new Vue({
		el : '#index',
		data : {
			upStream : "",
			downStream : "",
			well : "",
			pump1 : "",
			pump2 : "",
			pump3 : "",
			normalCheck : "",
			//时段标识
			shiftTime : null,
			shift : summer.pageParam.shift,
			record_id : summer.pageParam.record_id,
			module_id : summer.pageParam.module_id
		},
		methods : {
			//初始化数据库
			initDB : function() {
				if (this.record_id < 0) {
					summer.closeWin();
				}

				//建表
				var sql = "create table if not exists pumpstationdetail (pk_row_id INTEGER, pk_father_row_id INTEGER, up_stream INTEGER,down_stream INTEGER,well INTEGER,pump1 INTEGER,pump2 INTEGER,pump3 INTEGER,normal_check VARCHAR,fill_completion INTEGER,PRIMARY KEY (pk_row_id,pk_father_row_id))";
				var param = {
					"db" : 'hzlqzw.db',
					"sql" : sql
				}
				summer.UMSqlite.execSql(param);

				//插入数据
				var sql1 = "insert or ignore into pumpstationdetail(pk_row_id,pk_father_row_id,fill_completion) values('" + this.shiftTime + "','" + this.record_id + "','0');"
				var param1 = {
					"db" : 'hzlqzw.db',
					"sql" : sql1
				};
				summer.UMSqlite.execSql(param1);
			},
			//初始化页面
			fillPage : function() {
				//更新之前的过船信息&原表单填充
				var sql = "select * from pumpstationdetail where pk_row_id='" + this.shiftTime + "' and pk_father_row_id='" + this.record_id + "'";
				var param = {
					"db" : 'hzlqzw.db', //数据库名称
					"sql" : sql, //查询条件
					"startIndex" : 0, //可选参数 起始记录索引号(含)
				}
				var list = summer.UMSqlite.query(param);
				var list1 = eval(list);

				if (list1 != null && list1.length != 0) {
					var obj = list1[0];
					if (!objIsEmpty(obj["up_stream"])) {
						this.upStream = obj["up_stream"];
					}
					if (!objIsEmpty(obj["down_stream"])) {
						this.downStream = obj["down_stream"];
					}
					if (!objIsEmpty(obj["well"])) {
						this.well = obj["well"];
					}
					if (!objIsEmpty(obj["pump1"])) {
						this.pump1 = obj["pump1"];
					}
					if (!objIsEmpty(obj["pump2"])) {
						this.pump2 = obj["pump2"];
					}
					if (!objIsEmpty(obj["pump3"])) {
						this.pump3 = obj["pump3"];
					}
					if (!objIsEmpty(obj["normal_check"])) {
						this.normalCheck = obj["normal_check"];
					}
				}
			},
			initShiftTime : function() {
				switch(this.shift) {
				case(0):
					this.shiftTime = 0 + parseInt(summer.pageParam.j);
					break;
				case(1):
					this.shiftTime = 9 + parseInt(summer.pageParam.j);
					break;
				case(2):
					this.shiftTime = 17 + parseInt(summer.pageParam.j);
					break;
				default:
					break;
				}
			},
			// 暂存表单
			saveForm : function() {
				//根据完成度来做暂存
				var fillCompletion = (isEmpty(this.upStream) ? 0 : 1) + (isEmpty(this.downStream) ? 0 : 1) + (isEmpty(this.well) ? 0 : 1) + (isEmpty(this.pump1) ? 0 : 1) + (isEmpty(this.pump2) ? 0 : 1) + (isEmpty(this.pump3) ? 0 : 1) + (isEmpty(this.normalCheck) ? 0 : 1);
				if (fillCompletion) {
					fillCompletion = (this.shiftTime == 0 || this.shiftTime == 23) ? (fillCompletion == 7 ? 1 : 0) : (fillCompletion == 4 ? 1 : 0);
					var sql = "update pumpstationdetail set up_stream='" + this.upStream + "',down_stream='" + this.downStream + "',well='" + this.well + "',pump1='" + this.pump1 + "',pump2='" + this.pump2 + "',pump3='" + this.pump3 + "',normal_check='" + this.normalCheck + "',fill_completion='" + fillCompletion + "' where pk_row_id='" + this.shiftTime + "' and pk_father_row_id='" + this.record_id + "'";
					var param = {
						"db" : 'hzlqzw.db',
						"sql" : sql
					}
					summer.UMSqlite.execSql(param);
					roads.execScript("pumpStation", "vue.backfrDetail", {
						j : summer.pageParam.j,
						completion : fillCompletion
					});
					summer.closeWin();
				} else {
					UM.toast({
						title : '友情提示:',
						text : '没有任何填写的数据需保存',
						duration : 3000
					});
				}
			},
			// 返回
			goback : function() {
				roads.confirmClose();
			}
		},
		mounted : function() {
			// 加载数据...
			this.$nextTick(function() {//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//初始化时段
				this.initShiftTime();
				//初始化数据库
				this.initDB();
				//初始化表单
				this.fillPage();
			})
		}
	})
}

function percentcheck() {
	$("#down_precent").on("blur", function() {
		var val = $(this).val();
		if (val > 100) {
			document.getElementById("down_precent").value = "";
			UM.alert("百分比不能大于100");
		}

	});
	$("#up_precent").on("blur", function() {
		var val = $(this).val();
		if (val > 100) {
			document.getElementById("up_precent").value = "";
			UM.alert("百分比不能大于100");
		}
	})
}