/*by zhuhy*/
summerready = function() {
	initPage();
}
//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}
};

//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}

var vue = null;

function initPage() {
	vue = new Vue({
		el : '#index',
		data : {
			listDatas : [],
			contentDeletable : [{
				content : '删除',
				handler : function() {
					vue.deleteItem();
				}
			}],
			contentUndeletable : [{
				content : '不可删除',
				handler : function() {
					UM.toast({
						title : '友情提示:',
						text : '抱歉，只有最后的泵站操作信息可删除。',
						duration : 3000
					});
				}
			}],
			timeOutEvent : 0,
			shift : parseInt(summer.pageParam.shift),
			pumpID : parseInt(summer.pageParam.pumpID),
			record_id : summer.pageParam.record_id,
			module_id : summer.pageParam.module_id
		},
		methods : {
			//初始化数据库
			initDB : function() {
				var sql = "create table if not exists pumprecord (pk_row_id VARCHAR PRIMARY KEY,pk_father_row_id INTEGER,operation_time VARCHAR,status INTEGER,meter_read INTEGER,pump_id INTEGER,shift INTEGER,fill_completion INTEGER)"
				var param = {
					"db" : 'hzlqzw.db',
					"sql" : sql
				};
				summer.UMSqlite.execSql(param);
			},
			//查询已有的泵站操作信息
			dbQuery : function() {
				var sql = "select pk_row_id,operation_time,status,fill_completion from pumprecord where pk_father_row_id='" + this.record_id + "' and pump_id='" + this.pumpID + "'";
				var param = {
					"db" : 'hzlqzw.db', //数据库名称
					"sql" : sql, //查询条件
					"startIndex" : 0, //可选参数 起始记录索引号(含)
				}
				var list = summer.UMSqlite.query(param);
				var jsonArray = eval(list);
				//jsonArray分析处理
				if (jsonArray != null && jsonArray != undefined && jsonArray.length != 0) {
					var myDate = new Date();
					var mytime = myDate.toLocaleDateString();
					mytime = mytime.toString().trim().replace(/\//g, "");
					$.each(jsonArray, function(i, obj) {
						//过滤此条信息如果过期
						if (((obj.pk_row_id).split("i"))[0] != mytime) {
							var sql = "delete from pumprecord";
							var param = {
								"db" : 'hzlqzw.db',
								"sql" : sql
							};
							summer.UMSqlite.execSql(param);
							jsonArray.splice(i--, 1);
						}
					});
					this.listDatas = jsonArray;
				}
			},
			//添加新的泵站操作信息
			addItem : function() {
				roads.openWin("maintain", "pumpRecordDetail", "pumpStation/pumpRecordDetail.html", {
					"shift" : this.shift,
					"pumpID" : this.pumpID,
					"record_id" : this.record_id
				});
			},
			//已有的泵站操作信息点击修改事件
			itemClick : function(index) {
			},
			tapHold : function(index) {
				// 这里编辑长按列表项逻辑
				var ev = event || window.event;
				var touches = ev.targetTouches;
				if (touches.length > 1) {
					return;
				}
				this.timeOutEvent = setTimeout(function() {
					this.timeOutEvent = 0;
				}, 2000);
			},
			cancelTapHold : function() {
				// 取消长按
				clearTimeout(this.timeOutEvent);
				this.timeOutEvent = 0;
			},
			//已有的泵站操作信息滑动删除事件
			deleteItem : function() {
				var item = this.listDatas[this.listDatas.length - 1];
				UM.confirm({
					title : "确认删除",
					text : "确认要删除本条泵站操作信息吗？删除后将无法撤销。",
					btnText : ["取消", "确定"],
					overlay : true,
					duration : 2000,
					cancle : function() {
					},
					ok : function() {
						var sql = "delete from pumprecord where pk_row_id='" + item["pk_row_id"] + "' and pump_id='" + vue.pumpID + "' and pk_father_row_id='" + vue.record_id + "'";
						var param = {
							"db" : 'hzlqzw.db', //数据库名称
							"sql" : sql, //查询条件
							"startIndex" : 0, //可选参数 起始记录索引号(含)
						}
						summer.UMSqlite.execSql(param);
						vue.listDatas.pop();
					}
				});
			},
			// 返回
			goback : function() {
				roads.execScript("pumpStation", "vue.countPumpInfo", {
					pumpID : this.pumpID + ""
				});
				roads.closeWin();
			}
		},
		mounted : function() {
			this.$nextTick(function() {
				//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//初始化数据库
				this.initDB();
				//查询数据库中现有的过船信息
				this.dbQuery();
			})
		}
	});
}