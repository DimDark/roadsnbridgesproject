/*by zhuhy*/
summerready = function() {
	initPage();
}
//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}
};

//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}

var vue = null;

function initPage() {
	vue = new Vue({
		el : '#index',
		data : {
			popupVisible : false,
			popupVisibleProblem : false,
			equips : null,
			equipName : "",
			problemTypes : null,
			problemType : "",
			problemDescription : "",
			location : "",
			pic : [{
				picPaths : [],
				picNum : 0
			}, {
				picPaths : [],
				picNum : 0
			}],
			planCompletion : 0,
			realid : summer.pageParam.realid,
			record_id : summer.pageParam.record_id
		},
		methods : {
			//初始化数据库
			initDB : function() {
				var sql = "create table if not exists manageplan (pk_row_id VARCHAR PRIMARY KEY,pk_father_row_id INTEGER,equip_name VARCHAR,problem_type VARCHAR,problem_description VARCHAR,location VARCHAR,pic_paths_before VARCHAR,pic_num_before INTEGER,plan_completion INTEGER,pic_paths_after VARCHAR,pic_num_after INTEGER,fill_completion INTEGER)"
				var param = {
					"db" : 'hzlqzw.db',
					"sql" : sql
				};
				summer.UMSqlite.execSql(param);
			},
			// picker view 滚动时触发
			onValueChange : function(picker, value) {
			},
			//滚动器确认
			confirmChange : function() {
				this.equipName = this.$refs.equiPicker.getValues()[0]['text'];
				this.popupVisible = false;
			},
			//问题滚动器确认
			confirmChangeProblem : function() {
				this.problemType = this.$refs.problemPicker.getValues()[0]['text'];
				this.popupVisibleProblem = false;
			},
			//初始化问题类型选择器
			initOptionsp : function() {
				this.problemTypes = [{
					values : [{
						"value" : "0",
						"text" : "保洁"
					}, {
						"value" : "1",
						"text" : "市政"
					}]
				}];
			},
			//初始化选择器
			initOptions : function() {
				var readContent = summer.readFile("equips_maintain/manageRiver.txt");
				if (readContent != null && readContent != "") {
					this.equips = [{
						values : ($summer.isJSONObject(readContent) ? readContent : JSON.parse(readContent))
					}];
				} else {
					her.loading("加载中，请稍候..");
					var param = {
						"type" : this.module_id
					};
					this.queryData(param);
				}
			},
			//查询基础数据
			queryData : function(param) {
				/*
				roads.ajaxRequest({
				type : "get",
				url : "/mob/mobAdd2",
				param : param,
				header : ""
				}, this.querySuccess, this.queryFail);*/

				////////////////////////////////////////////假装获取equips/////////////////////////////////////////
				var receive = [{
					"value" : "0",
					"text" : "胜利河水闸"
				}, {
					"value" : "1",
					"text" : "大关水闸"
				}, {
					"value" : "2",
					"text" : "小和山水闸"
				}];
				this.equips = [{
					values : receive
				}];
				summer.writeFile("equips_maintain/manageRiver.txt", receive);
				her.loaded();
			},
			//查询成功
			querySuccess : function(data) {
				her.loaded();
				if (data.code == "1" || data.code == 1) {
					this.equips = [{
						values : data.msg
					}];
					summer.writeFile("equips_maintain/manageRiver.txt", data.msg);
				} else if (data.code == "0" || data.code == 0) {
					UM.toast({
						"title" : "加载失败",
						"text" : data.msg,
						"duration" : 3000
					});
				}
			},
			//查询失败
			queryFail : function() {
				her.loaded();
				UM.toast({
					"title" : "加载失败",
					"text" : "请检查网络",
					"duration" : 3000
				});
			},
			//前往图片总览页
			goPicCollection : function(owner) {
				roads.openWin("util", "picCollection", "picCollection/picCollection.html", {
					ownerTag : owner,
					picPaths : this.pic[owner].picPaths,
					amountLimit : 4,
					module_id : this.module_id,
					module_title : this.module_title,
					record_id : this.record_id,
					win_id : "manageRiverDetail"
				});
			},
			//从图片收集页返回
			backfrCollection : function(jsonStr) {
				this.pic[jsonStr["ownerTag"]].picPaths = jsonStr["picPaths"];
				this.pic[jsonStr["ownerTag"]].picNum = jsonStr["picPaths"].length;
			},
			//生成时间ID
			timeId : function() {
				var iter;
				var myDate = new Date();
				//生成时间戳并去除其中的斜杠
				var mytime = myDate.toLocaleDateString().toString().trim().replace(/\//g, "");
				if (isEmpty(summer.getStorage('river_iter')))
					iter = "001";
				else {
					//去零再累加
					iter = summer.getStorage('river_iter').replace(/\b(0+)/gi, "") + 1;
					var l = iter.toString().length;
					if (iter.length < 3) {
						//循环补零
						for (var i = 0; i < 3 - l; i++)
							iter = "0" + iter.toString().trim();
					}
				}
				//把累加器存回手机存储
				summer.setStorage('river_iter', iter);
				//'i'用来分隔时间戳和累加器
				return (mytime + 'i' + iter);
			},
			//获取真id判别是新增还是修改
			fillPage : function() {
				if (this.realid + "" != "new") {
					//更新之前的过船信息&原表单填充
					var sql = "select * from manageplan where pk_row_id='" + this.realid + "' and pk_father_row_id='" + this.record_id + "'";
					var param = {
						"db" : 'hzlqzw.db', //数据库名称
						"sql" : sql, //查询条件
						"startIndex" : 0, //可选参数 起始记录索引号(含)
					}

					var list = summer.UMSqlite.query(param);
					var list1 = eval(list);

					if (list1 != null && list1.length != 0) {
						var obj = list1[0];
						if (!objIsEmpty(obj["equip_name"])) {
							this.equipName = obj["equip_name"];
						}
						if (!objIsEmpty(obj["problem_type"])) {
							this.problemType = obj["problem_type"];
						}
						if (!objIsEmpty(obj["problem_description"])) {
							this.problemDescription = obj["problem_description"];
						}
						if (!objIsEmpty(obj["location"])) {
							this.location = obj["location"];
						}
						if (!objIsEmpty(obj["pic_paths_before"])) {
							this.pic[0].picPaths = eval(obj["pic_paths_before"]);
						}
						if (!objIsEmpty(obj["pic_num_before"])) {
							this.pic[0].picNum = obj["pic_num_before"];
						}
						if (!objIsEmpty(obj["plan_completion"])) {
							this.planCompletion = obj["plan_completion"];
						}
						if (!objIsEmpty(obj["pic_paths_after"])) {
							this.pic[1].picPaths = eval(obj["pic_paths_after"]);
						}
						if (!objIsEmpty(obj["pic_num_after"])) {
							this.pic[1].picNum = obj["pic_num_after"];
						}
					}
				} else {
					//新增过船信息
				}
			},
			// 暂存表单
			saveForm : function() {
				//根据完成度来做暂存
				var fillCompletion = (isEmpty(this.equipName) ? 0 : 1) + (isEmpty(this.problemType) ? 0 : 1) + (isEmpty(this.problemDescription) ? 0 : 1) + (isEmpty(this.location) ? 0 : 1) + (this.pic[0].picNum <= 0 ? 0 : 1) + (isEmpty(this.planCompletion) ? 0 : 1) + (this.pic[1].picNum <= 0 ? 0 : 1);
				if (fillCompletion) {
					if ((isEmpty(this.equipName) ? 0 : 1)) {
						var sql = null;
						if (this.realid == "new")
							sql = "insert into manageplan (pk_row_id ,pk_father_row_id ,equip_name ,problem_type ,problem_description ,location ,pic_paths_before ,pic_num_before ,plan_completion ,pic_paths_after ,pic_num_after ,fill_completion) values ('" + this.timeId() + "','" + this.record_id + "','" + this.equipName + "','" + this.problemType + "','" + this.problemDescription + "','" + this.location + "','" + JSON.stringify(this.pic[0].picPaths) + "','" + this.pic[0].picNum + "','" + this.planCompletion + "','" + JSON.stringify(this.pic[1].picPaths) + "','" + this.pic[1].picNum + "','" + (fillCompletion == 7 ? 1 : 0) + "')";
						else
							sql = "update manageplan set equip_name='" + this.equipName + "',problem_type='" + this.problemType + "',problem_description='" + this.problemDescription + "',location='" + this.location + "',pic_paths_before='" + JSON.stringify(this.pic[0].picPaths) + "',pic_num_before='" + this.pic[0].picNum + "',plan_completion='" + this.planCompletion + "',pic_paths_after='" + JSON.stringify(this.pic[1].picPaths) + "',pic_num_after='" + this.pic[1].picNum + "',fill_completion='" + (fillCompletion == 7 ? 1 : 0) + "' where pk_row_id='" + this.realid + "' and pk_father_row_id='" + this.record_id + "'";
						var param = {
							"db" : 'hzlqzw.db',
							"sql" : sql
						}
						summer.UMSqlite.execSql(param);
						roads.execScript("manageRiver", "vue.dbQuery", "");
						roads.closeWin();
					} else {
						UM.toast({
							title : '友情提示:',
							text : '没有选择必选项（第一项）,无法执行操作。',
							duration : 3000
						});
					}
				} else {
					UM.toast({
						title : '友情提示:',
						text : '没有任何填写的数据需保存',
						duration : 3000
					});
				}
			},
			// 返回
			goback : function() {
				roads.confirmClose();
			}
		},
		mounted : function() {
			// 加载数据...
			this.$nextTick(function() {
				//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//初始化数据库
				this.initDB();
				//初始化设备选择器
				this.initOptions();
				//初始化问题类型选择器
				this.initOptionsp();
				//初始化报表
				this.fillPage();
			})
		}
	})
}