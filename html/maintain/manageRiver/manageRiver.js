/*by zhuhy*/
summerready = function() {
	initPage();
}
//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}
};

//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}

var vue = null;

function initPage() {
	vue = new Vue({
		el : '#index',
		data : {
			dept : summer.pageParam.dept,
			module_id : summer.pageParam.module_id,
			module_title : summer.pageParam.module_title,
			record_id : parseInt(summer.pageParam.record_id),
			fillInDate : new Date().format("yyyy-MM-dd"),
			datePickerValue : new Date(),
			startDate : new Date(),
			endDate : new Date(),
			listDatas : [],
			timeOutEvent : 0
		},
		methods : {
			//初始化数据库
			initDB : function() {
				var sql = "create table if not exists " + this.module_id + " (pk_row_id INTEGER PRIMARY KEY AUTOINCREMENT,fillin_date VARCHAR,fill_completion INTEGER)"
				var param = {
					"db" : 'hzlqzw.db',
					"sql" : sql
				};
				summer.UMSqlite.execSql(param);
			},
			open : function(picker) {
				this.$refs[picker].open();
			},
			//日期选择器确认方法
			handleDateConfirm : function(value) {
				this.fillInDate = new Date(value).format("yyyy-MM-dd");
			},
			//生成日期选择器选择范围
			dateRange : function(day) {
				var time = new Date();
				time.setDate(time.getDate() + day);
				var y = time.getFullYear();
				var m = time.getMonth() + 1;
				var d = time.getDate();
				return new Date(y + "-" + m + "-" + d);
			},
			//初始化表单
			fillPage : function() {
				if (this.record_id >= 0) {
					var sql = "select * from " + this.module_id + " where pk_row_id='" + this.record_id + "'";
					var param = {
						"db" : 'hzlqzw.db', //数据库名称
						"sql" : sql, //查询条件
						"startIndex" : 0, //可选参数 起始记录索引号(含)
					}
					var list = summer.UMSqlite.query(param);
					var list1 = eval(list);

					if (list1 != null) {
						var obj = list1[0];
						if (!objIsEmpty(obj["fillin_date"]))
							this.fillInDate = obj["fillin_date"];
					}
				}
			},
			//查询已有的过船信息
			dbQuery : function() {
				var sql = "select pk_row_id,equip_name,fill_completion from manageplan where pk_father_row_id='" + this.record_id + "'";
				var param = {
					"db" : 'hzlqzw.db', //数据库名称
					"sql" : sql, //查询条件
					"startIndex" : 0, //可选参数 起始记录索引号(含)
				}
				var list = summer.UMSqlite.query(param);
				var jsonArray = eval(list);

				if (jsonArray != null && jsonArray != undefined && jsonArray.length != 0) {
					this.listDatas = jsonArray;
				}
			},
			//添加新的过船信息
			addItem : function() {
				if (this.saveFormKernal()) {
					roads.openWin(this.dept, "manageRiverDetail", "manageRiver/manageRiverDetail/manageRiverDetail.html", {
						"realid" : "new",
						"record_id" : this.record_id
					});
				}
			},
			//已有的过船信息点击修改事件
			itemClick : function(index) {
				var item = this.listDatas[index];
				roads.openWin(this.dept, "manageRiverDetail", "manageRiver/manageRiverDetail/manageRiverDetail.html", {
					"realid" : item["pk_row_id"],
					"record_id" : this.record_id
				});
			},
			tapHold : function(index) {
				// 这里编辑长按列表项逻辑
				var ev = event || window.event;
				var touches = ev.targetTouches;
				if (touches.length > 1) {
					return;
				}
				this.timeOutEvent = setTimeout(function() {
					this.timeOutEvent = 0;
				}, 2000);
			},
			cancelTapHold : function() {
				// 取消长按
				clearTimeout(this.timeOutEvent);
				this.timeOutEvent = 0;
			},
			//已有的过船信息滑动删除事件
			deleteItem : function(index) {
				var item = this.listDatas[index];
				UM.confirm({
					title : "确认删除",
					text : "确认要删除本条整改计划吗？删除后将无法撤销。",
					btnText : ["取消", "确定"],
					overlay : true,
					duration : 2000,
					cancle : function() {
					},
					ok : function() {
						var sql = "delete from manageplan where pk_row_id='" + item["pk_row_id"] + "' and pk_father_row_id='" + vue.record_id + "'";
						var param = {
							"db" : 'hzlqzw.db', //数据库名称
							"sql" : sql, //查询条件
							"startIndex" : 0, //可选参数 起始记录索引号(含)
						}
						summer.UMSqlite.execSql(param);
						vue.listDatas.splice(index, 1);
					}
				});
			},
			//暂存表单里
			saveFormKernal : function() {
				//根据完成度来做暂存
				var kernalResult = 0;
				var fillCompletion = (isEmpty(this.fillInDate) ? 0 : 1);
				if (fillCompletion) {
					if ((isEmpty(this.fillInDate) ? 0 : 1)) {
						var sql = "insert or replace into " + this.module_id + "(pk_row_id,fillin_date,fill_completion)values('1','" + this.fillInDate + "','" + (fillCompletion == 1 ? 1 : 0) + "')";
						var param = {
							"db" : 'hzlqzw.db',
							"sql" : sql
						}
						summer.UMSqlite.execSql(param);

						if (this.record_id < 0) {
							var sql1 = "select * from " + this.module_id;
							var param1 = {
								"db" : 'hzlqzw.db', //数据库名称
								"sql" : sql1, //查询条件
								"startIndex" : 0, //可选参数 起始记录索引号(含)
							}
							var list = summer.UMSqlite.query(param1);
							var jsonArray = eval(list);
							if (jsonArray.length > 0) {
								//插入成功
								kernalResult = 1;
								this.record_id = jsonArray.length;
							}

							var sql2 = "update modulestab set record_num='" + jsonArray.length + "' where module_id='" + this.module_id + "'";
							var param2 = {
								"db" : 'hzlqzw.db', //数据库名称
								"sql" : sql2, //查询条件
								"startIndex" : 0, //可选参数 起始记录索引号(含)
							}
							summer.UMSqlite.execSql(param2);
						} else {
							kernalResult = 1;
						}
					} else {
						UM.toast({
							title : '友情提示:',
							text : '没有选择必选项（第一项）,无法执行操作。',
							duration : 3000
						});
					}
				} else {
					UM.toast({
						title : '抱歉:',
						text : '没有填写任何的数据,无法执行操作。',
						duration : 3000
					});
				}
				return kernalResult;
			},
			// 暂存表单
			saveForm : function() {
				if (this.saveFormKernal()) {
					summer.closeWin();
					roads.execScript("menu", "vue.formSaved", {
						"module_title" : this.module_title
					});
				}
			},
			// 提交表单
			submitForm : function() {
				var self = this;
				var data = JSON.parse(JSON.stringify(self.$data));
				self.$toast({
					message : data,
					position : "center",
					duration : 3000
				});
			},
			// 返回
			goback : function() {
				roads.confirmClose();
			}
		},
		mounted : function() {
			this.$nextTick(function() {
				//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//初始化数据库
				this.initDB();
				//初始化日期选择器选择范围:前后一个月？
				this.startDate = this.dateRange(-30);
				this.endDate = this.dateRange(30);
				//初始化报表
				this.fillPage();
				//查询数据库中现有的过船信息
				this.dbQuery();
			})
		}
	})
}