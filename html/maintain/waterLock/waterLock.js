/*by zhuhy*/
summerready = function() {
	initPage();
}
//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}
};
//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}

var vue = null;

function initPage() {
	var vue = new Vue({
		el : '#index',
		data : {
			dept : summer.pageParam.dept,
			module_id : summer.pageParam.module_id,
			module_title : summer.pageParam.module_title,
			record_id : parseInt(summer.pageParam.record_id),
			popupVisible : false,
			popupVisibleWeather : false,
			equips : null,
			equipName : "",
			weather : "",
			weatherDescriptions : null,
			fillInDate : new Date().format("yyyy-MM-dd"),
			datePickerValue : new Date(),
			startDate : new Date(),
			endDate : new Date(),
			upWater : "",
			openDegree : "",
			downWater : "",
			memo : "",
			watchKeeper : ""
		},
		methods : {
			//初始化数据库
			initDB : function() {
				//建表
				var sql = "create table if not exists " + this.module_id + " (pk_row_id INTEGER PRIMARY KEY AUTOINCREMENT, equip_name VARCHAR UNIQUE, fillin_date VARCHAR, weather VARCHAR, watch_keeper VARCHAR, up_water VARCHAR,open_degree VARCHAR,down_water VARCHAR,memo VARCHAR,fill_completion INTEGER)";
				var param = {
					"db" : 'hzlqzw.db',
					"sql" : sql
				}
				summer.UMSqlite.execSql(param);
			},
			// picker view 滚动时触发
			onValueChange : function(picker, value) {
			},
			onValueChangeWeather : function(picker, value) {
			},
			//滚动器确认
			confirmChange : function() {
				this.equipName = this.$refs.equiPicker.getValues()[0]['text'];
				this.popupVisible = false;
			},
			confirmChangeWeather : function() {
				this.weather = this.$refs.weatherPicker.getValues()[0]['text'];
				this.popupVisibleWeather = false;
			},
			open : function(picker) {
				this.$refs[picker].open();
			},
			//日期选择器确认方法
			handleDateConfirm : function(value) {
				this.fillInDate = new Date(value).format("yyyy-MM-dd");
			},
			//初始化天气选择器
			initOptionsw : function() {
				this.weatherDescriptions = [{
					values : [{
						"value" : "0",
						"text" : "晴"
					}, {
						"value" : "1",
						"text" : "雨"
					}]
				}];
			},
			//初始化选择器
			initOptions : function() {
				var readContent = summer.readFile("equips_maintain/waterLock.txt");
				if (readContent != null && readContent != "") {
					this.equips = [{
						values : ($summer.isJSONObject(readContent) ? readContent : JSON.parse(readContent))
					}];
				} else {
					her.loading("加载中，请稍候..");
					var param = {
						"type" : this.module_id
					};
					this.queryData(param);
				}
			},
			//查询基础数据
			queryData : function(param) {
				/*
				roads.ajaxRequest({
				type : "get",
				url : "/mob/mobAdd2",
				param : param,
				header : ""
				}, this.querySuccess, this.queryFail);*/

				////////////////////////////////////////////假装获取equips/////////////////////////////////////////
				var receive = [{
					"value" : "0",
					"text" : "胜利河水闸"
				}, {
					"value" : "1",
					"text" : "大关水闸"
				}, {
					"value" : "2",
					"text" : "小和山水闸"
				}];
				this.equips = [{
					values : receive
				}];
				summer.writeFile("equips_maintain/waterLock.txt", receive);
				her.loaded();
			},
			//查询成功
			querySuccess : function(data) {
				her.loaded();
				if (data.code == "1" || data.code == 1) {
					this.equips = [{
						values : data.msg
					}];
					summer.writeFile("equips_maintain/waterLock.txt", data.msg);
				} else if (data.code == "0" || data.code == 0) {
					UM.toast({
						"title" : "加载失败",
						"text" : data.msg,
						"duration" : 3000
					});
				}
			},
			//查询失败
			queryFail : function() {
				her.loaded();
				UM.toast({
					"title" : "加载失败",
					"text" : "请检查网络",
					"duration" : 3000
				});
			},
			//生成日期选择器选择范围
			dateRange : function(day) {
				var time = new Date();
				time.setDate(time.getDate() + day);
				var y = time.getFullYear();
				var m = time.getMonth() + 1;
				var d = time.getDate();
				return new Date(y + "-" + m + "-" + d);
			},
			//初始化表单
			fillPage : function() {
				if (this.record_id >= 0) {
					var sql = "select * from " + this.module_id + " where pk_row_id='" + this.record_id + "'";
					var param = {
						"db" : 'hzlqzw.db', //数据库名称
						"sql" : sql, //查询条件
						"startIndex" : 0, //可选参数 起始记录索引号(含)
					}
					var list = summer.UMSqlite.query(param);
					var list1 = eval(list);

					if (list1 != null) {
						var obj = list1[0];
						if (!objIsEmpty(obj["equip_name"]))
							this.equipName = obj["equip_name"];
						if (!objIsEmpty(obj["fillin_date"]))
							this.fillInDate = obj["fillin_date"];
						if (!objIsEmpty(obj["weather"]))
							this.weather = obj["weather"];
						if (!objIsEmpty(obj["watch_keeper"]))
							this.watchKeeper = obj["watch_keeper"];
						if (!objIsEmpty(obj["up_water"]))
							this.upWater = obj["up_water"];
						if (!objIsEmpty(obj["open_degree"]))
							this.openDegree = obj["open_degree"];
						if (!objIsEmpty(obj["down_water"]))
							this.downWater = obj["down_water"];
						if (!objIsEmpty(obj["memo"]))
							this.memo = obj["memo"];
					}
				}
			},
			// 暂存表单
			saveForm : function() {
				var fillCompletion = (isEmpty(this.equipName) ? 0 : 1) + (isEmpty(this.fillInDate) ? 0 : 1) + (isEmpty(this.weather) ? 0 : 1) + (isEmpty(this.upWater) ? 0 : 1) + (isEmpty(this.openDegree) ? 0 : 1) + (isEmpty(this.downWater) ? 0 : 1) + (isEmpty(this.watchKeeper) ? 0 : 1) + (isEmpty(this.memo) ? 0 : 1);
				if (fillCompletion) {
					if ((isEmpty(this.equipName) ? 0 : 1)) {
						var sql = null;
						if (this.record_id < 0) {
							sql = "insert or replace into " + this.module_id + "(equip_name,fillin_date,weather,watch_keeper,up_water,open_degree,down_water,memo,fill_completion)values('" + this.equipName + "','" + this.fillInDate + "','" + this.weather + "','" + this.watchKeeper + "','" + this.upWater + "','" + this.openDegree + "','" + this.downWater + "','" + this.memo + "','" + (fillCompletion == 8 ? 1 : 0) + "')";
						} else {
							sql = "update " + this.module_id + " set equip_name='" + this.equipName + "',fillin_date='" + this.fillInDate + "',weather='" + this.weather + "',watch_keeper='" + this.watchKeeper + "',up_water='" + this.upWater + "',open_degree='" + this.openDegree + "',down_water='" + this.downWater + "',memo='" + this.memo + "',fill_completion='" + (fillCompletion == 8 ? 1 : 0) + "' where pk_row_id='" + this.record_id + "'";
						}

						var param = {
							"db" : 'hzlqzw.db',
							"sql" : sql
						}
						summer.UMSqlite.execSql(param);

						if (this.record_id < 0) {
							var sql1 = "select * from " + this.module_id;
							var param1 = {
								"db" : 'hzlqzw.db', //数据库名称
								"sql" : sql1, //查询条件
								"startIndex" : 0, //可选参数 起始记录索引号(含)
							}
							var list = summer.UMSqlite.query(param1);
							var jsonArray = eval(list);

							var sql2 = "update modulestab set record_num='" + jsonArray.length + "' where module_id='" + this.module_id + "'";
							var param2 = {
								"db" : 'hzlqzw.db', //数据库名称
								"sql" : sql2, //查询条件
							}
							summer.UMSqlite.execSql(param2);
						}

						summer.closeWin();
						roads.execScript("menu", "vue.formSaved", {
							"module_title" : this.module_title
						});
					} else {
						UM.toast({
							title : '友情提示:',
							text : '没有选择必选项（第一项）,无法执行操作。',
							duration : 3000
						});
					}
				} else {
					UM.toast({
						title : '抱歉:',
						text : '没有填写任何的数据,无法执行操作。',
						duration : 3000
					});
				}
			},
			// 返回
			goback : function() {
				roads.confirmClose();
			},
			// 提交表单
			submitForm : function() {
				var self = this;
				var data = this.$data;
				UM.confirm({
					title : "提交数据",
					text : "确认提交该数据",
					btnText : ["取消", "确定"],
					overlay : true,
					duration : 2000,
					cancle : function() {
					},
					ok : function() {
						this.dataSubmit(self, data);
					}
				});
			},
			dataSubmit : function(self, data) {
				var ev = data.equipValue;
				var wv = data.weatherValue;
				var uw = data.upwater;
				var dw = data.downwater;
				var wl = data.opendegree;
				var fd = data.curdate;
				var op = data.ondutyperson;
				var vemo = data.vemo;

				if (isEmpty(ev) || isEmpty(wv) || isEmpty(uw) || isEmpty(dw) || isEmpty(wl) || isEmpty(fd) || isEmpty(op)) {
					self.$toast({
						message : "除备注外其他信息都需要填写",
						position : "center",
						duration : 3000
					});
				} else {
					var param = {};
					param.ev = ev;
					param.wv = wv;
					param.uw = uw;
					param.dw = dw;
					param.wl = wl;
					param.fd = fd;
					param.op = op;
					param.type = op;
					param.vemo = vemo;
					askweb(param)
				}
			}
		},
		mounted : function() {
			// 加载数据...
			this.$nextTick(function() {
				//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//初始化数据库
				this.initDB();
				//初始化设备选择器
				this.initOptions();
				//初始化天气选择器
				this.initOptionsw();
				//初始化日期选择器选择范围:前后一个月？
				this.startDate = this.dateRange(-30);
				this.endDate = this.dateRange(30);
				//初始化报表
				this.fillPage();
			})
		}
	})
}