/*by zhuhy*/
summerready = function() {
	initPage();
}
//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}
};

//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}

var vue = null;

function initPage() {
	vue = new Vue({
		el : '#index',
		data : {
			upWater : "",
			upPercent : "",
			waterLevel : "",
			downPercent : "",
			downWater : "",
			//时段标识
			shiftTime : parseInt(summer.pageParam.shift) * 8 + parseInt(summer.pageParam.j),
			shift : summer.pageParam.shift,
			record_id : summer.pageParam.record_id,
			module_id : summer.pageParam.module_id
		},
		methods : {
			//初始化数据库
			initDB : function() {
				if (this.record_id < 0) {
					summer.closeWin();
				}

				//建表
				var sql = "create table if not exists boatlockdetail (pk_row_id INTEGER, pk_father_row_id INTEGER, up_water INTEGER,up_percent INTEGER,water_level INTEGER,down_percent INTEGER,down_water INTEGER,fill_completion INTEGER,PRIMARY KEY (pk_row_id,pk_father_row_id))";
				var param = {
					"db" : 'hzlqzw.db',
					"sql" : sql
				}
				summer.UMSqlite.execSql(param);

				//插入数据
				var sql1 = "insert or ignore into boatlockdetail(pk_row_id,pk_father_row_id,fill_completion) values('" + this.shiftTime + "','" + this.record_id + "','0');"
				var param1 = {
					"db" : 'hzlqzw.db',
					"sql" : sql1
				};
				summer.UMSqlite.execSql(param1);
			},
			//初始化页面
			fillPage : function() {
				//更新之前的过船信息&原表单填充
				var sql = "select * from boatlockdetail where pk_row_id='" + this.shiftTime + "' and pk_father_row_id='" + this.record_id + "'";
				var param = {
					"db" : 'hzlqzw.db', //数据库名称
					"sql" : sql, //查询条件
					"startIndex" : 0, //可选参数 起始记录索引号(含)
				}

				var list = summer.UMSqlite.query(param);
				var list1 = eval(list);

				if (list1 != null && list1.length != 0) {
					var obj = list1[0];
					if (!objIsEmpty(obj["up_water"])) {
						this.upWater = obj["up_water"];
					}
					if (!objIsEmpty(obj["up_percent"])) {
						this.upPercent = obj["up_percent"];
					}
					if (!objIsEmpty(obj["water_level"])) {
						this.waterLevel = obj["water_level"];
					}
					if (!objIsEmpty(obj["down_percent"])) {
						this.downPercent = obj["down_percent"];
					}
					if (!objIsEmpty(obj["down_water"])) {
						this.downWater = obj["down_water"];
					}
				}
			},
			// 暂存表单
			saveForm : function() {
				//根据完成度来做暂存
				var fillCompletion = (isEmpty(this.upWater) ? 0 : 1) + (isEmpty(this.upPercent) ? 0 : 1) + (isEmpty(this.waterLevel) ? 0 : 1) + (isEmpty(this.downPercent) ? 0 : 1) + (isEmpty(this.downWater) ? 0 : 1);
				if (fillCompletion) {
					var sql = "update boatlockdetail set up_water='" + this.upWater + "',up_percent='" + this.upPercent + "',water_level='" + this.waterLevel + "',down_percent='" + this.downPercent + "',down_water='" + this.downWater + "',fill_completion='" + (fillCompletion == 5 ? 1 : 0) + "' where pk_row_id='" + this.shiftTime + "' and pk_father_row_id='" + this.record_id + "'";
					var param = {
						"db" : 'hzlqzw.db',
						"sql" : sql
					}
					summer.UMSqlite.execSql(param);
					roads.execScript("boatLock", "vue.backfrDetail", {
						"j" : summer.pageParam.j,
						"completion" : (fillCompletion == 5 ? 1 : 0)
					});
					summer.closeWin();
				} else {
					UM.toast({
						title : '友情提示:',
						text : '没有任何填写的数据需保存',
						duration : 3000
					});
				}
			},
			// 返回
			goback : function() {
				roads.confirmClose();
			}
		},
		mounted : function() {
			// 加载数据...
			this.$nextTick(function() {//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//初始化数据库
				this.initDB();
				//初始化表单
				this.fillPage();
			})
		}
	})
}

function percentcheck() {
	$("#down_precent").on("blur", function() {
		var val = $(this).val();
		if (val > 100) {
			document.getElementById("down_precent").value = "";
			UM.alert("百分比不能大于100");
		}

	});
	$("#up_precent").on("blur", function() {
		var val = $(this).val();
		if (val > 100) {
			document.getElementById("up_precent").value = "";
			UM.alert("百分比不能大于100");
		}
	})
}