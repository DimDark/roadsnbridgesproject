/*by zhuhy*/
summerready = function() {
	initPage();
}
//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}
};
//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}

var vue = null;

function initPage() {
	vue = new Vue({
		el : '#index',
		data : {
			dept : summer.pageParam.dept,
			module_id : summer.pageParam.module_id,
			module_title : summer.pageParam.module_title,
			record_id : parseInt(summer.pageParam.record_id),
			popupVisible : false,
			popupVisibleWeather : false,
			equips : null,
			equipName : "",
			weather : "",
			weatherDescriptions : null,
			fillInDate : new Date().format("yyyy-MM-dd"),
			datePickerValue : new Date(),
			startDate : new Date(),
			endDate : new Date(),
			shift : null,
			shiftArray : [],
			//此为详情完成度缓存，最后一位代表班次shift，前八位分别表示对应班次中的八个时段的详情的完成度，0：未完成，1：完成
			scache : "000000000",
			boatNum : 0,
			memo : "",
			watchKeeper : ""
		},
		methods : {
			//初始化数据库
			initDB : function() {
				//建表
				var sql = "create table if not exists " + this.module_id + " (pk_row_id INTEGER PRIMARY KEY AUTOINCREMENT, equip_name VARCHAR UNIQUE, fillin_date VARCHAR, weather VARCHAR, scache VARCHAR, memo VARCHAR, watch_keeper VARCHAR, shift INTEGER, fill_completion INTEGER)";
				var param = {
					"db" : 'hzlqzw.db',
					"sql" : sql
				}
				summer.UMSqlite.execSql(param);
			},
			// picker view 滚动时触发
			onValueChange : function(picker, value) {
			},
			onValueChangeWeather : function(picker, value) {
			},
			confirmChange : function() {
				this.equipName = this.$refs.equiPicker.getValues()[0]['text'];
				this.popupVisible = false;
			},
			confirmChangeWeather : function() {
				this.weather = this.$refs.weatherPicker.getValues()[0]['text'];
				this.popupVisibleWeather = false;
			},
			open : function(picker) {
				this.$refs[picker].open();
			},
			//日期选择器确认方法
			handleDateConfirm : function(value) {
				this.fillInDate = new Date(value).format("yyyy-MM-dd");
			},
			//初始化天气选择器
			initOptionsw : function() {
				this.weatherDescriptions = [{
					values : [{
						"value" : "0",
						"text" : "晴"
					}, {
						"value" : "1",
						"text" : "雨"
					}]
				}];
			},
			//初始化选择器
			initOptions : function() {
				var readContent = summer.readFile("equips_maintain/boatLock.txt");
				if (readContent != null && readContent != "") {
					this.equips = [{
						values : ($summer.isJSONObject(readContent) ? readContent : JSON.parse(readContent))
					}];
				} else {
					her.loading("加载中，请稍候..");
					var param = {
						"type" : this.module_id
					};
					this.queryData(param);
				}
			},
			//查询基础数据
			queryData : function(param) {
				/*
				roads.ajaxRequest({
				type : "get",
				url : "/mob/mobAdd2",
				param : param,
				header : ""
				}, this.querySuccess, this.queryFail);*/

				////////////////////////////////////////////假装获取equips/////////////////////////////////////////
				var receive = [{
					"value" : "0",
					"text" : "胜利河水闸"
				}, {
					"value" : "1",
					"text" : "大关水闸"
				}, {
					"value" : "2",
					"text" : "小和山水闸"
				}];
				this.equips = [{
					values : receive
				}];
				summer.writeFile("equips_maintain/boatLock.txt", receive);
				her.loaded();
			},
			//查询成功
			querySuccess : function(data) {
				her.loaded();
				if (data.code == "1" || data.code == 1) {
					this.equips = [{
						values : data.msg
					}];
					summer.writeFile("equips_maintain/boatLock.txt", data.msg);
				} else if (data.code == "0" || data.code == 0) {
					UM.toast({
						"title" : "加载失败",
						"text" : data.msg,
						"duration" : 3000
					});
				}
			},
			//查询失败
			queryFail : function() {
				her.loaded();
				UM.toast({
					"title" : "加载失败",
					"text" : "请检查网络",
					"duration" : 3000
				});
			},
			//生成日期选择器选择范围
			dateRange : function(day) {
				var time = new Date();
				time.setDate(time.getDate() + day);
				var y = time.getFullYear();
				var m = time.getMonth() + 1;
				var d = time.getDate();
				return new Date(y + "-" + m + "-" + d);
			},
			//初始化表单
			fillPage : function() {
				////////////////////////////////////////////假装获取shift/////////////////////////////////////////
				this.shift = 0;
				if (this.record_id >= 0) {
					var sql = "select * from " + this.module_id + " where pk_row_id='" + this.record_id + "'";
					var param = {
						"db" : 'hzlqzw.db', //数据库名称
						"sql" : sql, //查询条件
						"startIndex" : 0, //可选参数 起始记录索引号(含)
					}
					var list = summer.UMSqlite.query(param);
					var list1 = eval(list);

					if (list1 != null) {
						var obj = list1[0];
						if (!objIsEmpty(obj["shift"])) {
							if (obj["shift"] == this.shift) {
								if (!objIsEmpty(obj["scache"]))
									this.scache = obj["scache"];
							} else {
								this.scache = "00000000" + this.shift;
								UM.toast({
									title : '抱歉:',
									text : '本记录已失效',
									duration : 3000
								});
								return;
							}
						}
						if (!objIsEmpty(obj["equip_name"]))
							this.equipName = obj["equip_name"];
						if (!objIsEmpty(obj["fillin_date"]))
							this.fillInDate = obj["fillin_date"];
						if (!objIsEmpty(obj["weather"]))
							this.weather = obj["weather"];
						if (!objIsEmpty(obj["memo"]))
							this.memo = obj["memo"];
						if (!objIsEmpty(obj["watch_keeper"]))
							this.watchKeeper = obj["watch_keeper"];
					}
				} else {
					this.scache = "00000000" + this.shift;
				}
			},
			//条目点击
			shiftClick : function(index) {
				if (this.saveFormKernal()) {
					roads.openWin(this.dept, "boatLockDetail", "boatLock/boatLockDetail.html", {
						"shift" : this.shift,
						"j" : index,
						"record_id" : this.record_id,
						"module_id" : this.module_id
					});
				}
			},
			//过船点击
			boatclick : function() {
				if (this.saveFormKernal()) {
					roads.openWin(this.dept, "goBoat", "boatLock/goBoat.html", {
						"shift" : this.shift,
						"record_id" : this.record_id,
						"module_id" : this.module_id
					});
				}
			},
			//从详情页返回，于是刷新班次表格的填报状态
			backfrDetail : function(params) {
				this.scache = this.scache.substring(0, eval(params)["j"]) + eval(params)["completion"] + this.scache.substring(eval(params)["j"] + 1);
				UM.toast({
					title : '友情提示:',
					text : '数据已保存',
					duration : 3000
				});
			},
			//获取过船信息的计数
			countGoboatNum : function() {
				var sql = "select * from goboat where pk_father_row_id='" + this.record_id + "' and fill_completion='1'";
				var param = {
					"db" : 'hzlqzw.db', //数据库名称
					"sql" : sql, //查询条件
					"startIndex" : 0, //可选参数 起始记录索引号(含)
				}
				var list = summer.UMSqlite.query(param);
				var jsonArray = eval(list);
				if (jsonArray != null && jsonArray != undefined && jsonArray.length != 0) {
					var myDate = new Date();
					var mytime = myDate.toLocaleDateString();
					mytime = mytime.toString().trim().replace(/\//g, "");
					$.each(jsonArray, function(i, obj) {
						//过滤此条信息如果过期
						if (((obj.pk_row_id).split("i"))[0] != mytime) {
							jsonArray.splice(i--, 1);
						}
					});
				}
				this.boatNum = jsonArray.length;
			},
			//暂存表单里
			saveFormKernal : function() {
				//根据完成度来做暂存
				var kernalResult = 0;
				var fillCompletion = (isEmpty(this.equipName) ? 0 : 1) + (isEmpty(this.fillInDate) ? 0 : 1) + (isEmpty(this.weather) ? 0 : 1) + (parseInt(this.scache.substring(0, this.scache.length - 1), 2) > 0 ? 1 : 0) + (isEmpty(this.memo) ? 0 : 1) + (isEmpty(this.watchKeeper) ? 0 : 1);
				if (fillCompletion) {
					if ((isEmpty(this.equipName) ? 0 : 1)) {
						var sql = null;
						if (this.record_id < 0)
							sql = "insert or replace into " + this.module_id + "(equip_name,fillin_date,weather,scache,memo,watch_keeper,shift,fill_completion)values('" + this.equipName + "','" + this.fillInDate + "','" + this.weather + "','" + this.scache + "','" + this.memo + "','" + this.watchKeeper + "','" + this.shift + "','" + (fillCompletion == 6 ? 1 : 0) + "')";
						else
							sql = "update " + this.module_id + " set equip_name='" + this.equipName + "',fillin_date='" + this.fillInDate + "',weather='" + this.weather + "',scache='" + this.scache + "',memo='" + this.memo + "',watch_keeper='" + this.watchKeeper + "',shift='" + this.shift + "',fill_completion='" + (fillCompletion == 6 ? 1 : 0) + "' where pk_row_id='" + this.record_id + "' ";
						var param = {
							"db" : 'hzlqzw.db',
							"sql" : sql
						}
						summer.UMSqlite.execSql(param);

						if (this.record_id < 0) {
							var sql1 = "select * from " + this.module_id;
							var param1 = {
								"db" : 'hzlqzw.db', //数据库名称
								"sql" : sql1, //查询条件
								"startIndex" : 0, //可选参数 起始记录索引号(含)
							}
							var list = summer.UMSqlite.query(param1);
							var jsonArray = eval(list);
							if (jsonArray.length > 0) {
								//插入成功
								kernalResult = 1;
								this.record_id = jsonArray.length;
							}

							var sql2 = "update modulestab set record_num='" + jsonArray.length + "' where module_id='" + this.module_id + "'";
							var param2 = {
								"db" : 'hzlqzw.db', //数据库名称
								"sql" : sql2, //查询条件
								"startIndex" : 0, //可选参数 起始记录索引号(含)
							}
							summer.UMSqlite.execSql(param2);
						} else {
							kernalResult = 1;
						}
					} else {
						UM.toast({
							title : '友情提示:',
							text : '没有选择必选项（第一项）,无法执行操作。',
							duration : 3000
						});
					}
				} else {
					UM.toast({
						title : '抱歉:',
						text : '没有填写任何的数据,无法执行操作。',
						duration : 3000
					});
				}
				return kernalResult;
			},
			// 暂存表单
			saveForm : function() {
				if (this.saveFormKernal()) {
					summer.closeWin();
					roads.execScript("menu", "vue.formSaved", {
						"module_title" : this.module_title
					});
				}
			},
			// 提交表单
			submitForm : function() {
				var self = this;
				var data = JSON.parse(JSON.stringify(self.$data));
				self.$toast({
					message : data,
					position : "center",
					duration : 3000
				});
			},
			// 返回
			goback : function() {
				roads.confirmClose();
			},
		},
		watch : {
			shift : function(val) {
				this.shiftArray = [];
				for (var i = 0; i < 8; i++) {
					var jsonObj = {
						"shiftTime" : (val * 8 + i) + ":00",
						"shiftCompletion" : 0
					};
					this.shiftArray.push(jsonObj);
				}
			},
			scache : function(val) {
				$.each(this.shiftArray, function(i, obj) {
					obj["shiftCompletion"] = parseInt(val[i]);
				});
			}
		},
		mounted : function() {
			// 加载数据...
			this.$nextTick(function() {//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//初始化数据库
				this.initDB();
				//初始化设备选择器
				this.initOptions();
				//初始化天气选择器
				this.initOptionsw();
				//初始化日期选择器选择范围:前后一个月？
				this.startDate = this.dateRange(-30);
				this.endDate = this.dateRange(30);
				//初始化报表
				this.fillPage();
				//加载过船信息数量
				this.countGoboatNum();
			})
		}
	})
}