/*by zhuhy*/
summerready = function() {
	initPage();
}
//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}
};

//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}

var vue = null;

function initPage() {
	vue = new Vue({
		el : '#index',
		data : {
			passTime : null,
			timePickerValue : null,
			startHour : 0,
			endHour : 0,
			passType : "",
			passNum : null,
			switchTime : null,
			direction : 0,
			realid : summer.pageParam.realid,
			shift : parseInt(summer.pageParam.shift),
			record_id : summer.pageParam.record_id
		},
		methods : {
			//根据当前班次加载时间选择器
			loadTimePicker : function() {
				switch(this.shift+"") {
				case '0':
					this.startHour = 0;
					this.endHour = 7;
					break;
				case '1':
					this.startHour = 8;
					this.endHour = 15;
					break;
				case '2':
					this.startHour = 16;
					this.endHour = 23;
					break;
				default:
					this.startHour = 0;
					this.endHour = 0;
					break;
				}
			},
			//获取真id判别是新增还是修改
			fillPage : function() {
				if (this.realid + "" != "new") {
					//更新之前的过船信息&原表单填充
					var sql = "select * from goboat where pk_row_id='" + this.realid + "' and pk_father_row_id='" + this.record_id + "'";
					var param = {
						"db" : 'hzlqzw.db', //数据库名称
						"sql" : sql, //查询条件
						"startIndex" : 0, //可选参数 起始记录索引号(含)
					}

					var list = summer.UMSqlite.query(param);
					var list1 = eval(list);

					if (list1 != null && list1.length != 0) {
						var obj = list1[0];
						if (!objIsEmpty(obj["pass_time"])) {
							this.passTime = obj["pass_time"];
						}
						if (!objIsEmpty(obj["pass_type"])) {
							this.passType = obj["pass_type"];
						}
						if (!objIsEmpty(obj["pass_num"])) {
							this.passNum = obj["pass_num"];
						}
						if (!objIsEmpty(obj["switch_time"])) {
							this.switchTime = obj["switch_time"];
						}
						if (!objIsEmpty(obj["direction"])) {
							this.direction = obj["direction"];
						}
					}
				} else {
					//新增过船信息
				}
			},
			//生成时间ID
			timeId : function() {
				var iter;
				var myDate = new Date();
				//生成时间戳并去除其中的斜杠
				var mytime = myDate.toLocaleDateString().toString().trim().replace(/\//g, "");
				if (isEmpty(summer.getStorage('boat_iter')))
					iter = "001";
				else {
					//去零再累加
					iter = summer.getStorage('boat_iter').replace(/\b(0+)/gi, "") + 1;
					var l = iter.toString().length;
					if (iter.length < 3) {
						//循环补零
						for (var i = 0; i < 3 - l; i++)
							iter = "0" + iter.toString().trim();
					}
				}
				//把累加器存回手机存储
				summer.setStorage('boat_iter', iter);
				//'i'用来分隔时间戳和累加器
				return (mytime + 'i' + iter);
			},
			onValueChange : function(picker, value) {
			},
			//时间选择器确认方法
			handleTimeConfirm : function(value) {
				this.passTime = value;
			},
			open : function(picker) {
				this.$refs[picker].open();
			},
			// 返回
			goback : function() {
				roads.confirmClose();
			},
			// 暂存表单
			saveForm : function() {
				//根据完成度来做暂存
				var fillCompletion = (isEmpty(this.passTime) ? 0 : 1) + (isEmpty(this.passType) ? 0 : 1) + (isEmpty(this.passNum) ? 0 : 1) + (isEmpty(this.switchTime) ? 0 : 1) + (isEmpty(this.direction) ? 0 : 1);
				if (fillCompletion) {
					var sql = null;
					if (this.realid == "new") {
						//新增-->插入新数据
						//生成新的时间id作为真id
						sql = "insert into goboat (pk_row_id,pk_father_row_id,pass_time,pass_type,pass_num,switch_time,direction,shift,fill_completion) values ('" + this.timeId() + "','" + this.record_id + "','" + this.passTime + "','" + this.passType + "','" + this.passNum + "','" + this.switchTime + "','" + this.direction + "','" + this.shift + "','" + (fillCompletion == 5 ? 1 : 0) + "')";
					} else {
						//修改-->更新老数据
						sql = "update goboat set pass_time='" + this.passTime + "',pass_type='" + this.passType + "',pass_num='" + this.passNum + "',switch_time='" + this.switchTime + "',direction='" + this.direction + "',fill_completion='" + (fillCompletion == 5 ? 1 : 0) + "' where pk_row_id='" + this.realid + "' and pk_father_row_id='" + this.record_id + "'";
					}
					var param = {
						"db" : 'hzlqzw.db',
						"sql" : sql
					}
					summer.UMSqlite.execSql(param);
					roads.execScript("goBoat", "vue.dbQuery", "");
					roads.closeWin();
				} else {
					UM.toast({
						title : '友情提示:',
						text : '没有任何填写的数据需保存',
						duration : 3000
					});
				}
			}
		},
		mounted : function() {
			// 加载数据...
			this.$nextTick(function() {
				//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//加载时间选择器
				this.loadTimePicker();
				//初始化报表
				this.fillPage();
			})
		}
	})
}