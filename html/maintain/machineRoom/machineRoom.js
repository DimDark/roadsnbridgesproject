/*by zhuhy*/
summerready = function() {
	initPage();
}
//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}
};
//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}

var vue = null;

function initPage() {
	vue = new Vue({
		el : '#index',
		data : {
			dept : summer.pageParam.dept,
			module_id : summer.pageParam.module_id,
			module_title : summer.pageParam.module_title,
			record_id : parseInt(summer.pageParam.record_id),
			popupVisible : false,
			equips : null,
			equipName : "",
			morningClass : 0,
			checkEquip : "",
			hygieneCondition : "",
			checkman : "",
			startHour : 0,
			endHour : 23,
			startDate : new Date(),
			endDate : new Date(),
			fillInTime : new Date().format("hh:mm"),
			fillInDate : new Date().format("yyyy-MM-dd"),
			datePickerValue : new Date(),
			timePickerValue : new Date().format("hh:mm"),
		},
		methods : {
			//初始化数据库
			initDB : function() {
				//建表
				var sql = "create table if not exists " + this.module_id + " (pk_row_id INTEGER PRIMARY KEY AUTOINCREMENT, equip_name VARCHAR UNIQUE, fillin_date VARCHAR, morning_class VARCHAR, fillin_time VARCHAR, check_equip VARCHAR, hygiene_condition VARCHAR, check_man VARCHAR, fill_completion INTEGER)";
				var param = {
					"db" : 'hzlqzw.db',
					"sql" : sql
				}
				summer.UMSqlite.execSql(param);
			},
			// picker view 滚动时触发
			onValueChange : function(picker, value) {
			},
			confirmChange : function() {
				this.equipName = this.$refs.equiPicker.getValues()[0]['text'];
				this.popupVisible = false;
			},
			open : function(picker) {
				this.$refs[picker].open();
			},
			//初始化设备选择器
			initEquips : function() {
				var readContent = summer.readFile("equips_maintain/machineroom.txt");
				if (readContent != null && readContent != "") {
					this.equips = [{
						values : ($summer.isJSONObject(readContent) ? readContent : JSON.parse(readContent))
					}];
				} else {
					her.loading("加载中，请稍候..");
					var param = {
						"type" : this.module_id
					};
					this.queryData(param);
				}
			},
			//查询基础数据
			queryData : function(param) {
				/*
				roads.ajaxRequest({
				type : "get",
				url : "/mob/mobAdd2",
				param : param,
				header : ""
				}, this.querySuccess, this.queryFail);*/

				////////////////////////////////////////////假装获取equips/////////////////////////////////////////
				var receive = [{
					"value" : "0",
					"text" : "胜利河水闸"
				}, {
					"value" : "1",
					"text" : "大关水闸"
				}, {
					"value" : "2",
					"text" : "小和山水闸"
				}];
				this.equips = [{
					values : receive
				}];
				summer.writeFile("equips_maintain/machineroom.txt", receive);
				her.loaded();
			},
			//查询成功
			querySuccess : function(data) {
				her.loaded();
				if (data.code == "1" || data.code == 1) {
					this.equips = [{
						values : data.msg
					}];
					summer.writeFile("equips_maintain/machineroom.txt", data.msg);
				} else if (data.code == "0" || data.code == 0) {
					UM.toast({
						"title" : "加载失败",
						"text" : data.msg,
						"duration" : 3000
					});
				}
			},
			//查询失败
			queryFail : function() {
				her.loaded();
				UM.toast({
					"title" : "加载失败",
					"text" : "请检查网络",
					"duration" : 3000
				});
			},
			//生成日期选择器选择范围
			dateRange : function(day) {
				var time = new Date();
				time.setDate(time.getDate() + day);
				var y = time.getFullYear();
				var m = time.getMonth() + 1;
				var d = time.getDate();
				return new Date(y + "-" + m + "-" + d);
			},
			//初始化表单
			fillPage : function() {
				if (this.record_id >= 0) {
					var sql = "select * from " + this.module_id + " where pk_row_id='" + this.record_id + "'";
					var param = {
						"db" : 'hzlqzw.db', //数据库名称
						"sql" : sql, //查询条件
						"startIndex" : 0, //可选参数 起始记录索引号(含)
					}
					var list = summer.UMSqlite.query(param);
					var list1 = eval(list);

					if (list1 != null) {
						var obj = list1[0];
						if (!objIsEmpty(obj["equip_name"])) {
							this.equipName = obj["equip_name"];
						}
						if (!objIsEmpty(obj["fillin_date"])) {
							this.fillInDate = obj["fillin_date"];
						}
						if (!objIsEmpty(obj["morning_class"])) {
							this.morningClass = parseInt(obj["morning_class"]);
						}
						if (!objIsEmpty(obj["fillin_time"])) {
							this.fillInTime = obj["fillin_time"];
						}
						if (!objIsEmpty(obj["check_equip"])) {
							this.checkEquip = obj["check_equip"];
						}
						if (!objIsEmpty(obj["hygiene_condition"])) {
							this.hygieneCondition = obj["hygiene_condition"];
						}
						if (!objIsEmpty(obj["check_man"])) {
							this.checkman = obj["check_man"];
						}
					}
				}
			},
			//日期选择器确认方法
			handleDateConfirm : function(value) {
				this.fillInDate = new Date(value).format("yyyy-MM-dd");
			},
			//时间选择器确认方法
			handleTimeConfirm : function(value) {
				this.fillInTime = value;
			},
			// 返回
			goback : function() {
				roads.confirmClose();
			},
			// 暂存表单
			saveForm : function() {
				//根据完成度来做暂存
				var fillCompletion = (isEmpty(this.equipName) ? 0 : 1) + (isEmpty(this.fillInDate) ? 0 : 1) + (isEmpty(this.morningClass) ? 0 : 1) + (isEmpty(this.fillInTime) ? 0 : 1) + (isEmpty(this.checkEquip) ? 0 : 1) + (isEmpty(this.hygieneCondition) ? 0 : 1) + (isEmpty(this.checkman) ? 0 : 1);
				if (fillCompletion) {
					if ((isEmpty(this.equipName) ? 0 : 1)) {
						var sql = null;
						if (this.record_id < 0)
							sql = "insert or replace into " + this.module_id + "(equip_name,fillin_date,morning_class,fillin_time,check_equip,hygiene_condition,check_man,fill_completion)values('" + this.equipName + "','" + this.fillInDate + "','" + this.morningClass + "','" + this.fillInTime + "','" + this.checkEquip + "','" + this.hygieneCondition + "','" + this.checkman + "','" + (fillCompletion == 7 ? 1 : 0) + "')";
						else
							sql = "update " + this.module_id + " set equip_name='" + this.equipName + "',fillin_date='" + this.fillInDate + "',morning_class='" + this.morningClass + "',fillin_time='" + this.fillInTime + "',check_equip='" + this.checkEquip + "',hygiene_condition='" + this.hygieneCondition + "',check_man='" + this.checkman + "',fill_completion='" + (fillCompletion == 7 ? 1 : 0) + "' where pk_row_id='" + this.record_id + "' ";
						var param = {
							"db" : 'hzlqzw.db',
							"sql" : sql
						}
						summer.UMSqlite.execSql(param);

						if (this.record_id < 0) {
							var sql1 = "select * from " + this.module_id;
							var param1 = {
								"db" : 'hzlqzw.db', //数据库名称
								"sql" : sql1, //查询条件
								"startIndex" : 0, //可选参数 起始记录索引号(含)
							}
							var list = summer.UMSqlite.query(param1);
							var jsonArray = eval(list);

							var sql2 = "update modulestab set record_num='" + jsonArray.length + "' where module_id='" + this.module_id + "'";
							var param2 = {
								"db" : 'hzlqzw.db', //数据库名称
								"sql" : sql2, //查询条件
								"startIndex" : 0, //可选参数 起始记录索引号(含)
							}
							summer.UMSqlite.execSql(param2);
						}
						summer.closeWin();
						roads.execScript("menu", "vue.formSaved", {
							"module_title" : this.module_title
						});
					} else {
						UM.toast({
							title : '友情提示:',
							text : '请务必选择设备的名称',
							duration : 3000
						});
					}
				} else {
					UM.toast({
						title : '友情提示:',
						text : '没有任何填写的数据需保存',
						duration : 3000
					});
				}
			},
			// 提交表单
			submitForm : function() {
				var self = this;
				var data = JSON.parse(JSON.stringify(self.$data));
				self.$toast({
					message : data,
					position : "center",
					duration : 3000
				});
			}
		},
		mounted : function() {
			// 加载数据...
			this.$nextTick(function() {
				//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//初始化数据库
				this.initDB();
				//初始化报表
				this.fillPage();
				//初始化设备选择器
				this.initEquips();
				//初始化日期选择器选择范围:前后一个月？
				this.startDate = this.dateRange(-30);
				this.endDate = this.dateRange(30);
			})
		}
	})
}