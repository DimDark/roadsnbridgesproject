/*by zhuhy*/
summerready = function() {
	initPage();
}
//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}
};

//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}

var vue = null;

function initPage() {
	vue = new Vue({
		el : '#index',
		data : {
			dept : summer.pageParam.dept,
			module_id : summer.pageParam.module_id,
			module_title : summer.pageParam.module_title,
			record_id : parseInt(summer.pageParam.record_id),
			popupVisible : false,
			fillInDate : new Date().format("yyyy-MM-dd"),
			datePickerValue : new Date(),
			startDate : new Date(),
			endDate : new Date(),
			categories : [],
			manager : "",

		},
		methods : {
			//初始化数据库
			initDB : function() {
				//建表
				/*主表: inspectRiver
				 pk_row_id: 主键*/
				var sql = "create table if not exists " + this.module_id + " (pk_row_id INTEGER PRIMARY KEY AUTOINCREMENT, fillin_date VARCHAR, manager VARCHAR, fill_completion INTEGER)";
				var param = {
					"db" : 'hzlqzw.db',
					"sql" : sql
				}
				summer.UMSqlite.execSql(param);
			},
			initCategory : function() {
				this.categories = [{
					"value" : "0",
					"text" : "屏蔽门",
					"id" : "shieldoor",
					"completion" : 0
				}, {
					"value" : "1",
					"text" : "车站机房，设备房",
					"id" : "machineRoom",
					"completion" : 0
				}, {
					"value" : "2",
					"text" : "轨行区沉水井，水沟",
					"id" : "drain",
					"completion" : 0
				}, {
					"value" : "3",
					"text" : "轨行区废水泵房",
					"id" : "pumproom",
					"completion" : 0
				}, {
					"value" : "4",
					"text" : "轨行区各种线路标志",
					"id" : "routesign",
					"completion" : 0
				}, {
					"value" : "5",
					"text" : "隧道清洁冲洗",
					"id" : "qstunnel",
					"completion" : 0
				}, {
					"value" : "6",
					"text" : "离壁沟无垃圾，杂物",
					"id" : "gutterway",
					"completion" : 0
				}, {
					"value" : "7",
					"text" : "落水管排水通畅，不堵塞",
					"id" : "downspout",
					"completion" : 0
				}, {
					"value" : "8",
					"text" : "车站站台板下，污水泵房",
					"id" : "trackArea",
					"completion" : 0
				}];
			},
			// picker view 滚动时触发
			onValueChange : function(picker, value) {
			},
			//日期选择器确认方法
			handleDateConfirm : function(value) {
				this.fillInDate = new Date(value).format("yyyy-MM-dd");
			},
			open : function(picker) {
				this.$refs[picker].open();
			},
			//生成日期选择器选择范围
			dateRange : function(day) {
				var time = new Date();
				time.setDate(time.getDate() + day);
				var y = time.getFullYear();
				var m = time.getMonth() + 1;
				var d = time.getDate();
				return new Date(y + "-" + m + "-" + d);
			},
			goCategory : function(subCate) {
				if (this.saveFormKernal()) {
					roads.openWin(this.dept, "trackAreaDetail", "trackArea/trackAreaDetail.html", {
						"module_id" : this.module_id,
						"record_id" : this.record_id
					});
				}
			},
			backfrCategory : function(params) {
				var tmpCompletion = this.categories[eval(params)["category_id"]]["completion"];
				this.categories[eval(params)["category_id"]]["completion"] = tmpCompletion.substring(0, eval(params)["sub_cate_id"]) + eval(params)["completion"] + tmpCompletion.substring(eval(params)["sub_cate_id"] + 1);
				UM.toast({
					title : '友情提示:',
					text : '数据已保存',
					duration : 3000
				});
			},
			//初始化表单
			fillPage : function() {
				if (this.record_id >= 0) {
					var sql = "select * from " + this.module_id + " where pk_row_id='" + this.record_id + "'";
					var param = {
						"db" : 'hzlqzw.db', //数据库名称
						"sql" : sql, //查询条件
						"startIndex" : 0, //可选参数 起始记录索引号(含)
					}
					var list = summer.UMSqlite.query(param);
					var list1 = eval(list);

					if (list1 != null) {
						var obj = list1[0];
						if (!objIsEmpty(obj["equip_name"]))
							this.equipName = obj["equip_name"];
						if (!objIsEmpty(obj["fillin_date"]))
							this.fillInDate = obj["fillin_date"];
						if (!objIsEmpty(obj["project_name"]))
							this.projectName = obj["project_name"];
						if (!objIsEmpty(obj["maintain_contractor"]))
							this.maintainContractor = obj["maintain_contractor"];
						if (!objIsEmpty(obj["memo"]))
							this.memo = obj["memo"];
						if (!objIsEmpty(obj["categories"]))
							this.categories = eval(obj["categories"]);
						if (!objIsEmpty(obj["inspector"]))
							this.inspector = obj["inspector"];
					}
				}
			},
			//暂存表单里
			saveFormKernal : function() {
				//根据完成度来做暂存
				var kernalResult = 0;
				var fillCompletion = (isEmpty(this.equipName) ? 0 : 1) + (isEmpty(this.fillInDate) ? 0 : 1) + (isEmpty(this.projectName) ? 0 : 1) + (isEmpty(this.maintainContractor) ? 0 : 1) + (isEmpty(this.inspector) ? 0 : 1);
				if (fillCompletion) {
					var sql = null;
					if (this.record_id < 0)
						sql = "insert or replace into " + this.module_id + "(equip_name,fillin_date,project_name,maintain_contractor,memo,categories,inspector,fill_completion)values('" + this.equipName + "','" + this.fillInDate + "','" + this.projectName + "','" + this.maintainContractor + "','" + this.memo + "','" + JSON.stringify(this.categories) + "','" + this.inspector + "','" + (fillCompletion == 6 ? 1 : 0) + "')";
					else
						sql = "update " + this.module_id + " set equip_name='" + this.equipName + "',fillin_date='" + this.fillInDate + "',project_name='" + this.projectName + "',maintain_contractor='" + this.maintainContractor + "',memo='" + this.memo + "',categories='" + JSON.stringify(this.categories) + "',inspector='" + this.inspector + "',fill_completion='" + (fillCompletion == 6 ? 1 : 0) + "' where pk_row_id='" + this.record_id + "'";

					var param = {
						"db" : 'hzlqzw.db',
						"sql" : sql
					}
					summer.UMSqlite.execSql(param);

					if (this.record_id < 0) {
						var sql1 = "select * from " + this.module_id;
						var param1 = {
							"db" : 'hzlqzw.db', //数据库名称
							"sql" : sql1, //查询条件
							"startIndex" : 0, //可选参数 起始记录索引号(含)
						}
						var list = summer.UMSqlite.query(param1);
						var jsonArray = eval(list);
						if (jsonArray.length > 0) {
							//插入成功
							kernalResult = 1;
							this.record_id = jsonArray.length;
						}

						var sql2 = "update modulestab set record_num='" + jsonArray.length + "' where module_id='" + this.module_id + "'";
						var param2 = {
							"db" : 'hzlqzw.db', //数据库名称
							"sql" : sql2, //查询条件
						}
						summer.UMSqlite.execSql(param2);
					} else {
						kernalResult = 1;
					}
				} else {
					UM.toast({
						title : '抱歉:',
						text : '没有填写任何的数据,无法执行操作。',
						duration : 3000
					});
				}
				return kernalResult;
			},
			// 暂存表单
			saveForm : function() {
				if (this.saveFormKernal()) {
					summer.closeWin();
					roads.execScript("menu", "vue.formSaved", {
						"module_title" : this.module_title
					});
				}
			},
			// 提交表单
			submitForm : function() {
				var self = this;
				var data = JSON.parse(JSON.stringify(self.$data));
				self.$toast({
					message : data,
					position : "center",
					duration : 3000
				});
			},
			// 返回
			goback : function() {
				roads.confirmClose();
			},
		},
		mounted : function() {
			// 加载数据...
			document.addEventListener("backbutton", this.goback, false);

			//初始化数据库
			this.initDB();
			//初始化数据库
			this.initCategory();
			//初始化日期选择器选择范围:前后一个月？
			this.startDate = this.dateRange(-30);
			this.endDate = this.dateRange(30);
			//初始化报表
			this.fillPage();
		}
	})
}