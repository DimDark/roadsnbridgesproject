/*by zhuhy*/
summerready = function() {
	initPage();
}
//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}
};
//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}

var vue = null;

function initPage() {
	vue = new Vue({
		el : '#index',
		data : {
			dept : summer.pageParam.dept,
			module_id : summer.pageParam.module_id,
			module_title : summer.pageParam.module_title,
			record_id : parseInt(summer.pageParam.record_id),

			//起始日期
			beginDate : new Date().format("yyyy-MM-dd"),
			beignDatePickerValue : new Date(),
			beginStart : new Date(),
			beginEnd : new Date(),
			//结束日期
			stopDate : new Date().format("yyyy-MM-dd"),
			stopDatePickerValue : new Date(),
			stopStart : new Date(),
			stopEnd : new Date(),

			workCondition : "",
			picNum : 0,
			picPaths : [],
			memo : ""
		},
		methods : {
			//初始化数据库
			initDB : function() {
				//建表
				var sql = "create table if not exists " + this.module_id + " (pk_row_id INTEGER PRIMARY KEY AUTOINCREMENT, begin_date VARCHAR, stop_date VARCHAR, work_condition VARCHAR,pic_num INTEGER,pic_paths VARCHAR,memo VARCHAR,fill_completion INTEGER)";
				var param = {
					"db" : 'hzlqzw.db',
					"sql" : sql
				}
				summer.UMSqlite.execSql(param);
			},
			// 打开日期picker 视图
			open : function(picker) {
				this.$refs[picker].open();
			},
			handleDateConfirm : function(tag, value) {
				//console.log(tag);
				tag == "begin" ? this.beginDate = value.format("yyyy-MM-dd") : this.stopDate = value.format("yyyy-MM-dd");
			},
			//初始化表单
			fillPage : function() {
				if (this.record_id >= 0) {
					var sql = "select * from " + this.module_id + " where pk_row_id='" + this.record_id + "'";
					var param = {
						"db" : 'hzlqzw.db', //数据库名称
						"sql" : sql, //查询条件
						"startIndex" : 0, //可选参数 起始记录索引号(含)
					}
					var list = summer.UMSqlite.query(param);
					var list1 = eval(list);

					if (list1 != null) {
						var obj = list1[0];
						if (!objIsEmpty(obj["begin_date"]))
							this.beginDate = obj["begin_date"];
						if (!objIsEmpty(obj["stop_date"]))
							this.stopDate = obj["stop_date"];
						if (!objIsEmpty(obj["work_condition"]))
							this.workCondition = obj["work_condition"];
						if (!objIsEmpty(obj["pic_num"]))
							this.picNum = obj["pic_num"];
						if (!objIsEmpty(obj["pic_paths"]))
							this.picPaths = eval(obj["pic_paths"]);
						if (!objIsEmpty(obj["memo"]))
							this.memo = obj["memo"];
					}
				}
			},
			//前往图片总览页
			goPicCollection : function() {
				roads.openWin("util", "picCollection", "picCollection/picCollection.html", {
					ownerTag : 0,
					picPaths : this.picPaths,
					amountLimit : 10,
					module_id : this.module_id,
					module_title : this.module_title,
					record_id : this.record_id,
					win_id : "trackAreaDetail"
				});
			},
			//从图片收集页返回
			backfrCollection : function(jsonStr) {
				this.picPaths = jsonStr["picPaths"];
				this.picNum = jsonStr["picPaths"].length;
			},
			// 暂存表单
			saveForm : function() {
				var fillCompletion = (isEmpty(this.beginDate) ? 0 : 1) + (isEmpty(this.stopDate) ? 0 : 1) + (isEmpty(this.workCondition) ? 0 : 1) + (this.picNum > 0 ? 1 : 0) + (isEmpty(this.memo) ? 0 : 1);
				if (fillCompletion) {
					var sql = "insert or replace into " + this.module_id + "(pk_row_id,begin_date,stop_date,work_condition,pic_num,pic_paths,memo,fill_completion)values('1','" + this.beginDate + "','" + this.stopDate + "','" + this.workCondition + "','" + this.picNum + "','" + JSON.stringify(this.picPaths) + "','" + this.memo + "','" + (fillCompletion == 5 ? 1 : 0) + "')";
					var param = {
						"db" : 'hzlqzw.db',
						"sql" : sql,
						"startIndex" : 0,
					}
					summer.UMSqlite.execSql(param);

					if (this.record_id < 0) {
						var sql1 = "select * from " + this.module_id;
						var param1 = {
							"db" : 'hzlqzw.db', //数据库名称
							"sql" : sql1, //查询条件
							"startIndex" : 0, //可选参数 起始记录索引号(含)
						}
						var list = summer.UMSqlite.query(param1);
						var jsonArray = eval(list);

						var sql2 = "update modulestab set record_num='" + jsonArray.length + "' where module_id='" + this.module_id + "'";
						var param2 = {
							"db" : 'hzlqzw.db', //数据库名称
							"sql" : sql2, //查询条件
						}
						summer.UMSqlite.execSql(param2);
					}

					summer.closeWin();
					roads.execScript("menu", "vue.formSaved", {
						"module_title" : this.module_title
					});
				} else {
					UM.toast({
						title : '抱歉:',
						text : '没有填写任何的数据,无法执行操作。',
						duration : 3000
					});
				}
			},
			// 返回
			goback : function() {
				roads.confirmClose();
			},

			manyfileupload : function() {
				var fileArray = [];
				var params = {};
				for (var i = 0; i < this.g_path.length; i++) {
					var item = {
						fileURL : this.g_path[i].path,
						type : "image/jpeg",
						name : "file" + this.g_path[i].jid
					};
					fileArray.push(item);
				}
				roads.fileUpload({
					url : "/dynamic/upPic",
					fileArray : fileArray,
					params : params
				}, function(ret) {
					var imgSrcs = ret.data;
					conmmitForm(imgSrcs);
				}, function() {
					summer.toast({
						msg : '上传图片失败'
					});
				});
			}
		},
		mounted : function() {
			// 加载数据...
			this.$nextTick(function() {
				//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//初始化数据库
				this.initDB();
				//初始化报表
				this.fillPage();
			})
		}
	});
}