/*by zhuhy*/
var vue = null;

summerready = function() {
	// 构造控件Vue实例
	vue = new Vue({
		el : '#index',
		data : {
			dept : summer.pageParam.dept,
			modules : [],
			checkAll : false,
			timeOutEvent : 0,
			touchStatus : 0
		},
		mounted : function() {
			this.$nextTick(function() {
				//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);
				this.initModules();
			})
		},
		methods : {
			//初始化模块列表
			initModules : function() {
				var sql = "select * from modulestab where dept='" + this.dept + "' and record_num > 0";
				var param = {
					"db" : 'hzlqzw.db', //数据库名称
					"sql" : sql, //查询条件
					"startIndex" : 0, //可选参数 起始记录索引号(含)
				}
				var list = summer.UMSqlite.query(param);
				var jsonArray = eval(list);
				if (jsonArray != null && jsonArray != undefined && jsonArray.length != 0) {
					$.each(jsonArray, function(i, obj) {
						obj["checkStatus"] = false;
					});
					this.modules = jsonArray;
				}
			},
			//暂存条目点击
			cacheClick : function(index) {
				var item = this.modules[index];
				if (parseInt(item["with_equip"])) {
					roads.openWin("menu", "cacheSubList", "cache/cacheSubList.html", {
						"dept" : this.dept,
						"module_id" : item["module_id"],
						"module_title" : item["module_title"]
					});
				} else {
					var sql = "select * from " + item["module_id"];
					var param = {
						"db" : 'hzlqzw.db', //数据库名称
						"sql" : sql, //查询条件
						"startIndex" : 0, //可选参数 起始记录索引号(含)
					}
					var list = summer.UMSqlite.query(param);
					var list1 = eval(list);

					if (list1 != null) {
						roads.openWin(this.dept, item["module_id"] + "", item["module_id"] + "/" + item["module_id"] + ".html", {
							"dept" : this.dept,
							"module_id" : item["module_id"],
							"module_title" : item["module_title"],
							"record_id" : list1[0]["pk_row_id"]
						});
					}
				}
			},
			//清除暂存
			clearCache : function() {
				$.each(this.modules, function(i, obj) {
					obj["checkStatus"] = false;
				});
				$("#fadeinComponent").fadeIn(400);
			},
			//取消按钮点击事件
			cancelMessageBox : function() {
				$("#fadeinComponent").fadeOut(400);
			},
			//确定按钮点击事件
			confirmMessageBox : function() {
			},
			// 返回
			goback : function() {
				roads.closeWin();
			},
		},
		watch : {
			//删除记录弹出框选择状态监听
			checkAll : function(val) {
				if (val) {
					$.each(this.modules, function(i, obj) {
						obj["checkStatus"] = true;
					});
				} else {
					var flag = true;
					$.each(this.modules, function(i, obj) {
						if (!obj["checkStatus"])
							flag = false;
					});
					if (flag) {
						$.each(this.modules, function(i, obj) {
							obj["checkStatus"] = false;
						});
					}
				}
			},
			modules : {
				handler : function(nValue, oValue) {
					if (nValue != null && nValue != undefined && nValue.length != 0) {
						$.each(nValue, function(i, obj) {
							if (!obj["checkStatus"])
								vue.checkAll = false;
						});
					}
				},
				immediate : true,
				deep : true
			}
		},
	});
}