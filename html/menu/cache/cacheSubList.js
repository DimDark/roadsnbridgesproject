/*by zhuhy*/
summerready = function() {
	initPage();
}
//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}
};

//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}

var vue = null;

function initPage() {
	vue = new Vue({
		el : '#index',
		data : {
			dept : summer.pageParam.dept,
			module_id : summer.pageParam.module_id,
			module_title : summer.pageParam.module_title,
			records : []
		},
		methods : {
			initRecords : function() {
				var sql = "select * from " + this.module_id;
				var param = {
					"db" : 'hzlqzw.db', //数据库名称
					"sql" : sql, //查询条件
					"startIndex" : 0, //可选参数 起始记录索引号(含)
				}
				var list = summer.UMSqlite.query(param);
				var jsonArray = eval(list);
				if (jsonArray != null && jsonArray != undefined && jsonArray.length != 0) {
					this.records = jsonArray;
				}
			},
			//暂存信息点击修改事件
			itemClick : function(index) {
				var item = this.records[index];
				roads.openWin(this.dept, this.module_id, this.module_id + "/" + this.module_id + ".html", {
					"dept" : this.dept,
					"module_id" : this.module_id,
					"module_title" : this.module_title,
					"record_id" : item["pk_row_id"]
				});
			},
			//暂存信息滑动删除事件
			deleteItem : function(index) {
				var item = this.records[index];
				UM.confirm({
					title : "确认删除",
					text : "确认要删除本条信息吗？删除后将无法撤销。",
					btnText : ["取消", "确定"],
					overlay : true,
					duration : 2000,
					cancle : function() {
					},
					ok : function() {
						var querysql = "delete from " + this.module_id + " where pk_row_id= '" + item["pk_row_id"] + "'";
						var queryparam = {
							"db" : 'hzlqzw.db', //数据库名称
							"sql" : querysql, //查询条件
							"startIndex" : 0, //可选参数 起始记录索引号(含)
						}
						summer.UMSqlite.execSql(queryparam);
						vue.records.splice(index, 1);
					}
				});
			},
			// 返回
			goback : function() {
				roads.closeWin();
			},
		},
		mounted : function() {
			this.$nextTick(function() {
				//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//初始化数据
				this.initRecords();
			})
		}
	});
}