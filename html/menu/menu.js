/*by zhuhy*/
var vue = null;

summerready = function() {
	// 构造控件Vue实例
	vue = new Vue({
		el : '#index',
		data : {
			//属于哪个部门
			dept : summer.pageParam.dept,
			deptShowName : "",
			//可用的功能模块
			modules : [],
			timeOutEvent : 0,
			touchStatus : 0
		},
		mounted : function() {
			this.initSlider();
			this.initModules();
			this.initDB();
		},
		methods : {
			//初始化模块列表
			initModules : function() {
				//管养功能列表
				var overAllList = {
					"maintain" : {
						"showName" : "管养",
						"moduleList" : [{
							"module_title" : "机房巡检",
							"module_id" : "machineRoom",
							"with_equip" : 1
						}, {
							"module_title" : "泵站运行",
							"module_id" : "pumpStation",
							"with_equip" : 1
						}, {
							"module_title" : "船闸运行",
							"module_id" : "boatLock",
							"with_equip" : 1
						}, {
							"module_title" : "水闸运行",
							"module_id" : "waterLock",
							"with_equip" : 1
						}, {
							"module_title" : "区管河道运行",
							"module_id" : "manageRiver",
							"with_equip" : 0
						}, {
							"module_title" : "道路巡查",
							"module_id" : "inspectRoad",
							"with_equip" : 1
						}, {
							"module_title" : "河道巡查问题",
							"module_id" : "inspectRiver",
							"with_equip" : 1
						}, {
							"module_title" : "突发应急事件",
							"module_id" : "emergency",
							"with_equip" : 0
						}, {
							"module_title" : "车站及轨行区养护",
							"module_id" : "trackArea",
							"with_equip" : 0
						}]
					},
					"bridges" : {
						"showName" : "桥隧",
						"moduleList" : [{
							"module_title" : "泵站巡查",
							"module_id" : "qspumpcheck",
							"with_equip" : 1
						}, {
							"module_title" : "泵站生产",
							"module_id" : "qspump",
							"with_equip" : 1
						}, {
							"module_title" : "复兴大桥",
							"module_id" : "qsbridgecontrol",
							"with_equip" : 1
						}, {
							"module_title" : "隧道运行养护",
							"module_id" : "qstunnelcare",
							"with_equip" : 1
						}, {
							"module_title" : "地道巡查",
							"module_id" : "qsdpavement",
							"with_equip" : 1
						}, {
							"module_title" : "天桥巡查",
							"module_id" : "qsoverpass",
							"with_equip" : 1
						}]
					},
					"qingchun" : {
						"showName" : "庆春",
						"moduleList" : [{
							"module_title" : "泵房巡检",
							"module_id" : "qcpump",
							"with_equip" : 1
						}, {
							"module_title" : "消防巡查",
							"module_id" : "fire",
							"with_equip" : 0
						}, {
							"module_title" : "配电巡检",
							"module_id" : "qcelec",
							"with_equip" : 1
						}, {
							"module_title" : "时段监控",
							"module_id" : "qcmonitor",
							"with_equip" : 0
						}, {
							"module_title" : "突发事件处理",
							"module_id" : "qcevent",
							"with_equip" : 0
						}]
					},
					"zizhi" : {
						"showName" : "紫之",
						"moduleList" : [{
							"module_title" : "交接班",
							"module_id" : "zzhandover",
							"with_equip" : 0
						}, {
							"module_title" : "时段监控",
							"module_id" : "zzmonitor",
							"with_equip" : 0
						}, {
							"module_title" : "突发事件处理",
							"module_id" : "zzevent",
							"with_equip" : 0
						}]
					}
				};

				//根据公司获取信息
				this.modules = overAllList[this.dept]["moduleList"];
				this.deptShowName = overAllList[this.dept]["showName"];
			},
			//初始化本地数据索引数据
			initDB : function() {
				//创建模块表（包含模块ID，模块名称，模块对应的暂存条数）
				var sql = "create table if not exists modulestab (pk_row_id INTEGER PRIMARY KEY AUTOINCREMENT, dept VARCHAR, module_id VARCHAR UNIQUE, module_title VARCHAR, module_path VARCHAR, record_num INTEGER, with_equip INTEGER)";
				var param = {
					"db" : 'hzlqzw.db',
					"sql" : sql
				}
				summer.UMSqlite.execSql(param);

				//插入数据
				var deptmp = this.dept;
				var sql1 = "insert or ignore into modulestab (dept, module_id, module_title, module_path, record_num, with_equip)";
				$.each(this.modules, function(i, obj) {
					sql1 = sql1 + ( i ? " union all select '" : " select '") + deptmp + "','" + obj["module_id"] + "','" + obj["module_title"] + "','" + obj["module_path"] + "','0','" + obj["with_equip"] + "'";
				});
				var param1 = {
					"db" : 'hzlqzw.db',
					"sql" : sql1
				}
				summer.UMSqlite.execSql(param1);
			},
			//初始化轮播
			initSlider : function() {
				var list = [{
					content : "../../img/bg1.jpg"
				}, {
					content : "../../img/bg2.jpg"
				}, {
					content : "../../img/bg3.png"
				}];
				var islider = new iSlider({
					type : 'pic',
					data : list,
					dom : document.getElementById("iSlider-wrapper"),
					isLooping : true,
					animateType : 'default',
					isAutoplay : true,
					animateTime : 800
				});
				//islider.addDot();
			},
			tapHold : function(index) {
				// 这里编辑长按列表项逻辑
				this.touchStatus = 1;
			},
			moveTapHold : function(index) {
				this.touchStatus = 0;
			},
			cancelTapHold : function(index) {
				var item = this.modules[index];
				// 取消长按
				if (this.touchStatus)
					roads.openWin(this.dept, item["module_id"] + "", item["module_id"] + "/" + item["module_id"] + ".html", {
						"dept" : this.dept,
						"module_id" : item["module_id"],
						"module_title" : item["module_title"],
						"record_id" : "-999"
					});
				this.touchStatus = 0;
			},
			//前往暂存列表
			goCacheList : function() {
				roads.openWin("menu", "cacheList", "cache/cacheList.html", {
					"dept" : this.dept
				});
			},
			formSaved : function(param) {
				UM.toast({
					"title" : "数据保存：",
					"text" : eval(param)["module_title"] + "表单数据保存成功",
					"duration" : 3000
				});
			}
		}
	});
}