

summerready = function() {
	initPage();
}




//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}
};


//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}




var vue = null;

function initPage() {
//子组件 radiobutton按钮重写
var NewRadio={
    props:[
    'title',
    'gouid',
    'chaid',
    'message'
     ],
    methods:{
    //打钩
    gouHandler:function(){    
	 this.$emit("choosegou");
     },
     //打叉
     chaHandler:function(){
	  this.$emit("choosecha");
     }
    },
    watch:{
      message:function(){
       switch(this.message){
       case(0):
       var gou=this.gouid;
        var cha=this.chaid;
        var newgou="#"+gou;
        var newcha="#"+cha;
       $(newgou).attr("src","img/hgou.png");
	   $(newcha).attr("src","img/hcha.png");
       break;
        case(1):
        var gou=this.gouid;
        var cha=this.chaid;
        var newgou="#"+gou;
        var newcha="#"+cha;
       $(newgou).attr("src","img/bgou.png");
	   $(newcha).attr("src","img/hcha.png");
        break;
        case(2):
         var gou=this.gouid;
    var cha=this.chaid;
    var newgou="#"+gou;
    var newcha="#"+cha;
      $(newcha).attr("src","img/bcha.png");
	  $(newgou).attr("src","img/hgou.png");
        break;
       
       }
      }
    },
	template:'<div class="show-content-item"> '+
	           "<div  class='show-item-text'>"+
             "<span class='show-item-text-dtl'>{{title}}</span>"+
            "</div>"+
		    "<div class='show-item-choose'>"+
                  "<div class='show-item-bz'>"+                      
                       "<label  v-on:click='gouHandler'>"+
                          "<img :id='gouid'  class='show-pic-gou' src='img/hgou.png' />"+
                       "</label>"+
                   "</div>"+
                   "<div class='show-item-bz show-item-bz-last'>"+
                        "<label v-on:click='chaHandler' >"+
                          "<img :id='chaid'  class='show-pic-cha'  src='img/hcha.png' />"+
                        "</label>"+
                   "</div>"+
              "</div>"+
			"</div>"

}

//子组件 施工radiobutton按钮重写
var Sgradio={
    props:[
    'title',
    'gouid',
    'chaid',
    'controlid',
    'message'
     ],
    methods:{
    //打钩
    gouHandler:function(){
	  this.$emit("choosegou");
     },
     //打叉
     chaHandler:function(){
	 this.$emit("choosecha");
     }
    },
    watch:{
          message:function(){
       switch(this.message){
       case(0):
       var gou=this.gouid;
       var cha=this.chaid;
       var newgou="#"+gou;
       var newcha="#"+cha;
       var cont=this.controlid;
       var newcontrolid="#"+cont;
       var data=this.message;
       $(newgou).attr("src","img/hgou.png");
	   $(newcha).attr("src","img/hcha.png");
	   $(newcontrolid).addClass('item1'); 
       break;
       case(1):
       var gou=this.gouid;
       var cha=this.chaid;
       var newgou="#"+gou;
       var newcha="#"+cha;
       var cont=this.controlid;
       var newcontrolid="#"+cont;
       var data=this.message;
       $(newgou).attr("src","img/bgou.png");
	   $(newcha).attr("src","img/hcha.png");
	   $(newcontrolid).removeClass('item1');
        break;
        case(2):
					var gou = this.gouid;
					var cha = this.chaid;
					var newgou = "#" + gou;
					var newcha = "#" + cha;
					var cont = this.controlid;
					var newcontrolid = "#" + cont;
					$(newcha).attr("src", "img/bcha.png");
					$(newgou).attr("src", "img/hgou.png");
					$(newcontrolid).addClass('item1'); 

        break;
       
       }
      }
    },
	template:'<div class="show-content-item"> '+
	           "<div  class='show-item-text'>"+
             "<span class='show-item-text-dtl'>{{title}}</span>"+
            "</div>"+
		    "<div class='show-item-choose'>"+
                  "<div class='show-item-bz'>"+                      
                       "<label  v-on:click='gouHandler'>"+
                          "<img :id='gouid'  class='show-pic-gou' src='img/hgou.png' />"+
                       "</label>"+
                   "</div>"+
                   "<div class='show-item-bz show-item-bz-last'>"+
                        "<label v-on:click='chaHandler' >"+
                          "<img :id='chaid'  class='show-pic-cha'  src='img/hcha.png' />"+
                        "</label>"+
                   "</div>"+
              "</div>"+
			"</div>"

}

	vue = new Vue({
		el : '#index',
		//基础数据
		data : {
			dept : summer.pageParam.dept,
			module_id : summer.pageParam.module_id,
			module_title : summer.pageParam.module_title,
			record_id : parseInt(summer.pageParam.record_id),
			popupVisible : false,
			equips : null,
			shift:null,
			hc:0,
			hv:0,
			hi:0,
			ws:0,
			wv:0,
			wi:0,
			wcall:0,
			wcontrol:0,
			wst:0,
			cl:0,
			mon:0,
			wequip:0,
			sec:0,
			pc:0,
			rc:0,
			sc:0,
			ac:0,
			mis:0,
			mid:0,
			mif:0,
			mir:0,
			mdr:0,
			mdh:0,
			mdpr:0,
			mdpl:0,
			mdpd:0,
			mdrk:0,
			mdsd:0,
			mdss:0,
			mdsf:0,
			mdrd:0,
			mdrl:0,
			mdrr:0,
			mrwg:0,
			mrwl:0,
			mws:0,
			mwm:0,
			mcg:0,
			mcd:0,
			mwd:0,
			mwg:0,
			mwt:0,
			mwc:0,
			mwwe:0,
			mwr:0,
			mww:0,
			mwo:"",
			ais:0,
			aid:0,
			aif:0,
			air:0,
			adr:0,
			adh:0,
			adpr:0,
			adpl:0,
			adpd:0,
			adrk:0,
			adsd:0,
			adss:0,
			adsf:0,
			adrd:0,
			adrl:0,
			adrr:0,
			arwg:0,
			arwl:0,
			aws:0,
			awm:0,
			acg:0,
			acd:0,
			awd:0,
			awg:0,
			awt:0,
			awc:0,
			awwe:0,
			awr:0,
			aww:0,
			awo:"",
			equipId : "",
			equipName : "",
			startDate : new Date(),
			endDate : new Date(),
			fillInDate : new Date().format("yyyy-MM-dd"),
			datePickerValue : new Date(),
			vemo:"",
            

			
		},
		components:{
		  'componenttest':NewRadio,
		  'sgcomponent':Sgradio
		},
		 watch : {

		},
		methods : {
		//泵房选择器方法onValueChange选择  confirmChange确认  open打开
			// picker view 滚动时触发
			onValueChange : function(picker, value) {
			},
			confirmChange : function() {
			    this.equipId=this.$refs.equiPicker.getValues()[0]['value'];
				this.equipName = this.$refs.equiPicker.getValues()[0]['text'];
				this.popupVisible = false;
			},
			open : function(picker) {
				this.$refs[picker].open();
			},
			//日期选择器确认方法
			handleDateConfirm : function(value) {
				this.fillInDate = new Date(value).format("yyyy-MM-dd");
			},
			
			//初始化设备选择器
			initEquips : function() {
				var readContent = summer.readFile("equips_bridge/pumpcheck.txt");
				if (readContent != null && readContent != "") {
					this.equips = [{
						values : ($summer.isJSONObject(readContent) ? readContent : JSON.parse(readContent))
					}];
				} else {
					her.loading("加载中，请稍候..");
					var param = {
						"type" : this.module_id
					};
					this.queryData(param);
				}
			},
			//请求后台,拉取泵房设备数据,放数组
			//查询基础数据
			queryData : function(param) {
				/*
				roads.ajaxRequest({
				type : "get",
				url : "/mob/mobAdd2",
				param : param,
				header : ""
				}, this.querySuccess, this.queryFail);*/

				////////////////////////////////////////////假装获取equips/////////////////////////////////////////
				var receive = [{
					"value" : "0",
					"text" : "胜利河水闸"
				}, {
					"value" : "1",
					"text" : "大关水闸"
				}, {
					"value" : "2",
					"text" : "小和山水闸"
				}];
				this.equips = [{
					values : receive
				}];
				summer.writeFile("equips_bridge/pumpcheck.txt", receive);
				her.loaded();
			},
			//查询成功
			querySuccess : function(data) {
				her.loaded();
				if (data.code == "1" || data.code == 1) {
					this.equips = [{
						values : data.msg
					}];
					summer.writeFile("equips_maintain/machineroom.txt", data.msg);
				} else if (data.code == "0" || data.code == 0) {
					UM.toast({
						"title" : "加载失败",
						"text" : data.msg,
						"duration" : 3000
					});
				}
			},
			//查询失败
			queryFail : function() {
				her.loaded();
				UM.toast({
					"title" : "加载失败",
					"text" : "请检查网络",
					"duration" : 3000
				});
			},
			//对日期控件进行范围限定，在mounted加载中使用
			//生成日期选择器选择范围
			dateRange : function(day) {
				var time = new Date();
				time.setDate(time.getDate() + day);
				var y = time.getFullYear();
				var m = time.getMonth() + 1;
				var d = time.getDate();
				return new Date(y + "-" + m + "-" + d);
			},
			
			
			//页签选择
			gotab:function(val){
			switch(val){
			case(0):
			$('#menutab1').addClass('menuactive');
		 	$('#menutab2').removeClass('menuactive');
		 	$('#menutab3').removeClass('menuactive');
			$('#tabpanel1').removeClass('item1');
		    $('#tabpanel2').addClass('item1');
		    $('#tabpanel3').addClass('item1');
		 	break;
		 	case(1):
		 	$('#menutab1').removeClass('menuactive');
		 	$('#menutab2').addClass('menuactive');
		 	$('#menutab3').removeClass('menuactive');
			$('#tabpanel1').addClass('item1');
		    $('#tabpanel2').removeClass('item1');
		    $('#tabpanel3').addClass('item1');
		 	break;
		 	case(2):
		 	$('#menutab1').removeClass('menuactive');
		 	$('#menutab2').removeClass('menuactive');
		 	$('#menutab3').addClass('menuactive');
			$('#tabpanel1').addClass('item1');
		    $('#tabpanel2').addClass('item1');
		    $('#tabpanel3').removeClass('item1');
		 	break;
			}
			},
			//早晚班切换
			daytabgo:function(val){
			switch(val){
			case(0):			
		    $('#morn-tab-panel').removeClass('item1');
		    $('#after-tab-panel').addClass('item1');
		 	break;
		 	case(1):
		 	$('#after-tab-panel').removeClass('item1');
		    $('#morn-tab-panel').addClass('item1');
		 	break;
			}
			},	
			//赋值修改
			mygou:function(val){
			switch(val){
			case "hc":
			  vue.hc=1
			  break;
			case "hv":
			  vue.hv=1
			  break;
			case "hi":
			  vue.hi=1
			  break;
			case "ws":
			  vue.ws=1
			  break;
			case "wv":
			  vue.wv=1
			  break;
			case "wi":
			  vue.wi=1
			  break;
			case "wcall":
			  vue.wcall=1
			  break;
			case "wcontrol":
			  vue.wcontrol=1
			  break;
			case "wst":
			  vue.wst=1
			  break;
			case "cl":
			  vue.cl=1
			  break;
			case "mon":
			  vue.mon=1
			  break;
			case "wequip":
			  vue.wequip=1
			  break;
			case "sec":
			  vue.sec=1
			  break;
			case "pc":
			  vue.pc=1
			  break;
			case "rc":
			  vue.rc=1
			  break;
			case "sc":
			  vue.sc=1
			  break;
			case "ac":
			  vue.ac=1
			  break;
			case "mis":
			  vue.mis=1
			  break;
			case "mid":
			  vue.mid=1
			  break;
			case "mif":
			  vue.mif=1
			  break;
			case "mir":
			  vue.mir=1
			  break;
			case "mdr":
			  vue.mdr=1
			  break;
			case "mdh":
			  vue.mdh=1
			  break;
			case "mdpr":
			  vue.mdpr=1
			  break;
			case "mdpl":
			  vue.mdpl=1
			  break;
			case "mdpd":
			  vue.mdpd=1
			  break;
			case "mdrk":
			  vue.mdrk=1
			  break;
			case "mdsd":
			  vue.mdsd=1
			  break;
			case "mdss":
			  vue.mdss=1
			  break;
			case "mdsf":
			  vue.mdsf=1
			  break;
			case "mdrd":
			  vue.mdrd=1
			  break;
			case "mdrl":
			  vue.mdrl=1
			  break;
			case "mdrr":
			  vue.mdrr=1
			  break;
			case "mrwg":
			  vue.mrwg=1
			  break;
			case "mrwl":
			  vue.mrwl=1
			  break;
			case "mws":
			  vue.mws=1
			  break;
			case "mwm":
			  vue.mwm=1
			  break;
			case "mcg":
			  vue.mcg=1
			  break;
			case "mcd":
			  vue.mcd=1
			  break;
			case "mwd":
			  vue.mwd=1
			  break;
			case "mwg":
			  vue.mwg=1
			  break;
			case "mwt":
			  vue.mwt=1
			  break;
			case "mwc":
			  vue.mwc=1
			  break;
			case "mwwe":
			  vue.mwwe=1
			  break;
			case "mwr":
			  vue.mwr=1
			  break;
			case "mww":
			  vue.mww=1
			  break;
			case "ais":
			  vue.ais=1
			  break;
			case "aid":
			  vue.aid=1
			  break;
			case "aif":
			  vue.aif=1
			  break;
			case "air":
			  vue.air=1
			  break;
			case "adr":
			  vue.adr=1
			  break;
			case "adh":
			  vue.adh=1
			  break;
			case "adpr":
			  vue.adpr=1
			  break;
			case "adpl":
			  vue.adpl=1
			  break;
			case "adpd":
			  vue.adpd=1
			  break;
			case "adrk":
			  vue.adrk=1
			  break;
			case "adsd":
			  vue.adsd=1
			  break;
			case "adss":
			  vue.adss=1
			  break;
			case "adsf":
			  vue.adsf=1
			  break;
			case "adrd":
			  vue.adrd=1
			  break;
			case "adrl":
			  vue.adrl=1
			  break;
			case "adrr":
			  vue.adrr=1
			  break;
			case "arwg":
			  vue.arwg=1
			  break;
			case "arwl":
			  vue.arwl=1
			  break;
			case "aws":
			  vue.aws=1
			  break;
			case "awm":
			  vue.awa=1
			  break;
			case "acg":
			  vue.acg=1
			  break;
			case "acd":
			  vue.acd=1
			  break;
			case "awd":
			  vue.awd=1
			  break;
			case "awg":
			  vue.awg=1
			  break;
			case "awt":
			  vue.awt=1
			  break;
			case "awc":
			  vue.awc=1
			  break;
			case "awwe":
			  vue.awwe=1
			  break;
			case "awr":
			  vue.awr=1
			  break;
			case "aww":
			  vue.aww=1
			  break;
			}
			   
			},	
			mycha:function(val){
			switch(val){
			case "hc":
			  vue.hc=2
			  break;
			case "hv":
			  vue.hv=2
			  break;
			case "hi":
			  vue.hi=2
			  break;
			case "ws":
			  vue.ws=2
			  break;
			case "wv":
			  vue.wv=2
			  break;
			case "wi":
			  vue.wi=2
			  break;
			case "wcall":
			  vue.wcall=2
			  break;
			case "wcontrol":
			  vue.wcontrol=2
			  break;
			case "wst":
			  vue.wst=2
			  break;
			case "cl":
			  vue.cl=2
			  break;
			case "mon":
			  vue.mon=2
			  break;
			case "wequip":
			  vue.wequip=2
			  break;
			case "sec":
			  vue.sec=2
			  break;
			case "pc":
			  vue.pc=2
			  break;
			case "rc":
			  vue.rc=2
			  break;
			case "sc":
			  vue.sc=2
			  break;
			case "ac":
			  vue.ac=2
			  break;
			case "mis":
			  vue.mis=2
			  break;
			case "mid":
			  vue.mid=2
			  break;
			case "mif":
			  vue.mif=2
			  break;
			case "mir":
			  vue.mir=2
			  break;
			case "mdr":
			  vue.mdr=2
			  break;
			case "mdh":
			  vue.mdh=2
			  break;
			case "mdpr":
			  vue.mdpr=2
			  break;
			case "mdpl":
			  vue.mdpl=2
			  break;
			case "mdpd":
			  vue.mdpd=2
			  break;
			case "mdrk":
			  vue.mdrk=2
			  break;
			case "mdsd":
			  vue.mdsd=2
			  break;
			case "mdss":
			  vue.mdss=2
			  break;
			case "mdsf":
			  vue.mdsf=2
			  break;
			case "mdrd":
			  vue.mdrd=2
			  break;
			case "mdrl":
			  vue.mdrl=2
			  break;
			case "mdrr":
			  vue.mdrr=2
			  break;
			case "mrwg":
			  vue.mrwg=2
			  break;
			case "mrwl":
			  vue.mrwl=2
			  break;
			case "mws":
			  vue.mws=2
			  break;
			case "mwm":
			  vue.mwm=2
			  break;
			case "mcg":
			  vue.mcg=2
			  break;
			case "mcd":
			  vue.mcd=2
			  break;
			case "mwd":
			  vue.mwd=2;
			  vue.mwg=0;
			  vue.mwt=0;
			  vue.mwc=0;
			  vue.mwwe=0;
			  vue.mwr=0;
			  vue.mww=0;
			  vue.mwo="";
			  break;
			case "mwg":
			  vue.mwg=2
			  break;
			case "mwt":
			  vue.mwt=2
			  break;
			case "mwc":
			  vue.mwc=2
			  break;
			case "mwwe":
			  vue.mwwe=2
			  break;
			case "mwr":
			  vue.mwr=2
			  break;
			case "mww":
			  vue.mww=2
			  break;
			case "ais":
			  vue.ais=2
			  break;
			case "aid":
			  vue.aid=2
			  break;
			case "aif":
			  vue.aif=2
			  break;
			case "air":
			  vue.air=2
			  break;
			case "adr":
			  vue.adr=2
			  break;
			case "adh":
			  vue.adh=2
			  break;
			case "adpr":
			  vue.adpr=2
			  break;
			case "adpl":
			  vue.adpl=2
			  break;
			case "adpd":
			  vue.adpd=2
			  break;
			case "adrk":
			  vue.adrk=2
			  break;
			case "adsd":
			  vue.adsd=2
			  break;
			case "adss":
			  vue.adss=2
			  break;
			case "adsf":
			  vue.adsf=2
			  break;
			case "adrd":
			  vue.adrd=2
			  break;
			case "adrl":
			  vue.adrl=2
			  break;
			case "adrr":
			  vue.adrr=2
			  break;
			case "arwg":
			  vue.arwg=2
			  break;
			case "arwl":
			  vue.arwl=2
			  break;
			case "aws":
			  vue.aws=2
			  break;
			case "awm":
			  vue.awa=2
			  break;
			case "acg":
			  vue.acg=2
			  break;
			case "acd":
			  vue.acd=2
			  break;
			case "awd":
			  vue.awd=2;
			  vue.awg=0;
			  vue.awt=0;
			  vue.awc=0;
			  vue.awwe=0;
			  vue.awr=0;
			  vue.aww=0;
			  vue.awo="";
			  break;
			case "awg":
			  vue.awg=2
			  break;
			case "awt":
			  vue.awt=2
			  break;
			case "awc":
			  vue.awc=2
			  break;
			case "awwe":
			  vue.awwe=2
			  break;
			case "awr":
			  vue.awr=2
			  break;
			case "aww":
			  vue.aww=2
			  break;
			}
			},	
			// 返回事件
			goback : function() {
				roads.confirmClose();
			},	
			//暂存事件
			saveForm : function() {
			if (this.saveFormKernal()) {
					summer.closeWin();
					roads.execScript("menu", "vue.formSaved", {
						"module_title" : this.module_title
					});
				}
			},
			//保存到数据库的保单
			saveFormKernal:function(){
			//定义保存结果是否成功
			  var kernalResult = 0;
			  if(!isEmpty(vue.fillInDate)&&!isEmpty(vue.equipId)&&!isEmpty(vue.equipName)){
			  var sql = null;
			    if (this.record_id < 0){
			    sql = "insert or replace into qspumpcheck(equip_id,equip_name,fillin_date,hc,hv,"+
			                "hi,ws,wv,wi,wcall,wcontrol,wst,cl,mon,wequip,sec,pc,rc,sc,ac,"+
			                "mis,mid,mif,mir,mdr,mdh,mdpr,mdpl,mdpd,mdrk,mdsd,mdss,mdsf,mdrd,mdrl,mdrr,"+
			                "mrwg,mrwl,mws,mwm,mcg,mcd,mwd,mwg,mwt,mwc,mwwe,mwr,mww,mwo,ais,aid,"+
			                "aif,air,adr,adh,adpr,adpl,adpd,adrk,adsd,adss,adsf,adrd,adrl,adrr,arwg,arwl,"+
							"aws,awm,acg,acd,awd,awg,awt,awc,awwe,awr,aww,awo,vemo)"+
							"values('"+this.equipId+"','"+this.equipName+"','"+this.fillInDate+"',"+
							"'"+this.hc+"','"+this.hv+"','"+this.hi+"','"+this.ws+"','"+this.wv+"','"+this.wi+"',"+
							"'"+this.wcall+"','"+this.wcontrol+"','"+this.wst+"','"+this.cl+"','"+this.mon+"',"+
							"'"+this.wequip+"','"+this.sec+"','"+this.pc+"','"+this.rc+"','"+this.sc+"','"+this.ac+"',"+
							"'"+this.mis+"','"+this.mid+"','"+this.mif+"','"+this.mir+"','"+this.mdr+"','"+this.mdh+"',"+
							"'"+this.mdpr+"','"+this.mdpl+"','"+this.mdpd+"','"+this.mdrk+"','"+this.mdsd+"','"+this.mdss+"',"+
							"'"+this.mdsf+"','"+this.mdrd+"','"+this.mdrl+"','"+this.mdrr+"','"+this.mrwg+"','"+this.mrwl+"',"+
							"'"+this.mws+"','"+this.mwm+"','"+this.mcg+"','"+this.mcd+"','"+this.mwd+"','"+this.mwg+"',"+
							"'"+this.mwt+"','"+this.mwc+"','"+this.mwwe+"','"+this.mwr+"','"+this.mww+"','"+this.mwo+"',"+
							"'"+this.ais+"','"+this.aid+"','"+this.aif+"','"+this.air+"','"+this.adr+"','"+this.adh+"',"+
							"'"+this.adpr+"','"+this.adpl+"','"+this.adpd+"','"+this.adrk+"','"+this.adsd+"','"+this.adss+"',"+
							"'"+this.adsf+"','"+this.adrd+"','"+this.adrl+"','"+this.adrr+"','"+this.arwg+"','"+this.arwl+"',"+
							"'"+this.aws+"','"+this.awm+"','"+this.acg+"','"+this.acd+"','"+this.awd+"','"+this.awg+"',"+
							"'"+this.awt+"','"+this.awc+"','"+this.awwe+"','"+this.awr+"','"+this.aww+"','"+this.awo+"',"+
							"'"+this.vemo+"')";
			    }else{
			    sql = "update qspumpcheck set equip_id='" + this.equipId + "',equip_name='" + this.equipName + "',"+
			    "fillin_date='" + this.fillInDate + "',hc='"+this.hc+"',hv='"+this.hv+"',hi='"+this.hi+"',"+
			    "ws='"+this.ws+"',wv='"+this.wv+"',wi='"+this.wi+"',wcall='"+this.wcall+"',wcontrol='"+this.wcontrol+"',"+
			    "wst='"+this.wst+"',cl='"+this.cl+"',mon='"+this.mon+"',wequip='"+this.wequip+"',sec='"+this.sec+"',"+
			    "pc='"+this.pc+"',rc='"+this.rc+"',sc='"+this.sc+"',ac='"+this.ac+"',mis='"+this.mis+"',mid='"+this.mid+"',"+
			    "mif='"+this.mif+"',mir='"+this.mir+"',mdr='"+this.mdr+"',mdh='"+this.mdh+"',mdpr='"+this.mdpr+"',mdpl='"+this.mdpl+"',"+
			    "mdpd='"+this.mdpd+"',mdrk='"+this.mdrk+"',mdsd='"+this.mdsd+"',mdss='"+this.mdss+"',mdsf='"+this.mdsf+"',"+
			    "mdrd='"+this.mdrd+"',mdrl='"+this.mdrl+"',mdrr='"+this.mdrr+"',mrwg='"+this.mrwg+"',mrwl='"+this.mrwl+"',"+
			    "mws='"+this.mws+"',mwm='"+this.mwm+"',mcg='"+this.mcg+"',mcd='"+this.mcd+"',mwd='"+this.mwd+"',"+
			    "mwg='"+this.mwg+"',mwt='"+this.mwt+"',mwc='"+this.mwc+"',mwwe='"+this.mwwe+"',mwr='"+this.mwr+"',"+
			    "mww='"+this.mww+"',mwo='"+this.mwo+"',ais='"+this.ais+"',aid='"+this.aid+"',aif='"+this.aif+"',"+
			    "air='"+this.air+"',adr='"+this.adr+"',adh='"+this.adh+"',adpr='"+this.adpr+"',adpl='"+this.adpl+"',"+
			    "adpd='"+this.adpd+"',adrk='"+this.adrk+"',adsd='"+this.adsd+"',adss='"+this.adss+"',adsf='"+this.adsf+"',"+
			    "adrd='"+this.adrd+"',adrl='"+this.adrl+"',adrr='"+this.adrr+"',arwg='"+this.arwg+"',arwl='"+this.arwl+"',"+
			    "aws='"+this.aws+"',awm='"+this.awm+"',acg='"+this.acg+"',acd='"+this.acd+"',awd='"+this.awd+"',"+
			    "awg='"+this.awg+"',awt='"+this.awt+"',awc='"+this.awc+"',awwe='"+this.awwe+"',awr='"+this.awr+"',"+
			    "aww='"+this.aww+"',awo='"+this.awo+"',vemo='"+this.vemo+"'"+
			    "where pk_row_id='" + this.record_id + "'";
			    }
			    var param = {
							"db" : 'hzlqzw.db',
							"sql" : sql
						}
			    summer.UMSqlite.execSql(param);
				if (this.record_id < 0) {
							var sql1 = "select * from qspumpcheck" ;
							var param1 = {
								"db" : 'hzlqzw.db', //数据库名称
								"sql" : sql1, //查询条件
								"startIndex" : 0, //可选参数 起始记录索引号(含)
							}
							var list = summer.UMSqlite.query(param1);
							var jsonArray = eval(list);
							if (jsonArray.length > 0) {
								//插入成功
								kernalResult = 1;
								
							}

							var sql2 = "update modulestab set record_num='" + jsonArray.length + "' where module_id='qspumpcheck' ";
							var param2 = {
								"db" : 'hzlqzw.db', //数据库名称
								"sql" : sql2, //查询条件
								"startIndex" : 0, //可选参数 起始记录索引号(含)
							}
							summer.UMSqlite.execSql(param2);
						} else {
							kernalResult = 1;
						}			
									
			  }else {
					UM.toast({
						title : '抱歉:',
						text : '没有选择泵站,无法进行数据保存。',
						duration : 3000
					});
			}
			  
			 
            return kernalResult;
			},
           // 提交表单
			submitForm : function() {
				var self = this;
				var data = JSON.parse(JSON.stringify(self.$data));
				console.log(data.record_id<0);
				console.log(data);				
				alert(data.equipId);
				alert(data.equipName);
				alert(data.hc);
				alert(data.hv);
				alert(data.hi);
				self.$toast({
					message : data,
					position : "center",
					duration : 3000
				});
			},
			initDB:function(){
			
			//建表
				var sql = "create table if not exists qspumpcheck(pk_row_id INTEGER PRIMARY KEY AUTOINCREMENT,"+
				"equip_id VARCHAR UNIQUE,equip_name VARCHAR UNIQUE, fillin_date VARCHAR, hc VARCHAR,"+ 
				"hv VARCHAR,hi VARCHAR,ws VARCHAR,wv VARCHAR,wi VARCHAR,wcall VARCHAR,wcontrol VARCHAR,wst VARCHAR,"+ 
				"cl VARCHAR,mon VARCHAR,wequip VARCHAR,sec VARCHAR,pc VARCHAR,rc VARCHAR,sc VARCHAR,ac VARCHAR,"+ 
				"mis VARCHAR,mid VARCHAR,mif VARCHAR,mir VARCHAR,mdr VARCHAR,mdh VARCHAR,mdpr VARCHAR,mdpl VARCHAR,"+ 
				"mdpd VARCHAR,mdrk VARCHAR,mdsd VARCHAR,mdss VARCHAR,mdsf VARCHAR,mdrd VARCHAR,mdrl VARCHAR,mdrr VARCHAR,"+
				"mrwg VARCHAR,mrwl VARCHAR,mws VARCHAR,mwm VARCHAR, mcg VARCHAR,mcd VARCHAR,mwd VARCHAR,mwg VARCHAR,"+
				"mwt VARCHAR,mwc VARCHAR,mwwe VARCHAR,mwr VARCHAR,mww VARCHAR,mwo VARCHAR,ais VARCHAR,aid VARCHAR,"+
				"aif VARCHAR,air VARCHAR,adr VARCHAR,adh VARCHAR,adpr VARCHAR,adpl VARCHAR,adpd VARCHAR,adrk VARCHAR,"+ 
				"adsd VARCHAR,adss VARCHAR,adsf VARCHAR,adrd VARCHAR,adrl VARCHAR,adrr VARCHAR,arwg VARCHAR,arwl VARCHAR,"+ 
				"aws VARCHAR,awm VARCHAR,acg VARCHAR,acd VARCHAR,awd VARCHAR,awg VARCHAR,awt VARCHAR,awc VARCHAR,"+  
				"awwe VARCHAR,awr VARCHAR,aww VARCHAR,awo VARCHAR,vemo VARCHAR)";
				var param = {
					"db" : 'hzlqzw.db',
					"sql" : sql
				}
				summer.UMSqlite.execSql(param);
			
			
			},
			//初始化页面数据
			initePage:function(){
			  if (this.record_id >= 0) {
			        var sql = "select * from qspumpcheck where pk_row_id='" + this.record_id + "'";
			        var param = {
						"db" : 'hzlqzw.db', //数据库名称
						"sql" : sql, //查询条件
						"startIndex" : 0, //可选参数 起始记录索引号(含)
					}
					var list = summer.UMSqlite.query(param);
					var list1 = eval(list);
					if (list1 != null) {
						var obj = list1[0];
                         if (!objIsEmpty(obj["equip_id"])){
                         this.equipId = obj["equip_id"];
                         }
							
						if (!objIsEmpty(obj["equip_name"]))
							vue.equipName = obj["equip_name"];
						if (!objIsEmpty(obj["fillin_date"]))
							vue.fillInDate = obj["fillin_date"];
						if (!objIsEmpty(obj["hc"]))
							vue.hc = parseInt(obj["hc"]);
						if (!objIsEmpty(obj["hv"]))
							vue.hv = parseInt(obj["hv"]);
						if (!objIsEmpty(obj["hi"]))
							vue.hi = parseInt(obj["hi"]);
						if (!objIsEmpty(obj["ws"]))
							this.ws = parseInt(obj["ws"]);
						if (!objIsEmpty(obj["wv"]))
							this.wv = parseInt(obj["wv"]);
						if (!objIsEmpty(obj["wi"]))
							this.wi = parseInt(obj["wi"]);
						if (!objIsEmpty(obj["wcall"]))
							this.wcall = parseInt(obj["wcall"]);
						if (!objIsEmpty(obj["wcontrol"]))
							this.wcontrol = parseInt(obj["wcontrol"]);
						if (!objIsEmpty(obj["wst"]))
							this.wst = parseInt(obj["wst"]);
						if (!objIsEmpty(obj["cl"]))
							this.cl = parseInt(obj["cl"]);
						if (!objIsEmpty(obj["mon"]))
							this.mon = parseInt(obj["mon"]);
						if (!objIsEmpty(obj["wequip"]))
							this.wequip = parseInt(obj["wequip"]);
						if (!objIsEmpty(obj["sec"]))
							this.sec = parseInt(obj["sec"]);
						if (!objIsEmpty(obj["pc"]))
							this.pc = parseInt(obj["pc"]);
						if (!objIsEmpty(obj["rc"]))
							this.rc = parseInt(obj["rc"]);
						if (!objIsEmpty(obj["sc"]))
							this.sc = parseInt(obj["sc"]);
						if (!objIsEmpty(obj["ac"]))
							this.ac = parseInt(obj["ac"]);
						if (!objIsEmpty(obj["mis"]))
							this.mis = parseInt(obj["mis"]);
						if (!objIsEmpty(obj["mid"]))
							this.mid = parseInt(obj["mid"]);
						if (!objIsEmpty(obj["mif"]))
							this.mif = parseInt(obj["mif"]);
						if (!objIsEmpty(obj["mir"]))
							this.mir = parseInt(obj["mir"]);
						if (!objIsEmpty(obj["mdr"]))
							this.mdr = parseInt(obj["mdr"]);
						if (!objIsEmpty(obj["mdh"]))
							this.mdh = parseInt(obj["mdh"]);
						if (!objIsEmpty(obj["mdpr"]))
							this.mdpr = parseInt(obj["mdpr"]);
						if (!objIsEmpty(obj["mdpl"]))
							this.mdpl = parseInt(obj["mdpl"]);
						if (!objIsEmpty(obj["mdpd"]))
							this.mdpd = parseInt(obj["mdpd"]);
						if (!objIsEmpty(obj["mdrk"]))
							this.mdrk = parseInt(obj["mdrk"]);
						if (!objIsEmpty(obj["mdsd"]))
							this.mdsd = parseInt(obj["mdsd"]);
						if (!objIsEmpty(obj["mdss"]))
							this.mdss = parseInt(obj["mdss"]);
						if (!objIsEmpty(obj["mdsf"]))
							this.mdsf = parseInt(obj["mdsf"]);
						if (!objIsEmpty(obj["mdrd"]))
							this.mdrd = parseInt(obj["mdrd"]);
						if (!objIsEmpty(obj["mdrl"]))
							this.mdrl = parseInt(obj["mdrl"]);
						if (!objIsEmpty(obj["mdrr"]))
							this.mdrr = parseInt(obj["mdrr"]);
						if (!objIsEmpty(obj["mrwg"]))
							this.mrwg = parseInt(obj["mrwg"]);
						if (!objIsEmpty(obj["mrwl"]))
							this.mrwl = parseInt(obj["mrwl"]);
						if (!objIsEmpty(obj["mws"]))
							this.mws = parseInt(obj["mws"]);
						if (!objIsEmpty(obj["mwm"]))
							this.mwm = parseInt(obj["mwm"]);
						if (!objIsEmpty(obj["mcg"]))
							this.mcg = parseInt(obj["mcg"]);
						if (!objIsEmpty(obj["mcd"]))
							this.mcd = parseInt(obj["mcd"]);
						if (!objIsEmpty(obj["mwd"]))
							this.mwd = parseInt(obj["mwd"]);
						if (!objIsEmpty(obj["mwg"]))
							this.mwg = parseInt(obj["mwg"]);
						if (!objIsEmpty(obj["mwt"]))
							this.mwt = parseInt(obj["mwt"]);
						if (!objIsEmpty(obj["mwc"]))
							this.mwc = parseInt(obj["mwc"]);
						if (!objIsEmpty(obj["mwwe"]))
							this.mwwe = parseInt(obj["mwwe"]);
						if (!objIsEmpty(obj["mwr"]))
							this.mwr = parseInt(obj["mwr"]);
						if (!objIsEmpty(obj["mww"]))
							this.mww = parseInt(obj["mww"]);
						if (!objIsEmpty(obj["mwo"]))
							this.mwo = parseInt(obj["mwo"]);
						if (!objIsEmpty(obj["ais"]))
							this.ais = parseInt(obj["ais"]);
						if (!objIsEmpty(obj["aid"]))
							this.aid = parseInt(obj["aid"]);
						if (!objIsEmpty(obj["aif"]))
							this.aif = parseInt(obj["aif"]);
						if (!objIsEmpty(obj["air"]))
							this.air = parseInt(obj["air"]);
						if (!objIsEmpty(obj["adr"]))
							this.adr = parseInt(obj["adr"]);
						if (!objIsEmpty(obj["adh"]))
							this.adh = parseInt(obj["adh"]);
						if (!objIsEmpty(obj["adpr"]))
							this.adpr = parseInt(obj["adpr"]);
						if (!objIsEmpty(obj["adpl"]))
							this.adpl = parseInt(obj["adpl"]);
						if (!objIsEmpty(obj["adpd"]))
							this.adpd = parseInt(obj["adpd"]);
						if (!objIsEmpty(obj["adrk"]))
							this.adrk = parseInt(obj["adrk"]);
						if (!objIsEmpty(obj["adsd"]))
							this.adsd = parseInt(obj["adsd"]);
						if (!objIsEmpty(obj["adss"]))
							this.adss = parseInt(obj["adss"]);
						if (!objIsEmpty(obj["adsf"]))
							this.adsf = parseInt(obj["adsf"]);
						if (!objIsEmpty(obj["adrd"]))
							this.adrd = parseInt(obj["adrd"]);
						if (!objIsEmpty(obj["adrl"]))
							this.adrl = parseInt(obj["adrl"]);
						if (!objIsEmpty(obj["adrr"]))
							this.adrr = parseInt(obj["adrr"]);
						if (!objIsEmpty(obj["arwg"]))
							this.arwg = parseInt(obj["arwg"]);
						if (!objIsEmpty(obj["arwl"]))
							this.arwl = parseInt(obj["arwl"]);
						if (!objIsEmpty(obj["aws"]))
							this.aws = parseInt(obj["aws"]);
						if (!objIsEmpty(obj["awm"]))
							this.awm = parseInt(obj["awm"]);
						if (!objIsEmpty(obj["acg"]))
							this.acg = parseInt(obj["acg"]);
						if (!objIsEmpty(obj["acd"]))
							this.acd = parseInt(obj["acd"]);
						if (!objIsEmpty(obj["awd"]))
							this.awd = parseInt(obj["awd"]);
						if (!objIsEmpty(obj["awg"]))
							this.awg = parseInt(obj["awg"]);
						if (!objIsEmpty(obj["awt"]))
							this.awt = parseInt(obj["awt"]);
						if (!objIsEmpty(obj["awc"]))
							this.awc = parseInt(obj["awc"]);
						if (!objIsEmpty(obj["awwe"]))
							this.awwe = parseInt(obj["awwe"]);
						if (!objIsEmpty(obj["awr"]))
							this.awr = parseInt(obj["awr"]);
						if (!objIsEmpty(obj["aww"]))
							this.aww = parseInt(obj["aww"]);
						if (!objIsEmpty(obj["awo"]))
							this.awo = parseInt(obj["awo"]);
					    if (!objIsEmpty(obj["vemo"]))
							this.vemo = obj["vemo"]; 
					}
			        
			  }
			
			
			}
			
		},	
		beforeCreate:function(){
		
		},			
		mounted : function() {
          // 加载数据...
			this.$nextTick(function() {
				//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//初始化数据库
				this.initDB();
				//初始化报表
				this.initePage();
				//初始化设备选择器
				this.initEquips();
				//初始化日期选择器选择范围:前后一个月？
				this.startDate = this.dateRange(-30);
				this.endDate = this.dateRange(30);
			})
          
		}
	})
}