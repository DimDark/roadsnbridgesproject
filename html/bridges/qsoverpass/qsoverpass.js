

summerready = function() {
	initPage();
}

var vue = null;

function initPage() {
//子组件 radiobutton按钮重写
var NewRadio={
    props:[
    'title',
    'gouid',
    'chaid',
    'message'
     ],
    methods:{
    //打钩
    gouHandler:function(){    
	 this.$emit("choosegou");
     },
     //打叉
     chaHandler:function(){
	  this.$emit("choosecha");
     }
    },
    watch:{
      message:function(){
       switch(this.message){
       case(0):
       var gou=this.gouid;
        var cha=this.chaid;
        var newgou="#"+gou;
        var newcha="#"+cha;
       $(newgou).attr("src","img/hgou.png");
	   $(newcha).attr("src","img/hcha.png");
       break;
        case(1):
        var gou=this.gouid;
        var cha=this.chaid;
        var newgou="#"+gou;
        var newcha="#"+cha;
       $(newgou).attr("src","img/bgou.png");
	   $(newcha).attr("src","img/hcha.png");
        break;
        case(2):
         var gou=this.gouid;
    var cha=this.chaid;
    var newgou="#"+gou;
    var newcha="#"+cha;
      $(newcha).attr("src","img/bcha.png");
	  $(newgou).attr("src","img/hgou.png");
        break;
       
       }
      }
    },
	template:'<div class="show-board-item"> '+
	           "<div  class='show-item-text'>"+
             "<span class='show-item-text-dtl'>{{title}}</span>"+
            "</div>"+
		    "<div class='show-item-choose'>"+
                  "<div class='show-item-bz'>"+                      
                       "<label  v-on:click='gouHandler'>"+
                          "<img :id='gouid'  class='show-pic-gou' src='img/hgou.png' />"+
                       "</label>"+
                   "</div>"+
                   "<div class='show-item-bz show-item-bz-last'>"+
                        "<label v-on:click='chaHandler' >"+
                          "<img :id='chaid'  class='show-pic-cha'  src='img/hcha.png' />"+
                        "</label>"+
                   "</div>"+
              "</div>"+
			"</div>"

}

//子组件 施工radiobutton按钮重写
var Sgradio={
    props:[
    'title',
    'gouid',
    'chaid',
    'controlid',
    'message'
     ],
    methods:{
    //打钩
    gouHandler:function(){
    this.message=1;
	  this.$emit("choosegou");
     },
     //打叉
     chaHandler:function(){
     this.message=2;
	 this.$emit("choosecha");
     }
    },
    watch:{
          message:function(){
       switch(this.message){
       case(0):
       var gou=this.gouid;
       var cha=this.chaid;
       var newgou="#"+gou;
       var newcha="#"+cha;
       var cont=this.controlid;
       var newcontrolid="#"+cont;
       var data=this.message;
       $(newgou).attr("src","img/hgou.png");
	   $(newcha).attr("src","img/hcha.png");
	   $(newcontrolid).addClass('item1'); 
       break;
       case(1):
       var gou=this.gouid;
       var cha=this.chaid;
       var newgou="#"+gou;
       var newcha="#"+cha;
       var cont=this.controlid;
       var newcontrolid="#"+cont;
       var data=this.message;
       $(newgou).attr("src","img/bgou.png");
	   $(newcha).attr("src","img/hcha.png");
	   $(newcontrolid).removeClass('item1');
        break;
        case(2):
					var gou = this.gouid;
					var cha = this.chaid;
					var newgou = "#" + gou;
					var newcha = "#" + cha;
					var cont = this.controlid;
					var newcontrolid = "#" + cont;
					$(newcha).attr("src", "img/bcha.png");
					$(newgou).attr("src", "img/hgou.png");
					$(newcontrolid).addClass('item1'); 

        break;
       
       }
      }
    },
	template:'<div class="show-content-item"> '+
	           "<div  class='show-item-text'>"+
             "<span class='show-item-text-dtl'>{{title}}</span>"+
            "</div>"+
		    "<div class='show-item-choose'>"+
                  "<div class='show-item-bz'>"+                      
                       "<label  v-on:click='gouHandler'>"+
                          "<img :id='gouid'  class='show-pic-gou' src='img/hgou.png' />"+
                       "</label>"+
                   "</div>"+
                   "<div class='show-item-bz show-item-bz-last'>"+
                        "<label v-on:click='chaHandler' >"+
                          "<img :id='chaid'  class='show-pic-cha'  src='img/hcha.png' />"+
                        "</label>"+
                   "</div>"+
              "</div>"+
			"</div>"

}

vue = new Vue({
		el : '#index',
		//基础数据
		data : {
			dept : summer.pageParam.dept,
			module_id : summer.pageParam.module_id,
			module_title : summer.pageParam.module_title,
			record_id : parseInt(summer.pageParam.record_id),
			popupVisible : false,
			equips : null,
			shift:null,
			 timePickerValue : null,
		     passTime : null,
			hc:0,
			hv:0,
			hi:0,
			ws:0,
			wv:0,
			wi:0,
			wcall:0,
			wcontrol:0,
			wst:0,
			cl:0,
			mon:0,
			wequip:0,
			sec:0,
			pc:0,
			rc:0,
			sc:0,
			ac:0,
			mis:0,
			mid:0,
			mif:0,
			mir:0,
			mdr:0,
			mdh:0,
			mdpr:0,
			mdpl:0,
			mdpd:0,
			mdrk:0,
			mdsd:0,
			mdss:0,
			mdsf:0,
			mdrd:0,
			mdrl:0,
			mdrr:0,
			mrwg:0,
			mrwl:0,
			mws:0,
			mwm:0,
			mcg:0,
			mcd:0,
			mwd:0,
			mwg:0,
			mwt:0,
			mwc:0,
			mwwe:0,
			mwr:0,
			mww:0,
			mwo:"",
			ais:0,
			aid:0,
			aif:0,
			air:0,
			adr:0,
			adh:0,
			adpr:0,
			adpl:0,
			adpd:0,
			adrk:0,
			adsd:0,
			adss:0,
			adsf:0,
			adrd:0,
			adrl:0,
			adrr:0,
			arwg:0,
			arwl:0,
			aws:0,
			awm:0,
			acg:0,
			acd:0,
			awd:0,
			awg:0,
			awt:0,
			awc:0,
			awwe:0,
			awr:0,
			aww:0,
			workbz:0,
			checkbz:0,
			maintenbz:0,
			lookbz:0,
			awo:"",
			equipName : "",
			startDate : new Date(),
			endDate : new Date(),
			fillInDate : new Date().format("yyyy-MM-dd"),
			datePickerValue : new Date(),
			vemo:"",
            checkman:""

			
		},
		components:{
		  'componenttest':NewRadio,
		  'sgcomponent':Sgradio
		},
		 watch : {
          workbz:function(val){
            if(val==0){
            $('#workul').addClass('item1');
            $('#worki').removeClass('icon-san2');
            $('#worki').addClass('icon-san1');
            }else{
            $('#workul').removeClass('item1');
            $('#worki').removeClass('icon-san1');
            $('#worki').addClass('icon-san2');
            }
          },
          checkbz:function(val){
            if(val==0){
            $('#checkul').addClass('item1');
            $('#checki').removeClass('icon-san2');
            $('#checki').addClass('icon-san1');
            }else{
            $('#checkul').removeClass('item1');
            $('#checki').removeClass('icon-san1');
            $('#checki').addClass('icon-san2');
            }
          },
          maintenbz:function(val){
            if(val==0){
            $('#maintenul').addClass('item1');
            $('#mainteni').removeClass('icon-san2');
            $('#mainteni').addClass('icon-san1');
            }else{
            $('#maintenul').removeClass('item1');
            $('#mainteni').removeClass('icon-san1');
            $('#mainteni').addClass('icon-san2');
            }
          },
          lookbz:function(val){
            if(val==0){
            $('#lookul').addClass('item1');
            $('#looki').removeClass('icon-san2');
            $('#looki').addClass('icon-san1');
            }else{
            $('#lookul').removeClass('item1');
            $('#looki').removeClass('icon-san1');
            $('#looki').addClass('icon-san2');
            }
          }
		},
		methods : {
		//泵房选择器方法onValueChange选择  confirmChange确认  open打开
			// picker view 滚动时触发
			onValueChange : function(picker, value) {
			},
			confirmChange : function() {
				this.equipName = this.$refs.equiPicker.getValues()[0]['text'];
				this.popupVisible = false;
			},//时间选择器确认方法
			handleTimeConfirm : function(value) {
				this.passTime = value;
			},
			
			open : function(picker) {
				this.$refs[picker].open();
			},
			
			//初始化设备选择器
			initEquips : function() {
				var readContent = summer.readFile("equips_bridge/pumpcheck.txt");
				if (readContent != null && readContent != "") {
					this.equips = [{
						values : ($summer.isJSONObject(readContent) ? readContent : JSON.parse(readContent))
					}];
				} else {
					her.loading("加载中，请稍候..");
					var param = {
						"type" : this.module_id
					};
					this.queryData(param);
				}
			},
			//请求后台,拉取泵房设备数据,放数组
			//查询基础数据
			queryData : function(param) {
				/*
				roads.ajaxRequest({
				type : "get",
				url : "/mob/mobAdd2",
				param : param,
				header : ""
				}, this.querySuccess, this.queryFail);*/

				////////////////////////////////////////////假装获取equips/////////////////////////////////////////
				var receive = [{
					"value" : "0",
					"text" : "胜利河水闸"
				}, {
					"value" : "1",
					"text" : "大关水闸"
				}, {
					"value" : "2",
					"text" : "小和山水闸"
				}];
				this.equips = [{
					values : receive
				}];
				summer.writeFile("equips_bridge/pumpcheck.txt", receive);
				her.loaded();
			},
			//查询成功
			querySuccess : function(data) {
				her.loaded();
				if (data.code == "1" || data.code == 1) {
					this.equips = [{
						values : data.msg
					}];
					summer.writeFile("equips_maintain/machineroom.txt", data.msg);
				} else if (data.code == "0" || data.code == 0) {
					UM.toast({
						"title" : "加载失败",
						"text" : data.msg,
						"duration" : 3000
					});
				}
			},
			//查询失败
			queryFail : function() {
				her.loaded();
				UM.toast({
					"title" : "加载失败",
					"text" : "请检查网络",
					"duration" : 3000
				});
			},
			//对日期控件进行范围限定，在mounted加载中使用
			//生成日期选择器选择范围
			dateRange : function(day) {
				var time = new Date();
				time.setDate(time.getDate() + day);
				var y = time.getFullYear();
				var m = time.getMonth() + 1;
				var d = time.getDate();
				return new Date(y + "-" + m + "-" + d);
			},
			//日期选择器确认方法
			handleDateConfirm : function(value) {
				this.fillInDate = new Date(value).format("yyyy-MM-dd");
			},
			
			showdtl:function(val){
			switch(val){
			case("work"):
			 if(this.workbz==0){
			 this.workbz=1
			 }else{
			 this.workbz=0
			 }
			break;
			case("check"):
			 if(this.checkbz==0){
			 this.checkbz=1
			 }else{
			 this.checkbz=0
			 }
			break;
			case("mainten"):
			 if(this.maintenbz==0){
			 this.maintenbz=1
			 }else{
			 this.maintenbz=0
			 }
			break;
			case("look"):
			 if(this.lookbz==0){
			 this.lookbz=1
			 }else{
			 this.lookbz=0
			 }
			break;
			}
			
						
			},
			
			
			//早晚班切换
			daytabgo:function(val){
			switch(val){
			case(0):			
		    $('#morn-tab-panel').removeClass('item1');
		    $('#after-tab-panel').addClass('item1');
		 	break;
		 	case(1):
		 	$('#after-tab-panel').removeClass('item1');
		    $('#morn-tab-panel').addClass('item1');
		 	break;
			}
			},	
			//赋值修改
			mygou:function(val){
			switch(val){
			case "hc":
			  vue.hc=1
			  break;
			case "hv":
			  vue.hv=1
			  break;
			case "hi":
			  vue.hi=1
			  break;
			case "ws":
			  vue.ws=1
			  break;
			case "wv":
			  vue.wv=1
			  break;
			case "wi":
			  vue.wi=1
			  break;
			case "wcall":
			  vue.wcall=1
			  break;
			case "wcontrol":
			  vue.wcontrol=1
			  break;
			case "wst":
			  vue.wst=1
			  break;
			case "cl":
			  vue.cl=1
			  break;
			case "mon":
			  vue.mon=1
			  break;
			case "wequip":
			  vue.wequip=1
			  break;
			case "sec":
			  vue.sec=1
			  break;
			case "pc":
			  vue.pc=1
			  break;
			case "rc":
			  vue.rc=1
			  break;
			case "sc":
			  vue.sc=1
			  break;
			case "ac":
			  vue.ac=1
			  break;
			case "mis":
			  vue.mis=1
			  break;
			case "mid":
			  vue.mid=1
			  break;
			case "mif":
			  vue.mif=1
			  break;
			case "mir":
			  vue.mir=1
			  break;
			case "mdr":
			  vue.mdr=1
			  break;
			case "mdh":
			  vue.mdh=1
			  break;
			case "mdpr":
			  vue.mdpr=1
			  break;
			case "mdpl":
			  vue.mdpl=1
			  break;
			case "mdpd":
			  vue.mdpd=1
			  break;
			case "mdrk":
			  vue.mdrk=1
			  break;
			case "mdsd":
			  vue.mdsd=1
			  break;
			case "mdss":
			  vue.mdss=1
			  break;
			case "mdsf":
			  vue.mdsf=1
			  break;
			case "mdrd":
			  vue.mdrd=1
			  break;
			case "mdrl":
			  vue.mdrl=1
			  break;
			case "mdrr":
			  vue.mdrr=1
			  break;
			case "mrwg":
			  vue.mrwg=1
			  break;
			case "mrwl":
			  vue.mrwl=1
			  break;
			case "mws":
			  vue.mws=1
			  break;
			case "mwm":
			  vue.mwm=1
			  break;
			case "mcg":
			  vue.mcg=1
			  break;
			case "mcd":
			  vue.mcd=1
			  break;
			case "mwd":
			  vue.mwd=1
			  break;
			case "mwg":
			  vue.mwg=1
			  break;
			case "mwt":
			  vue.mwt=1
			  break;
			case "mwc":
			  vue.mwc=1
			  break;
			case "mwwe":
			  vue.mwwe=1
			  break;
			case "mwr":
			  vue.mwr=1
			  break;
			case "mww":
			  vue.mww=1
			  break;
			case "ais":
			  vue.ais=1
			  break;
			case "aid":
			  vue.aid=1
			  break;
			case "aif":
			  vue.aif=1
			  break;
			case "air":
			  vue.air=1
			  break;
			case "adr":
			  vue.adr=1
			  break;
			case "adh":
			  vue.adh=1
			  break;
			case "adpr":
			  vue.adpr=1
			  break;
			case "adpl":
			  vue.adpl=1
			  break;
			case "adpd":
			  vue.adpd=1
			  break;
			case "adrk":
			  vue.adrk=1
			  break;
			case "adsd":
			  vue.adsd=1
			  break;
			case "adss":
			  vue.adss=1
			  break;
			case "adsf":
			  vue.adsf=1
			  break;
			case "adrd":
			  vue.adrd=1
			  break;
			case "adrl":
			  vue.adrl=1
			  break;
			case "adrr":
			  vue.adrr=1
			  break;
			case "arwg":
			  vue.arwg=1
			  break;
			case "arwl":
			  vue.arwl=1
			  break;
			case "aws":
			  vue.aws=1
			  break;
			case "awm":
			  vue.awa=1
			  break;
			case "acg":
			  vue.acg=1
			  break;
			case "acd":
			  vue.acd=1
			  break;
			case "awd":
			  vue.awd=1
			  break;
			case "awg":
			  vue.awg=1
			  break;
			case "awt":
			  vue.awt=1
			  break;
			case "awc":
			  vue.awc=1
			  break;
			case "awwe":
			  vue.awwe=1
			  break;
			case "awr":
			  vue.awr=1
			  break;
			case "aww":
			  vue.aww=1
			  break;
			}
			   
			},	
			mycha:function(val){
			switch(val){
			case "hc":
			  vue.hc=2
			  break;
			case "hv":
			  vue.hv=2
			  break;
			case "hi":
			  vue.hi=2
			  break;
			case "ws":
			  vue.ws=2
			  break;
			case "wv":
			  vue.wv=2
			  break;
			case "wi":
			  vue.wi=2
			  break;
			case "wcall":
			  vue.wcall=2
			  break;
			case "wcontrol":
			  vue.wcontrol=2
			  break;
			case "wst":
			  vue.wst=2
			  break;
			case "cl":
			  vue.cl=2
			  break;
			case "mon":
			  vue.mon=2
			  break;
			case "wequip":
			  vue.wequip=2
			  break;
			case "sec":
			  vue.sec=2
			  break;
			case "pc":
			  vue.pc=2
			  break;
			case "rc":
			  vue.rc=2
			  break;
			case "sc":
			  vue.sc=2
			  break;
			case "ac":
			  vue.ac=2
			  break;
			case "mis":
			  vue.mis=2
			  break;
			case "mid":
			  vue.mid=2
			  break;
			case "mif":
			  vue.mif=2
			  break;
			case "mir":
			  vue.mir=2
			  break;
			case "mdr":
			  vue.mdr=2
			  break;
			case "mdh":
			  vue.mdh=2
			  break;
			case "mdpr":
			  vue.mdpr=2
			  break;
			case "mdpl":
			  vue.mdpl=2
			  break;
			case "mdpd":
			  vue.mdpd=2
			  break;
			case "mdrk":
			  vue.mdrk=2
			  break;
			case "mdsd":
			  vue.mdsd=2
			  break;
			case "mdss":
			  vue.mdss=2
			  break;
			case "mdsf":
			  vue.mdsf=2
			  break;
			case "mdrd":
			  vue.mdrd=2
			  break;
			case "mdrl":
			  vue.mdrl=2
			  break;
			case "mdrr":
			  vue.mdrr=2
			  break;
			case "mrwg":
			  vue.mrwg=2
			  break;
			case "mrwl":
			  vue.mrwl=2
			  break;
			case "mws":
			  vue.mws=2
			  break;
			case "mwm":
			  vue.mwm=2
			  break;
			case "mcg":
			  vue.mcg=2
			  break;
			case "mcd":
			  vue.mcd=2
			  break;
			case "mwd":
			  vue.mwd=2;
			  vue.mwg=0;
			  vue.mwt=0;
			  vue.mwc=0;
			  vue.mwwe=0;
			  vue.mwr=0;
			  vue.mww=0;
			  vue.mwo="";
			  break;
			case "mwg":
			  vue.mwg=2
			  break;
			case "mwt":
			  vue.mwt=2
			  break;
			case "mwc":
			  vue.mwc=2
			  break;
			case "mwwe":
			  vue.mwwe=2
			  break;
			case "mwr":
			  vue.mwr=2
			  break;
			case "mww":
			  vue.mww=2
			  break;
			case "ais":
			  vue.ais=2
			  break;
			case "aid":
			  vue.aid=2
			  break;
			case "aif":
			  vue.aif=2
			  break;
			case "air":
			  vue.air=2
			  break;
			case "adr":
			  vue.adr=2
			  break;
			case "adh":
			  vue.adh=2
			  break;
			case "adpr":
			  vue.adpr=2
			  break;
			case "adpl":
			  vue.adpl=2
			  break;
			case "adpd":
			  vue.adpd=2
			  break;
			case "adrk":
			  vue.adrk=2
			  break;
			case "adsd":
			  vue.adsd=2
			  break;
			case "adss":
			  vue.adss=2
			  break;
			case "adsf":
			  vue.adsf=2
			  break;
			case "adrd":
			  vue.adrd=2
			  break;
			case "adrl":
			  vue.adrl=2
			  break;
			case "adrr":
			  vue.adrr=2
			  break;
			case "arwg":
			  vue.arwg=2
			  break;
			case "arwl":
			  vue.arwl=2
			  break;
			case "aws":
			  vue.aws=2
			  break;
			case "awm":
			  vue.awa=2
			  break;
			case "acg":
			  vue.acg=2
			  break;
			case "acd":
			  vue.acd=2
			  break;
			case "awd":
			  vue.awd=2;
			  vue.awg=0;
			  vue.awt=0;
			  vue.awc=0;
			  vue.awwe=0;
			  vue.awr=0;
			  vue.aww=0;
			  vue.awo="";
			  break;
			case "awg":
			  vue.awg=2
			  break;
			case "awt":
			  vue.awt=2
			  break;
			case "awc":
			  vue.awc=2
			  break;
			case "awwe":
			  vue.awwe=2
			  break;
			case "awr":
			  vue.awr=2
			  break;
			case "aww":
			  vue.aww=2
			  break;
			}
			},	
			// 返回事件
			goback : function() {
				roads.confirmClose();
			},	
			//暂存事件
			saveForm : function() {
			},
			// 提交表单
			submitForm : function() {
				var self = this;
				var data = JSON.parse(JSON.stringify(self.$data));
				self.$toast({
					message : data,
					position : "center",
					duration : 3000
				});
			}
			
		},				
		mounted : function() {
          // 加载数据...
			this.$nextTick(function() {
				//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//初始化数据库
				//this.initDB();
				//初始化报表
				//this.fillPage();
				//初始化设备选择器
				this.initEquips();
				//初始化日期选择器选择范围:前后一个月？
				this.startDate = this.dateRange(-30);
				this.endDate = this.dateRange(30);
			})
          
		}
	})
}