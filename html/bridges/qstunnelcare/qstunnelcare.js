summerready = function() {
	initList();
}
initList = function() {
	initPage();
}
function initPage() {
	var jsonArray = [{
		"tilename" : "隧道养护时段监控记录表",
		"img" : "img/period.png",
		"url" : "html/bridges/qstunnelcare/periodmonitor/periodmonitor.html",
		"id":"periodmonitor"
	}, {
		"tilename" : "隧道实时监控数据记录表",
		"img" : "img/monitoring.png",
		"url" : "html/bridges/qstunnelcare/monitoring/monitoring.html",
		"id":"monitoring"
	}, {
		"tilename" : "隧道养护交接班记录表",
		"img" : "img/handover.png",
		"url" : "html/bridges/qstunnelcare/handover/handover.html",
		"id":"handover"
	}, {
		"tilename" : "风机运行及水泵实时记录表",
		"img" : "img/ww.png",
		"url" : "html/bridges/qstunnelcare/windandwater/windandwater.html",
		"id":"windandwater"
	}, {
		"tilename" : "隧道突发事件处理记录表",
		"img" : "img/tf.png",
		"url" : "html/bridges/qstunnelcare/qstf/qstf.html",
		"id":"qstf"
	}, {
		"tilename" : "日常巡查记录表",
		"img" : "img/daily.png",
		"url" : "html/bridges/qstunnelcare/dailycheck/dailycheck.html",
		"id":"dailycheck"
	}, {
		"tilename" : "隧道定期夜间保洁记录表",
		"img" : "img/night.png",
		"url" : "html/bridges/qstunnelcare/night/night.html",
		"id":"night"
	}];
	// 构造控件Vue实例
	var listview = new Vue({
		el : '#listview',
		data : {
			listDatas : [],
			timeOutEvent : 0
		},
		mounted : function() {
			// 组件挂载后执行...
			this.listDatas = jsonArray;
		},
		methods : {
			/* 在这里定义方法 */
			itemClick : function(index) {
				summer.openWin({
					"id" : this.listDatas[index]['id'],
					"url" : this.listDatas[index]['url'],
					"pageParam" : {
					}
				});
			}
		}
	});
}

