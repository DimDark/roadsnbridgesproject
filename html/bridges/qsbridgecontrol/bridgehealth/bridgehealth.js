

summerready = function() {
	initPage();
}

var vue = null;

function initPage() {
	vue = new Vue({
		el : '#index',
		//基础数据
		data : {
			dept : summer.pageParam.dept,
			module_id : summer.pageParam.module_id,
			module_title : summer.pageParam.module_title,
			record_id : parseInt(summer.pageParam.record_id),
			popupVisible : false,
			equips : null,
			shift:null,
	        shifts : null,
	        shiftVisible:false,
	        shiftName:"",
			equipName : "",
			startDate : new Date(),
			endDate : new Date(),
			fillInDate : new Date().format("yyyy-MM-dd"),
			datePickerValue : new Date(),
			vemo:"",
              timePickerValue : null,
		     passTime : null,

			
		},
		components:{

		},
		 watch : {

		},
		methods : {
		//泵房选择器方法onValueChange选择  confirmChange确认  open打开
			// picker view 滚动时触发
			onValueChange : function(picker, value) {
			},
			confirmChange : function() {
				this.equipName = this.$refs.equiPicker.getValues()[0]['text'];
				this.popupVisible = false;
			},
			onValueChangeShift:function(picker, value){			    
			},			
			confirmChangeShift:function() {
				this.shiftName = this.$refs.shiftPicker.getValues()[0]['text'];
				this.shiftVisible = false;
			},
			//时间选择器确认方法
			handleTimeConfirm : function(value) {
				this.passTime = value;
			},
			open : function(picker) {
				this.$refs[picker].open();
			},
			
			//初始化设备选择器
			initEquips : function() {
				var readContent = summer.readFile("equips_bridge/pumpcheck.txt");
				if (readContent != null && readContent != "") {
					this.equips = [{
						values : ($summer.isJSONObject(readContent) ? readContent : JSON.parse(readContent))
					}];
				} else {
					her.loading("加载中，请稍候..");
					var param = {
						"type" : this.module_id
					};
					this.queryData(param);
				}
			},
			//初始化班次选择器
			initOptionshift : function() {
				this.shifts = [{
					values : [{
						"value" : "0",
						"text" : "早班"
					}, {
						"value" : "1",
						"text" : "中班"
					}, {
						"value" : "2",
						"text" : "晚班"
					}]
				}];
			},
			//请求后台,拉取泵房设备数据,放数组
			//查询基础数据
			queryData : function(param) {
				/*
				roads.ajaxRequest({
				type : "get",
				url : "/mob/mobAdd2",
				param : param,
				header : ""
				}, this.querySuccess, this.queryFail);*/

				////////////////////////////////////////////假装获取equips/////////////////////////////////////////
				var receive = [{
					"value" : "0",
					"text" : "胜利河水闸"
				}, {
					"value" : "1",
					"text" : "大关水闸"
				}, {
					"value" : "2",
					"text" : "小和山水闸"
				}];
				this.equips = [{
					values : receive
				}];
				summer.writeFile("equips_bridge/pumpcheck.txt", receive);
				her.loaded();
			},
			//查询成功
			querySuccess : function(data) {
				her.loaded();
				if (data.code == "1" || data.code == 1) {
					this.equips = [{
						values : data.msg
					}];
					summer.writeFile("equips_maintain/machineroom.txt", data.msg);
				} else if (data.code == "0" || data.code == 0) {
					UM.toast({
						"title" : "加载失败",
						"text" : data.msg,
						"duration" : 3000
					});
				}
			},
			//查询失败
			queryFail : function() {
				her.loaded();
				UM.toast({
					"title" : "加载失败",
					"text" : "请检查网络",
					"duration" : 3000
				});
			},
			//对日期控件进行范围限定，在mounted加载中使用
			//生成日期选择器选择范围
			dateRange : function(day) {
				var time = new Date();
				time.setDate(time.getDate() + day);
				var y = time.getFullYear();
				var m = time.getMonth() + 1;
				var d = time.getDate();
				return new Date(y + "-" + m + "-" + d);
			},
			//日期选择器确认方法
			handleDateConfirm : function(value) {
				this.fillInDate = new Date(value).format("yyyy-MM-dd");
			},
			
			//页签选择
			gotab:function(val){
			switch(val){
			case(0):
			$('#menutab1').addClass('menuactive');
		 	$('#menutab2').removeClass('menuactive');
		 	$('#menutab3').removeClass('menuactive');
			$('#tabpanel1').removeClass('item1');
		    $('#tabpanel2').addClass('item1');
		    $('#tabpanel3').addClass('item1');
		 	break;
		 	case(1):
		 	$('#menutab1').removeClass('menuactive');
		 	$('#menutab2').addClass('menuactive');
		 	$('#menutab3').removeClass('menuactive');
			$('#tabpanel1').addClass('item1');
		    $('#tabpanel2').removeClass('item1');
		    $('#tabpanel3').addClass('item1');
		 	break;
		 	case(2):
		 	$('#menutab1').removeClass('menuactive');
		 	$('#menutab2').removeClass('menuactive');
		 	$('#menutab3').addClass('menuactive');
			$('#tabpanel1').addClass('item1');
		    $('#tabpanel2').addClass('item1');
		    $('#tabpanel3').removeClass('item1');
		 	break;
			}
			},
			//早晚班切换
			daytabgo:function(val){
			switch(val){
			case(0):			
		    $('#morn-tab-panel').removeClass('item1');
		    $('#after-tab-panel').addClass('item1');
		 	break;
		 	case(1):
		 	$('#after-tab-panel').removeClass('item1');
		    $('#morn-tab-panel').addClass('item1');
		 	break;
			}
			},	
		

			// 返回事件
			goback : function() {
				roads.confirmClose();
			},	
			//暂存事件
			saveForm : function() {
			},
			// 提交表单
			submitForm : function() {
				var self = this;
				var data = JSON.parse(JSON.stringify(self.$data));
				self.$toast({
					message : data,
					position : "center",
					duration : 3000
				});
			}
			
		},				
		mounted : function() {
          // 加载数据...
			this.$nextTick(function() {
				//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//初始化数据库
				//this.initDB();
				//初始化报表
				//this.fillPage();
				//初始化设备选择器
				this.initEquips();
				//初始化班次
				this.initOptionshift();
				//初始化日期选择器选择范围:前后一个月？
				this.startDate = this.dateRange(-30);
				this.endDate = this.dateRange(30);
			})
          
		}
	})
}