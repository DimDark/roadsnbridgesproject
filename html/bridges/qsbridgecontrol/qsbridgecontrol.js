summerready = function() {
	initList();
}
initList = function() {
	initPage();
}
function initPage() {
	var jsonArray = [{
		"tilename" : "卫生记录",
		"img" : "img/night.png",
		"url" : "html/bridges/qsbridgecontrol/bridgehealth/bridgehealth.html",
		"id":"bridgehealth"
	}, {
		"tilename" : "实时监控",
		"img" : "img/period.png",
		"url" : "html/bridges/qsbridgecontrol/bridgemonitor/bridgemonitor.html",
		"id":"bridgemonitor"
	}, {
		"tilename" : "交接班",
		"img" : "img/handover.png",
		"url" : "html/bridges/qsbridgecontrol/bridgehandover/bridgehandover.html",
		"id":"bridgehandover"
	}, {
		"tilename" : "突发事件处理",
		"img" : "img/qstf.png",
		"url" : "html/bridges/qsbridgecontrol/bridgetf/bridgetf.html",
		"id":"bridgetf"
	}];
	// 构造控件Vue实例
	var listview = new Vue({
		el : '#listview',
		data : {
			listDatas : [],
			timeOutEvent : 0
		},
		mounted : function() {
			// 组件挂载后执行...
			this.listDatas = jsonArray;
		},
		methods : {
			/* 在这里定义方法 */
			itemClick : function(index) {
				summer.openWin({
					"id" : this.listDatas[index]['id'],
					"url" : this.listDatas[index]['url'],
					"pageParam" : {
					}
				});
			}
		}
	});
}

