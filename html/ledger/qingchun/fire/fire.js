

summerready = function() {
	initPage();
}

var vue = null;

function initPage() {
//子组件 radiobutton按钮重写
var NewRadio={
    props:[
    'title',
    'message',
    'vemo'
     ],
    methods:{
    //打钩
    gouHandler:function(){    
	 this.$emit("choosegou");
     },
     //打叉
     chaHandler:function(){
	  this.$emit("choosecha");
     },
     fillvemo:function(){
     this.$emit("fillvemo");
     }
    },
   watch: { 
      message:function() {
				switch(this.message) {
				case(0):
					var gou = this.comid;
					var cha = this.comid;
					var newgou = "#" + gou+"gouimg";
					var newcha = "#" + cha+"chaimg";
					$(newgou).attr("src", "img/hgou.png");
					$(newcha).attr("src", "img/hcha.png");
					break;
				case(1):
					var comid=this.comid;
					var newgou = "#" + comid+"gouimg";
					var newcha = "#" + comid+"chaimg";
					var newcom="#" + comid+"fillid";
					$(newgou).attr("src", "img/bgou.png");
					$(newcha).attr("src", "img/hcha.png");
					$(newcom).addClass('item1');
					break;
				case(2):
					var comid=this.comid;
					var newgou = "#" + comid+"gouimg";
					var newcha = "#" + comid+"chaimg";
					var newcom="#" + comid+"fillid";
					$(newcha).attr("src", "img/bcha.png");
					$(newgou).attr("src", "img/hgou.png");
					$(newcom).removeClass('item1');
					break;

				}
			}
	},
    computed:{
       getfilldiv:function(){
       var comid = this.comid;
       comid=comid+"fillid";
       return comid;
       },
       getgouid:function(){
       var comid = this.comid;
       comid=comid+"gouimg";
       return comid;
       },
       getchaid:function(){
       var comid = this.comid;
       comid=comid+"chaimg";
       return comid;
       },
    },
	template:'<div class="show-choose-content-item">'+
	          "<div class='show-choose-title'>"+
                 "<div  class='show-choose-item-text'><span class='show-choose-item-text-dtl'>{{title}}</span>"+
                   "</div>"+
		         "<div class='show-choose-item-choose'>"+
                      "<div class='show-item-bz'>"+                      
                         "<label  v-on:click='gouHandler'>"+
                            "<img :id='getgouid'  class='show-pic-gou' src='img/hgou.png' />"+
                         "</label>"+
                      "</div>"+
                      "<div class='show-item-bz show-item-bz-last'>"+
                         "<label v-on:click='chaHandler' >"+
                            "<img :id='getchaid'  class='show-pic-cha'  src='img/hcha.png' />"+
                          "</label>"+
                      "</div>"+
                 "</div>"+
              "</div>"+
			  "<div :id='getfilldiv' class='show-choose-fill item1'>"+			  
			    "<div class='show-choose-fill-item'>"+
			      "<div class='show-choose-fill-item-title fill-font-color'>"+
			        "<span class='show-choose-item-text-dtl'>故障原因及处理情况</span>"+
			      "</div>"+
			      "<div class='show-choose-fill-item-dtl'>"+
			        "<textarea  v-bind:value=vemo v-on:change='fillvemo()'></textarea>"+
			      "</div>"+
			    "</div>"+
			  "</div>"+
			"</div>"

}


vue = new Vue({
		el : '#index',
		//基础数据
		data : {
			dept : summer.pageParam.dept,
			module_id : summer.pageParam.module_id,
			module_title : summer.pageParam.module_title,
			record_id : parseInt(summer.pageParam.record_id),
			popupVisible : false,
			popupVisibleWeather : false,
			equips : null,
			shift:null,
			shifts : null,
			shiftVisible:false,
			shiftName:"",
			equipName : "",
			startDate : new Date(),
			endDate : new Date(),
			fillInDate : new Date().format("yyyy-MM-dd"),
			datePickerValue : new Date(),
			vemo:"",
            weather : "",
			weatherDescriptions : null,
			sfshowVisible:false,
			sfshowbz:0,
			sfshow:"",
			sfshows : null,
			qsfshowVisible:false,
			qsfshowbz:0,
			qsfshow:"",
            unitVisible:false,
			units:null,
			unit:"",
			ckd:0,
			ckdvemo:"",
			checkman:"",
			xfbz:0,
			hzbz:0,
			gsbz:0,
			xwbz:0,
			rjbz:0,
			pybz:0,
			ssbz:0,
			gbbz:0,
			fgbz:0,
			weather : "",
			weatherDescriptions : null,
			popupVisibleWeather : false,
	
		},
		components:{
		  'componenttest':NewRadio,
		  

		},
		watch : {
          xfbz:function(val){
            if(val==0){
            $('#xful').addClass('item1');
            $('#xfi').removeClass('icon-san2');
            $('#xfi').addClass('icon-san1');
            }else{
            $('#xful').removeClass('item1');
            $('#xfi').removeClass('icon-san1');
            $('#xfi').addClass('icon-san2');
            }
          },
          hzbz:function(val){
            if(val==0){
            $('#hzul').addClass('item1');
            $('#hzi').removeClass('icon-san2');
            $('#hzi').addClass('icon-san1');
            }else{
            $('#hzul').removeClass('item1');
            $('#hzi').removeClass('icon-san1');
            $('#hzi').addClass('icon-san2');
            }
          },
          gsbz:function(val){
            if(val==0){
            $('#gsul').addClass('item1');
            $('#gsi').removeClass('icon-san2');
            $('#gsi').addClass('icon-san1');
            }else{
            $('#gsul').removeClass('item1');
            $('#gsi').removeClass('icon-san1');
            $('#gsi').addClass('icon-san2');
            }
          },
          xwbz:function(val){
            if(val==0){
            $('#xwul').addClass('item1');
            $('#xwi').removeClass('icon-san2');
            $('#xwi').addClass('icon-san1');
            }else{
            $('#xwul').removeClass('item1');
            $('#xwi').removeClass('icon-san1');
            $('#xwi').addClass('icon-san2');
            }
          },
          rjbz:function(val){
            if(val==0){
            $('#rjul').addClass('item1');
            $('#rji').removeClass('icon-san2');
            $('#rji').addClass('icon-san1');
            }else{
            $('#rjul').removeClass('item1');
            $('#rji').removeClass('icon-san1');
            $('#rji').addClass('icon-san2');
            }
          },
          pybz:function(val){
            if(val==0){
            $('#pyul').addClass('item1');
            $('#pyi').removeClass('icon-san2');
            $('#pyi').addClass('icon-san1');
            }else{
            $('#pyul').removeClass('item1');
            $('#pyi').removeClass('icon-san1');
            $('#pyi').addClass('icon-san2');
            }
          },
          ssbz:function(val){
            if(val==0){
            $('#ssul').addClass('item1');
            $('#ssi').removeClass('icon-san2');
            $('#ssi').addClass('icon-san1');
            }else{
            $('#ssul').removeClass('item1');
            $('#ssi').removeClass('icon-san1');
            $('#ssi').addClass('icon-san2');
            }
          },
          gbbz:function(val){
            if(val==0){
            $('#gbul').addClass('item1');
            $('#gbi').removeClass('icon-san2');
            $('#gbi').addClass('icon-san1');
            }else{
            $('#gbul').removeClass('item1');
            $('#gbi').removeClass('icon-san1');
            $('#gbi').addClass('icon-san2');
            }
          },
          fgbz:function(val){
            if(val==0){
            $('#fgul').addClass('item1');
            $('#fgi').removeClass('icon-san2');
            $('#fgi').addClass('icon-san1');
            }else{
            $('#fgul').removeClass('item1');
            $('#fgi').removeClass('icon-san1');
            $('#fgi').addClass('icon-san2');
            }
          },
         
          
        
		},
		methods : {
		//泵房选择器方法onValueChange选择  confirmChange确认  open打开
			// picker view 滚动时触发
			onValueChange : function(picker, value) {
			},
			confirmChange : function() {
				this.equipName = this.$refs.equiPicker.getValues()[0]['text'];
				this.popupVisible = false;
			},
			open : function(picker) {
				this.$refs[picker].open();
			},
			onValueChangeShift:function(picker, value){			    
			},			
			confirmChangeShift:function() {
				this.shiftName = this.$refs.shiftPicker.getValues()[0]['text'];
				this.shiftVisible = false;
			},
			onValueChangeWeather : function(picker, value) {
			},
			confirmChangeWeather : function() {
				this.weather = this.$refs.weatherPicker.getValues()[0]['text'];
				this.popupVisibleWeather = false;
			},
			confirmChangeUnit:function(){
			    this.unit = this.$refs.unitPicker.getValues()[0]['text'];
				this.unitVisible = false;
			},
			onValueChangeUnit:function(picker, value){
			
			},
			onValueChangeSfshow : function(picker, value) {
			},
			confirmChangeSfshow : function() {
			    this.sfshowbz = this.$refs.sfshowPicker.getValues()[0]['value'];
			    this.sfshow = this.$refs.sfshowPicker.getValues()[0]['text'];
				this.sfshowVisible = false;
			},
			onValueChangeQsfshow : function(picker, value) {
			},
			confirmChangeQsfshow : function() {
			    this.qsfshowbz = this.$refs.qsfshowPicker.getValues()[0]['value'];
			    this.qsfshow = this.$refs.sfshowPicker.getValues()[0]['text'];
				this.qsfshowVisible = false;
			},
			//初始化设备选择器
			initEquips : function() {
				var readContent = summer.readFile("equips_bridge/pumpcheck.txt");
				if (readContent != null && readContent != "") {
					this.equips = [{
						values : ($summer.isJSONObject(readContent) ? readContent : JSON.parse(readContent))
					}];
				} else {
					her.loading("加载中，请稍候..");
					var param = {
						"type" : this.module_id
					};
					this.queryData(param);
				}
			},
			//请求后台,拉取泵房设备数据,放数组
			//查询基础数据
			queryData : function(param) {
				/*
				roads.ajaxRequest({
				type : "get",
				url : "/mob/mobAdd2",
				param : param,
				header : ""
				}, this.querySuccess, this.queryFail);*/

				////////////////////////////////////////////假装获取equips/////////////////////////////////////////
				var receive = [{
					"value" : "0",
					"text" : "胜利河水闸"
				}, {
					"value" : "1",
					"text" : "大关水闸"
				}, {
					"value" : "2",
					"text" : "小和山水闸"
				}];
				this.equips = [{
					values : receive
				}];
				summer.writeFile("equips_bridge/pumpcheck.txt", receive);
				her.loaded();
			},
			//初始化班次选择器
			initOptionshift : function() {
				this.shifts = [{
					values : [{
						"value" : "0",
						"text" : "上午"
					}, {
						"value" : "1",
						"text" : "下午"
					}]
				}];
			},
			//初始化养护单位
			initOptionUnit : function() {
				this.units = [{
					values : [{
						"value" : "0",
						"text" : "小单位"
					}, {
						"value" : "1",
						"text" : "大单位"
					}, {
						"value" : "2",
						"text" : "中单位"
					}]
				}];
			},
			//初始化养护单位
			initOptionsfshow : function() {
				this.sfshows = [{
					values : [{
						"value" : "1",
						"text" : "是"
					}, {
						"value" : "2",
						"text" : "否"
					}]
				}];
			},
			//初始化天气选择器
			initOptionsw : function() {
				this.weatherDescriptions = [{
					values : [{
						"value" : "0",
						"text" : "晴"
					}, {
						"value" : "1",
						"text" : "雨"
					}]
				}];
			},
			//查询成功
			querySuccess : function(data) {
				her.loaded();
				if (data.code == "1" || data.code == 1) {
					this.equips = [{
						values : data.msg
					}];
					summer.writeFile("equips_maintain/machineroom.txt", data.msg);
				} else if (data.code == "0" || data.code == 0) {
					UM.toast({
						"title" : "加载失败",
						"text" : data.msg,
						"duration" : 3000
					});
				}
			},
			showdtl:function(val){
			switch(val){
			case("xf"):
			 if(this.xfbz==0){
			 this.xfbz=1
			 }else{
			 this.xfbz=0
			 }
			break;
			case("hz"):
			 if(this.hzbz==0){
			 this.hzbz=1
			 }else{
			 this.hzbz=0
			 }
			break;
			case("gs"):
			 if(this.gsbz==0){
			 this.gsbz=1
			 }else{
			 this.gsbz=0
			 }
			break;
			case("xw"):
			 if(this.xwbz==0){
			 this.xwbz=1
			 }else{
			 this.xwbz=0
			 }
			break;
			case("rj"):
			 if(this.rjbz==0){
			 this.rjbz=1
			 }else{
			 this.rjbz=0
			 }
			break;
			case("py"):
			 if(this.pybz==0){
			 this.pybz=1
			 }else{
			 this.pybz=0
			 }
			break;
			case("ss"):
			 if(this.ssbz==0){
			 this.ssbz=1
			 }else{
			 this.ssbz=0
			 }
			break;
			case("gb"):
			 if(this.gbbz==0){
			 this.gbbz=1
			 }else{
			 this.gbbz=0
			 }
			break;
			case("fg"):
			 if(this.fgbz==0){
			 this.fgbz=1
			 }else{
			 this.fgbz=0
			 }
			break;
			}
			
						
			},
			//查询失败
			queryFail : function() {
				her.loaded();
				UM.toast({
					"title" : "加载失败",
					"text" : "请检查网络",
					"duration" : 3000
				});
			},
			//对日期控件进行范围限定，在mounted加载中使用
			//生成日期选择器选择范围
			dateRange : function(day) {
				var time = new Date();
				time.setDate(time.getDate() + day);
				var y = time.getFullYear();
				var m = time.getMonth() + 1;
				var d = time.getDate();
				return new Date(y + "-" + m + "-" + d);
			},
			//日期选择器确认方法
			handleDateConfirm : function(value) {
				this.fillInDate = new Date(value).format("yyyy-MM-dd");
			},
			
				
			setvemo:function(val){
				switch(val) {
				case "ckd":
                this.ckdvemo=event.target.value;
			    break;
			   
				}
			
			},	
			
			mycha:function(val,newval) {
			switch(val) {
				case "ckd":
				//防止触发多次按到x,导致数据清空
				if(vue.ckd!=newval){
                vue.ckd=2;
                vue.ckdvemo="";
                }
			    break;
			   
				}
			},
           mygou:function(val,newval) {
				switch(val) {
				case "ckd":
                vue.ckd=1;
                vue.ckdvemo="";
			    break;
			   
				}
			},

			// 返回事件
			goback : function() {
				roads.confirmClose();
			},	
			//暂存事件
			saveForm : function() {
			},
			// 提交表单
			submitForm : function() {
				var self = this;
				var data = JSON.parse(JSON.stringify(self.$data));
				console.log(data)
			
				self.$toast({
					message : data,
					position : "center",
					duration : 3000
				});
			}
			
		},				
		mounted : function() {
          // 加载数据...
			this.$nextTick(function() {
				//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//初始化数据库
				//this.initDB();
				//初始化报表
				//this.fillPage();
				//初始化设备选择器
				this.initEquips();
				//初始化上下午选择
				this.initOptionshift();
				//初始化天气选择器
				this.initOptionsw();
				//初始化单位
				this.initOptionUnit();
				this.initOptionsfshow();
				//初始化日期选择器选择范围:前后一个月？
				this.startDate = this.dateRange(-30);
				
				this.endDate = this.dateRange(30);
			})
          
		}
	})
}