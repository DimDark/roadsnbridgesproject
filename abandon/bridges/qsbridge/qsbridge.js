

summerready = function() {
	initPage();
}

var vue = null;

function initPage() {
//子组件 radiobutton按钮重写
var NewRadio={
    props:[
    'title',
    'message',
    'fillitem',
    'mdtl',
    'mvemo',
    'comid'
     ],
    methods:{
    //打钩
    gouHandler:function(){    
	 this.$emit("choosegou");
     },
     //打叉
     chaHandler:function(){
	  this.$emit("choosecha");
     },
     filldtl:function(){
      this.$emit("filldtl");
     },
     fillvemo:function(){
      this.$emit("fillvemo");
     },
    
     
    },
   watch: { 
      message:function() {
				switch(this.message) {
				case(0):
					var gou = this.comid;
					var cha = this.comid;
					var newgou = "#" + gou+"gouimg";
					var newcha = "#" + cha+"chaimg";
					$(newgou).attr("src", "img/hgou.png");
					$(newcha).attr("src", "img/hcha.png");
					break;
				case(1):
					var comid=this.comid;
					var newgou = "#" + comid+"gouimg";
					var newcha = "#" + comid+"chaimg";
					var newcom="#" + comid+"fillid";
					$(newgou).attr("src", "img/bgou.png");
					$(newcha).attr("src", "img/hcha.png");
					$(newcom).addClass('item1');
					break;
				case(2):
					var comid=this.comid;
					var newgou = "#" + comid+"gouimg";
					var newcha = "#" + comid+"chaimg";
					var newcom="#" + comid+"fillid";
					$(newcha).attr("src", "img/bcha.png");
					$(newgou).attr("src", "img/hgou.png");
					$(newcom).removeClass('item1');
					break;

				}
			}
	},
    computed:{
       getfilldiv:function(){
       var comid = this.comid;
       comid=comid+"fillid";
       return comid;
       },
       getgouid:function(){
       var comid = this.comid;
       comid=comid+"gouimg";
       return comid;
       },
       getchaid:function(){
       var comid = this.comid;
       comid=comid+"chaimg";
       return comid;
       },
    },
	template:'<div class="show-choose-content-item">'+
	          "<div class='show-choose-title'>"+
                 "<div  class='show-choose-item-text'><span class='show-choose-item-text-dtl'>{{title}}</span>"+
                   "</div>"+
		         "<div class='show-choose-item-choose'>"+
                      "<div class='show-item-bz'>"+                      
                         "<label  v-on:click='gouHandler'>"+
                            "<img :id='getgouid'  class='show-pic-gou' src='img/hgou.png' />"+
                         "</label>"+
                      "</div>"+
                      "<div class='show-item-bz show-item-bz-last'>"+
                         "<label v-on:click='chaHandler' >"+
                            "<img :id='getchaid'  class='show-pic-cha'  src='img/hcha.png' />"+
                          "</label>"+
                      "</div>"+
                 "</div>"+
              "</div>"+
			  "<div :id='getfilldiv' class='show-choose-fill item1'>"+
			    "<div class='show-choose-fill-item'>"+
			      "<div class='show-choose-fill-item-title fill-font-color'>"+
			        "<span class='show-choose-item-text-dtl'>{{fillitem}}</span>"+
			      "</div>"+
			      "<div class='show-choose-fill-item-dtl'>"+
			        "<input type='text' v-bind:value=mdtl v-on:change='filldtl()'>"+
			      "</div>"+
			    "</div>"+
			    "<div class='show-choose-fill-item'>"+
			      "<div class='show-choose-fill-item-title fill-font-color'>"+
			        "<span class='show-choose-item-text-dtl'>病害描述</span>"+
			      "</div>"+
			      "<div class='show-choose-fill-item-dtl' v-bind:value=mvemo v-on:change='fillvemo()'>"+
			        "<input type='text' v-bind:value=mvemo v-on:change='fillvemo()' />"+
			      "</div>"+
			    "</div>"+
			  "</div>"+
			"</div>"

}

var NewRadioBtn={
    props:[
    'title',
    'message',
    'fillitem1',
    'fillitem2',
    'mdtl',
    'mvemo',
    'comid'
     ],
    methods:{
    //打钩
    gouHandler:function(){    
	 this.$emit("choosegou");
     },
     //打叉
     chaHandler:function(){
	  this.$emit("choosecha");
     },
     filldtl:function(){
      this.$emit("filldtl");
     },
     fillvemo:function(){
      this.$emit("fillvemo");
     },
    
     
    },
   watch: { 
      message:function() {
				switch(this.message) {
				case(1):
					var comid=this.comid;
					var newgou = "#" + comid+"gouimg";
					var newcha = "#" + comid+"chaimg";
					var newcom="#" + comid+"fillid";
					$(newgou).addClass('pic-gou-span-active');
					$(newcha).removeClass('pic-gou-span-active');
					$(newcom).removeClass('item1');
					break;
				case(2):
					var comid=this.comid;
					var newgou = "#" + comid+"gouimg";
					var newcha = "#" + comid+"chaimg";
					var newcom="#" + comid+"fillid";
					$(newgou).removeClass('pic-gou-span-active');
					$(newcha).addClass('pic-gou-span-active');
					$(newcom).addClass('item1');
					break;

				}
			}
	},
    computed:{
       getfilldiv:function(){
       var comid = this.comid;
       comid=comid+"fillid";
       return comid;
       },
       getgouid:function(){
       var comid = this.comid;
       comid=comid+"gouimg";
       return comid;
       },
       getchaid:function(){
       var comid = this.comid;
       comid=comid+"chaimg";
       return comid;
       },
    },
	template:'<div class="show-choose-content-item">'+
	          "<div class='show-choose-title'>"+
                 "<div  class='show-choose-item-text'><span class='show-choose-item-text-dtl'>{{title}}</span>"+
                   "</div>"+
		         "<div class='show-choose-item-choose'>"+
                      "<div class='show-item-bz show-item-bz-left'>"+                      
                         "<label  v-on:click='gouHandler'>"+
                            "<span :id='getgouid'  class='show-pic-gou-span'>有</span>"+
                         "</label>"+
                      "</div>"+
                      "<div class='show-item-bz show-item-bz-last'>"+
                         "<label v-on:click='chaHandler' >"+
                            "<span :id='getchaid'  class='show-pic-gou-span'>无</span>"+ 
                            "</label>"+
                       "</div>"+
                 "</div>"+
              "</div>"+
			  "<div :id='getfilldiv' class='show-choose-fill item1'>"+
			    "<div class='show-choose-fill-item'>"+
			      "<div class='show-choose-fill-item-title fill-font-color'>"+
			        "<span class='show-choose-item-text-dtl'>{{fillitem1}}</span>"+
			      "</div>"+
			      "<div class='show-choose-fill-item-dtl'>"+
			        "<input type='text' v-bind:value=mdtl v-on:change='filldtl()'/>"+
			      "</div>"+
			    "</div>"+
			    "<div class='show-choose-fill-item'>"+
			      "<div class='show-choose-fill-item-title fill-font-color'>"+
			        "<span class='show-choose-item-text-dtl'>{{fillitem2}}</span>"+
			      "</div>"+
			      "<div class='show-choose-fill-item-dtl' v-bind:value=mvemo v-on:change='fillvemo()'>"+
			        "<input type='text' v-bind:value=mvemo v-on:change='fillvemo()' />"+
			      "</div>"+
			    "</div>"+
			  "</div>"+
			"</div>"

}

var NewRadioChooseBtn={
    props:[
    'title',
    'message',
    'fillitem1',
    'fillitem2',
    'mdtl',
    'mvemo',
    'comid',
    'sfshow'
     ],   
    methods:{
    //打钩
    gouHandler:function(){    
	 this.$emit("choosegou");
     },
     //打叉
     chaHandler:function(){
	  this.$emit("choosecha");
     },
     filldtl:function(){
      this.$emit("filldtl");
     },
     fillvemo:function(){
      this.$emit("fillvemo");
     },
	showVisible:function(){
	 this.$emit("showvisible");
	},
	    
    },
   watch: { 
      message:function() {
				switch(this.message) {
				case(1):
					var comid=this.comid;
					var newgou = "#" + comid+"gouimg";
					var newcha = "#" + comid+"chaimg";
					var newcom="#" + comid+"fillid";
					$(newgou).addClass('pic-gou-span-active');
					$(newcha).removeClass('pic-gou-span-active');
					$(newcom).removeClass('item1');
					break;
				case(2):
					var comid=this.comid;
					var newgou = "#" + comid+"gouimg";
					var newcha = "#" + comid+"chaimg";
					var newcom="#" + comid+"fillid";
					$(newgou).removeClass('pic-gou-span-active');
					$(newcha).addClass('pic-gou-span-active');
					$(newcom).addClass('item1');
					break;

				}
			},
	
	},
    computed:{
       getfilldiv:function(){
       var comid = this.comid;
       comid=comid+"fillid";
       return comid;
       },
       getgouid:function(){
       var comid = this.comid;
       comid=comid+"gouimg";
       return comid;
       },
       getchaid:function(){
       var comid = this.comid;
       comid=comid+"chaimg";
       return comid;
       },
    },
	template:'<div class="show-choose-content-item">'+
	          "<div class='show-choose-title'>"+
                 "<div  class='show-choose-item-text'><span class='show-choose-item-text-dtl'>{{title}}</span>"+
                   "</div>"+
		         "<div class='show-choose-item-choose'>"+
                      "<div class='show-item-bz show-item-bz-left'>"+                      
                         "<label  v-on:click='gouHandler'>"+
                            "<span :id='getgouid'  class='show-pic-gou-span'>有</span>"+
                         "</label>"+
                      "</div>"+
                      "<div class='show-item-bz show-item-bz-last'>"+
                         "<label v-on:click='chaHandler' >"+
                            "<span :id='getchaid'  class='show-pic-gou-span'>无</span>"+ 
                            "</label>"+
                       "</div>"+
                 "</div>"+
              "</div>"+
			  "<div :id='getfilldiv' class='show-choose-fill item1'>"+
			    "<div class='show-choose-fill-item'>"+
			      "<div class='show-choose-fill-item-title fill-font-color'>"+
			        "<span class='show-choose-item-text-dtl'>{{fillitem1}}</span>"+
			      "</div>"+
			      "<div class='show-choose-fill-item-dtl'>"+
			        "<input type='text' v-on:click='showVisible()' readonly v-bind:value=sfshow />"+
			      "</div>"+
			    "</div>"+
			    "<div class='show-choose-fill-item'>"+
			      "<div class='show-choose-fill-item-title fill-font-color'>"+
			        "<span class='show-choose-item-text-dtl'>{{fillitem2}}</span>"+
			      "</div>"+
			      "<div class='show-choose-fill-item-dtl' v-bind:value=mvemo v-on:change='fillvemo()'>"+
			        "<input type='text' v-bind:value=mvemo v-on:change='fillvemo()' >"+
			      "</div>"+
			    "</div>"+
			  "</div>"+
			"</div>"

}




	vue = new Vue({
		el : '#index',
		//基础数据
		data : {
			dept : summer.pageParam.dept,
			module_id : summer.pageParam.module_id,
			module_title : summer.pageParam.module_title,
			record_id : parseInt(summer.pageParam.record_id),
			popupVisible : false,
			popupVisibleWeather : false,
			equips : null,
			shift:null,
			shifts : null,
			shiftVisible:false,
			shiftName:"",
			equipName : "",
			startDate : new Date(),
			endDate : new Date(),
			fillInDate : new Date().format("yyyy-MM-dd"),
			datePickerValue : new Date(),
			vemo:"",
            weather : "",
			weatherDescriptions : null,
			sfshowVisible:false,
			sfshowbz:0,
			sfshow:"",
			sfshows : null,
			qsfshowVisible:false,
			qsfshowbz:0,
			qsfshow:"",
            unitVisible:false,
			units:null,
			unit:"",
			qmp:0,
			qmpdtl:"",
			qmpvemo:"",
			xzp:0,
			xzpdtl:"",
			xzpvemo:"",
			lg:0,
			lgdtl:"",
			lgvemo:"",
			dz:0,
			dzdtl:"",
			dzvemo:"",
			rxd:0,
			rxddtl:"",
			rxdvemo:"",
			cxd:0,
			cxddtl:"",
			cxdvemo:"",
			gll:0,
			glldtl:"",
			gllvemo:"",
			ssf:0,
			ssfdtl:"",
			ssfvemo:"",
			sgj:0,
			sgjdtl:"",
			sgjvemo:"",
			dl:0,
			dldtl:"",
			dlvemo:"",
			bgj:0,
			bgjdtl:"",
			bgjvemo:"",
			lsg:0,
			lsgdtl:"",
			lsgvemo:"",
			gxg:0,
			gxgdtl:"",
			gxgvemo:"",
			gyp:0,
			gypdtl:"",
			gypvemo:"",
			fzq:0,
			fzqdtl:"",
			fzqvemo:"",
			fpw:0,
			fpwdtl:"",
			fpwvemo:"",
			zft:0,
			zftdtl:"",
			zftvemo:"",
			zft:0,
			zftdtl:"",
			zftvemo:"",
			by:0,
			bydtl:"",
			byvemo:"",
			qqsg:0,
			qqsgdtl:"",
			qqsgvemo:"",
			qxkj:0,
			qxkjdtl:"",
			qxkjvemo:"",
			checkman:"",
			
		},
		components:{
		  'componenttest':NewRadio,
		  'radiobtn':NewRadioBtn,
		 'newradiobtn':NewRadioChooseBtn

		},
		 watch : {

		},
		methods : {
		//泵房选择器方法onValueChange选择  confirmChange确认  open打开
			// picker view 滚动时触发
			onValueChange : function(picker, value) {
			},
			confirmChange : function() {
				this.equipName = this.$refs.equiPicker.getValues()[0]['text'];
				this.popupVisible = false;
			},
			open : function(picker) {
				this.$refs[picker].open();
			},
			onValueChangeShift:function(picker, value){			    
			},			
			confirmChangeShift:function() {
				this.shiftName = this.$refs.shiftPicker.getValues()[0]['text'];
				this.shiftVisible = false;
			},
			onValueChangeWeather : function(picker, value) {
			},
			confirmChangeWeather : function() {
				this.weather = this.$refs.weatherPicker.getValues()[0]['text'];
				this.popupVisibleWeather = false;
			},
			confirmChangeUnit:function(){
			    this.unit = this.$refs.unitPicker.getValues()[0]['text'];
				this.unitVisible = false;
			},
			onValueChangeUnit:function(picker, value){
			
			},
			onValueChangeSfshow : function(picker, value) {
			},
			confirmChangeSfshow : function() {
			    this.sfshowbz = this.$refs.sfshowPicker.getValues()[0]['value'];
			    this.sfshow = this.$refs.sfshowPicker.getValues()[0]['text'];
				this.sfshowVisible = false;
			},
			onValueChangeQsfshow : function(picker, value) {
			},
			confirmChangeQsfshow : function() {
			    this.qsfshowbz = this.$refs.qsfshowPicker.getValues()[0]['value'];
			    this.qsfshow = this.$refs.sfshowPicker.getValues()[0]['text'];
				this.qsfshowVisible = false;
			},
			//初始化设备选择器
			initEquips : function() {
				var readContent = summer.readFile("equips_bridge/pumpcheck.txt");
				if (readContent != null && readContent != "") {
					this.equips = [{
						values : ($summer.isJSONObject(readContent) ? readContent : JSON.parse(readContent))
					}];
				} else {
					her.loading("加载中，请稍候..");
					var param = {
						"type" : this.module_id
					};
					this.queryData(param);
				}
			},
			//请求后台,拉取泵房设备数据,放数组
			//查询基础数据
			queryData : function(param) {
				/*
				roads.ajaxRequest({
				type : "get",
				url : "/mob/mobAdd2",
				param : param,
				header : ""
				}, this.querySuccess, this.queryFail);*/

				////////////////////////////////////////////假装获取equips/////////////////////////////////////////
				var receive = [{
					"value" : "0",
					"text" : "胜利河水闸"
				}, {
					"value" : "1",
					"text" : "大关水闸"
				}, {
					"value" : "2",
					"text" : "小和山水闸"
				}];
				this.equips = [{
					values : receive
				}];
				summer.writeFile("equips_bridge/pumpcheck.txt", receive);
				her.loaded();
			},
			//初始化班次选择器
			initOptionshift : function() {
				this.shifts = [{
					values : [{
						"value" : "0",
						"text" : "上午"
					}, {
						"value" : "1",
						"text" : "下午"
					}]
				}];
			},
			//初始化养护单位
			initOptionUnit : function() {
				this.units = [{
					values : [{
						"value" : "0",
						"text" : "小单位"
					}, {
						"value" : "1",
						"text" : "大单位"
					}, {
						"value" : "2",
						"text" : "中单位"
					}]
				}];
			},
			//初始化养护单位
			initOptionsfshow : function() {
				this.sfshows = [{
					values : [{
						"value" : "1",
						"text" : "是"
					}, {
						"value" : "2",
						"text" : "否"
					}]
				}];
			},
			//初始化天气选择器
			initOptionsw : function() {
				this.weatherDescriptions = [{
					values : [{
						"value" : "0",
						"text" : "晴"
					}, {
						"value" : "1",
						"text" : "雨"
					}]
				}];
			},
			//查询成功
			querySuccess : function(data) {
				her.loaded();
				if (data.code == "1" || data.code == 1) {
					this.equips = [{
						values : data.msg
					}];
					summer.writeFile("equips_maintain/machineroom.txt", data.msg);
				} else if (data.code == "0" || data.code == 0) {
					UM.toast({
						"title" : "加载失败",
						"text" : data.msg,
						"duration" : 3000
					});
				}
			},
			//查询失败
			queryFail : function() {
				her.loaded();
				UM.toast({
					"title" : "加载失败",
					"text" : "请检查网络",
					"duration" : 3000
				});
			},
			//对日期控件进行范围限定，在mounted加载中使用
			//生成日期选择器选择范围
			dateRange : function(day) {
				var time = new Date();
				time.setDate(time.getDate() + day);
				var y = time.getFullYear();
				var m = time.getMonth() + 1;
				var d = time.getDate();
				return new Date(y + "-" + m + "-" + d);
			},
			//日期选择器确认方法
			handleDateConfirm : function(value) {
				this.fillInDate = new Date(value).format("yyyy-MM-dd");
			},
			
			setdtl:function(val){
			switch(val) {
				case "qmp":
                this.qmpdtl=event.target.value;
			    break;
			    case "xzp":
                this.xzpdtl=event.target.value;
			    break;
			    case "lg":
                this.lgdtl=event.target.value;
			    break;
			    case "dz":
                this.dzdtl=event.target.value;
			    break;
			    case "rxd":
                this.rxddtl=event.target.value;
			    break;
			    case "cxd":
                this.cxddtl=event.target.value;
			    break;
			    case "gll":
                this.glldtl=event.target.value;
			    break;
			    case "ssf":
                this.ssfdtl=event.target.value;
			    break;
			    case "sgj":
                this.sgjdtl=event.target.value;
			    break;
			    case "dl":
                this.dldtl=event.target.value;
			    break;
			    case "bgj":
                this.bgjdtl=event.target.value;
			    break;
			    case "lsg":
                this.lsgdtl=event.target.value;
			    break;
			    case "qmp":
                this.qmpdtl=event.target.value;
			    break;
			    case "gxg":
                this.gxgdtl=event.target.value;
			    break;
			    case "gyp":
                this.gypdtl=event.target.value;
			    break;
			    case "fzq":
                this.fzqdtl=event.target.value;
			    break;
			    case "fpw":
                this.fpwdtl=event.target.value;
			    break;
			    case "zft":
                this.zftdtl=event.target.value;
			    break;
			    case "by":
                this.bydtl=event.target.value;
			    break;
			    
				}
			
			},	
			setvemo:function(val){
				switch(val) {
				case "qmp":
                this.qmpvemo=event.target.value;
			    break;
			    case "xzp":
                this.xzpvemo=event.target.value;
			    break;
			    case "lg":
                this.lgvemo=event.target.value;
			    break;
			    case "dz":
                this.dzvemo=event.target.value;
			    break;
			    case "rxd":
                this.rxdvemo=event.target.value;
			    break;
			    case "cxd":
                this.cxdvemo=event.target.value;
			    break;
			    case "gll":
                this.gllvemo=event.target.value;
			    break;
			    case "ssf":
                this.ssfvemo=event.target.value;
			    break;
			    case "sgj":
                this.sgjvemo=event.target.value;
			    break;
			    case "dl":
                this.dlvemo=event.target.value;
			    break;
			    case "bgj":
                this.bgjvemo=event.target.value;
			    break;
			    case "lsg":
                this.lsgvemo=event.target.value;
			    break;
			    case "qmp":
                this.qmpvemo=event.target.value;
			    break;
			    case "gxg":
                this.gxgvemo=event.target.value;
			    break;
			    case "gyp":
                this.gypvemo=event.target.value;
			    break;
			    case "fzq":
                this.fzqvemo=event.target.value;
			    break;
			    case "fpw":
                this.fpwvemo=event.target.value;
			    break;
			    case "zft":
                this.zftvemo=event.target.value;
			    break;
			    case "by":
                this.byvemo=event.target.value;
			    break;
			    
				}
			
			},	
			
			mycha:function(val,newval) {
			switch(val) {
				case "qmp":
				//防止触发多次按到x,导致数据清空
				if(vue.qmp!=newval){
                vue.qmp=2;
                vue.qmpdtl="";
                vue.qmpvemo=""
                }
			    break;
			    case "xzp":
			    if(vue.xzp!=newval){
                vue.xzp=2;
                vue.xzpdtl="";
                vue.xzpvemo=""
                }
			    break;
			    case "lg":
			    if(vue.lg!=newval){
                vue.lg=2;
                vue.lgdtl="";
                vue.lgvemo=""
                }
			    break;
			    case "dz":
			    if(vue.dz!=newval){
                vue.dz=2;
                vue.dzdtl="";
                vue.dzvemo=""
                }
			    break;
			    case "rxd":
			    if(vue.rxd!=newval){
                vue.rxd=2;
                vue.rxddtl="";
                vue.rxdvemo=""
                }
			    break;
			    case "cxd":
			    if(vue.cxd!=newval){
                vue.cxd=2;
                vue.cxddtl="";
                vue.cxdvemo=""
                }
			    break;
			    case "gll":
			    if(vue.gll!=newval){
                vue.gll=2;
                vue.glldtl="";
                vue.gllvemo=""
                }
			    break;
			    case "ssf":
			    if(vue.ssf!=newval){
                vue.ssf=2;
                vue.ssfdtl="";
                vue.ssfvemo=""
                }
			    break;
			    case "sgj":
			    if(vue.sgj!=newval){
                vue.sgj=2;
                vue.sgjdtl="";
                vue.sgjvemo=""
                }
			    break;
			    case "dl":
			    if(vue.dl!=newval){
                vue.dl=2;
                vue.dldtl="";
                vue.dlvemo=""
                }
			    break;
			    case "bgj":
			    if(vue.bgj!=newval){
                vue.bgj=2;
                vue.bgjdtl="";
                vue.bgjvemo=""
                }
			    break;
			    case "lsg":
			    if(vue.lsg!=newval){
                vue.lsg=2;
                vue.lsgdtl="";
                vue.lsgvemo=""
                }
			    break;
			    case "gxg":
			    if(vue.gxg!=newval){
                vue.gxg=2;
                vue.gxgdtl="";
                vue.gxgvemo=""
                }
			    break;
			    case "gyp":
			    if(vue.gyp!=newval){
                vue.gyp=2;
                vue.gypdtl="";
                vue.gypvemo=""
                }
			    break;
			    case "fzq":
			    if(vue.fzq!=newval){
                vue.fzq=2;
                vue.fzqdtl="";
                vue.fzqvemo=""
                }
			    break;
			    case "fpw":
			    if(vue.fpw!=newval){
                vue.fpw=2;
                vue.fpwdtl="";
                vue.fpwvemo=""
                }
			    break;
			    case "zft":
			    if(vue.zft!=newval){
                vue.zft=2;
                vue.zftdtl="";
                vue.zftvemo=""
                }
			    break;			    
			    case "by":
                vue.by=2;
                vue.bydtl="";
                vue.byvemo=""
			    break;	
			    case "qqsg":
                vue.qqsg=2;
                vue.sfshowbz=0;
                vue.sfshow="";
                vue.qqsgdtl="";
                vue.qqsgvemo=""
			    break;
			    case "qxkj":
                vue.qxkj=2;
                vue.qxkjdtl="";
                vue.qxkjvemo=""
			    break;		  
				}
			},
           mygou:function(val,newval) {
				switch(val) {
				case "qmp":
                vue.qmp=1;
                vue.qmpdtl="";
                vue.qmpvemo=""
			    break;
			    case "xzp":
                vue.xzp=1;
                vue.xzpdtl="";
                vue.xzpvemo=""
			    break;
			    case "lg":
                vue.lg=1;
                vue.lgdtl="";
                vue.lgvemo=""
			    break;
			    case "dz":
                vue.dz=1;
                vue.dzdtl="";
                vue.dzvemo=""
			    break;
			    case "rxd":
                vue.rxd=1;
                vue.rxddtl="";
                vue.rxdvemo=""
			    break;
			    case "cxd":
                vue.cxd=1;
                vue.cxddtl="";
                vue.cxdvemo=""
			    break;
			    case "gll":
                vue.gll=1;
                vue.glldtl="";
                vue.gllvemo=""
			    break;
			    case "ssf":
                vue.ssf=1;
                vue.ssfdtl="";
                vue.ssfvemo=""
			    break;
			    case "sgj":
                vue.sgj=1;
                vue.sgjdtl="";
                vue.sgjvemo=""
			    break;
			    case "dl":
                vue.dl=1;
                vue.dldtl="";
                vue.dlvemo=""
			    break;
			    case "bgj":
                vue.bgj=1;
                vue.bgjdtl="";
                vue.bgjvemo=""
			    break;
			    case "lsg":
                vue.lsg=1;
                vue.lsgdtl="";
                vue.lsgvemo=""
			    break;
			    case "gxg":
                vue.gxg=1;
                vue.gxgdtl="";
                vue.gxgvemo=""
			    break;
			    case "gyp":
                vue.gyp=1;
                vue.gypdtl="";
                vue.gypvemo=""
			    break;
			    case "fzq":
                vue.fzq=1;
                vue.fzqdtl="";
                vue.fzqvemo=""
			    break;
			    case "fpw":
                vue.fpw=1;
                vue.fpwdtl="";
                vue.fpwvemo=""
			    break;
			    case "zft":
                vue.zft=1;
                vue.zftdtl="";
                vue.zftvemo=""
			    break;	
			    case "by":
			    if(vue.by!=1){
                vue.by=1;
                vue.bydtl="";
                vue.byvemo=""
                } 
			    break;
			    case "qqsg":
			    if(vue.qqsg!=1){
                vue.qqsg=1;
                vue.sfshowbz=1;
                vue.sfshow="是";
                vue.qqsgdtl="";
                vue.qqsgvemo=""
                }
			    break;
			    case "qxkj":
			    if(vue.qxkj!=1){
                vue.qxkj=1;
                vue.qsfshowbz=1;
                vue.qsfshow="是";
                vue.qxkjdtl="";
                vue.qxkjvemo=""
                }
			    break;	
				}
			},
			showTab:function(val){
			switch(val){
			case("qqsg"):
			this.sfshowVisible=true
			break;
			case("qxkj"):
			this.qsfshowVisible=true
			break;
			}
			 
			},
			// 返回事件
			goback : function() {
				roads.confirmClose();
			},	
			//暂存事件
			saveForm : function() {
			},
			// 提交表单
			submitForm : function() {
				var self = this;
				var data = JSON.parse(JSON.stringify(self.$data));
				console.log(data)
			
				self.$toast({
					message : data,
					position : "center",
					duration : 3000
				});
			}
			
		},				
		mounted : function() {
          // 加载数据...
			this.$nextTick(function() {
				//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//初始化数据库
				//this.initDB();
				//初始化报表
				//this.fillPage();
				//初始化设备选择器
				this.initEquips();
				//初始化上下午选择
				this.initOptionshift();
				//初始化天气选择器
				this.initOptionsw();
				//初始化单位
				this.initOptionUnit();
				this.initOptionsfshow();
				//初始化日期选择器选择范围:前后一个月？
				this.startDate = this.dateRange(-30);
				
				this.endDate = this.dateRange(30);
			})
          
		}
	})
}