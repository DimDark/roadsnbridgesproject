

summerready = function() {
	initPage();
}

var vue = null;

function initPage() {
	vue = new Vue({
		el : '#index',
		//基础数据
		data : {
			dept : summer.pageParam.dept,
			module_id : summer.pageParam.module_id,
			module_title : summer.pageParam.module_title,
			record_id : parseInt(summer.pageParam.record_id),
	        timePickerValue : null,
		     passTime : null,
             fircrt:null,
             firreading:null,

			
		},
		components:{
		},
		 watch : {

		},
		methods : {
		   onValueChange : function(picker, value) {
			},
			//时间选择器确认方法
			handleTimeConfirm : function(value) {
				this.passTime = value;
			},
			open : function(picker) {
				this.$refs[picker].open();
			},
		
			
			
		
			
	
			// 返回事件
			goback : function() {
				roads.confirmClose();
			},	
			//暂存事件
			saveForm : function() {
			},
			// 提交表单
			submitForm : function() {
				var self = this;
				var data = JSON.parse(JSON.stringify(self.$data));
				self.$toast({
					message : data,
					position : "center",
					duration : 3000
				});
			}
			
		},				
		mounted : function() {
          // 加载数据...
			this.$nextTick(function() {
				//监听返回按钮
				document.addEventListener("backbutton", this.goback, false);

				//初始化数据库
				//this.initDB();
				//初始化报表
				//this.fillPage();

			
			})
          
		}
	})
}