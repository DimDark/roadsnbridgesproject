summerready = function() {
	initList();
}
initList = function() {
	loadPage();
}
//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}
};

//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}

function fillZero(num, length) {
	var numstr = num.toString();
	numstr.replace(/\b(0+)/gi, "");
	numstr++;
	var l = numstr.toString().length;
	if (numstr.length >= length) {
		return numstr;
	}
	for (var i = 0; i < length - l; i++) {
		numstr = "0" + numstr.toString().trim();
	}
	return numstr;
}

//生成时间ID
function timeId() {
	var iter;
	var timeid = "";
	var myDate = new Date();
	var mytime = myDate.toLocaleDateString();
	if (isEmpty(summer.getStorage('iter'))) {
		summer.setStorage('iter', '001');
		timeid = mytime.toString().trim() + "i001";
	} else {
		iter = summer.getStorage('iter');
		iter = fillZero(iter, 3);
		summer.setStorage('iter', iter);
		timeid = mytime.toString().trim() + "i" + iter;
	}
	return timeid.replace(/\//g, "").toString().trim();
}

//加载页面
function loadPage() {
	var goboaTime = document.getElementById("goboat-time");
	var goboaType = document.getElementById("goboat-type");
	var goboatAmount = document.getElementById("goboat-amount");
	var switchTime = document.getElementById("switch-time");
	var goboatDirection = document.getElementById("goboat-direction");

	document.addEventListener("backbutton", onBackKeyDown, false);
	function onBackKeyDown() {
		roads.confirmClosenExec("qghd", "reloadPage", "");
	}

	var returnbtn = document.getElementById("returnbtn");
	$summer.addEvt(returnbtn, 'click', function() {
		onBackKeyDown();
	});

	//获取真id判别是新增还是修改
	var realid = summer.pageParam.realid;
	if (realid != "new") {
		//更新之前的过船信息
		//原表单填充
		var sql = "select * from riverbq where pk_row_id='" + realid + "'";
		var param = {
			"db" : 'hzlqzw.db', //数据库名称
			"sql" : sql, //查询条件
			"startIndex" : 0, //可选参数 起始记录索引号(含)
		}

		var list = summer.UMSqlite.query(param);
		var list1 = eval(list);

		if (list1 != null) {
			var obj = list1[0];
			if (!objIsEmpty(obj["pass_time"])) {
				goboaTime.value = obj["pass_time"];
			}
			if (!objIsEmpty(obj["pass_type"])) {
				goboaType.value = obj["pass_type"];
			}
			if (!objIsEmpty(obj["pass_num"])) {
				goboatAmount.value = obj["pass_num"];
			}
			if (!objIsEmpty(obj["switch_time"])) {
				switchTime.value = obj["switch_time"];
			}
			if (!objIsEmpty(obj["direction"])) {
				goboatDirection.value = obj["direction"];
			}
		}
	} else {
		//新增过船信息

	}

	var savebtn = document.getElementById("savebtn");
	var shift = summer.pageParam.shift;
	$summer.addEvt(savebtn, 'click', function() {
		var recordbz = 0;
		var sql = "";
		var gti = (!isEmpty(goboaTime.value)) ? (goboaTime.value) : "";
		var gty = (!isEmpty(goboaType.value)) ? (goboaType.value) : "";
		var ga = (!isEmpty(goboatAmount.value)) ? (goboatAmount.value) : "";
		var st = (!isEmpty(switchTime.value)) ? (switchTime.value) : "";
		var gd = (!isEmpty(goboatDirection.value)) ? (goboatDirection.value) : "";
		if (!isEmpty(goboaTime.value) && !isEmpty(goboaType.value) && !isEmpty(goboatAmount.value) && !isEmpty(switchTime.value) && !isEmpty(goboatDirection.value)) {
			recordbz = 1;
		}
		if (realid == "new") {
			//新增-->插入新数据
			//生成新的时间id作为真id
			sql = sql + "insert into goboatbq (pk_row_id,pass_time,pass_type,pass_num,switch_time,direction,shift,recordbz) values ('" + timeId() + "','" + gti + "','" + gty + "','" + ga + "','" + st + "','" + gd + "','" + shift + "','" + recordbz + "')";
		} else {
			//修改-->更新老数据
			sql = sql + "update goboatbq set pass_time='" + gti + "',pass_type='" + gty + "',pass_num='" + ga + "',switch_time='" + st + "',direction='" + gd + "',recordbz='" + recordbz + "' where pk_row_id='" + realid + "'";
		}

		var param = {
			"db" : 'hzlqzw.db',
			"sql" : sql
		}
		summer.UMSqlite.execSql(param);
		roads.execScript("goboat", "reloadPage", "");
		roads.closeWin();
	});
}

function timechushi() {
	var shift = summer.pageParam.shift;
	var choose = document.getElementById("goboat-time");
	var goboattime = []
	if (shift == "0" || shift == 0) {
		for (var j = 0; j < 8; j++) {
			for (var k = 0; k < 10; k++) {
				var ss = "0" + j + ":0" + k;
				goboattime.push(ss);
			}
			for (var k = 10; k < 60; k++) {
				var ss = "0" + j + ":" + k;
				goboattime.push(ss);
			}
		}
		for (var i = 0; i < morningtime.length; i++) {
			var newoption = document.createElement("option");
			newoption.value = goboattime[i];
			newoption.innerHTML = goboattime[i];
			choose.appendChild(newoption);
		}

	} else if (shift == "1" || shift == 1) {
		for (var j = 8; j < 16; j++) {
			if (j < 10) {
				for (var k = 0; k < 10; k++) {
					var ss = "0" + j + ":0" + k;
					goboattime.push(ss);
				}
				for (var k = 10; k < 60; k++) {
					var ss = "0" + j + ":" + k;
					goboattime.push(ss);
				}
			} else {
				for (var k = 0; k < 10; k++) {
					var ss = j + ":0" + k;
					goboattime.push(ss);
				}
				for (var k = 10; k < 60; k++) {
					var ss = j + ":" + k;
					goboattime.push(ss);
				}
			}
		}
		for (var i = 0; i < morningtime.length; i++) {
			var newoption = document.createElement("option");
			newoption.value = goboattime[i];
			newoption.innerHTML = goboattime[i];
			choose.appendChild(newoption);
		}

	} else if (shift == "2" || shift == 2) {
		for (var j = 16; j < 24; j++) {

			for (var k = 0; k < 10; k++) {
				var ss = j + ":0" + k;
				goboattime.push(ss);
			}
			for (var k = 10; k < 60; k++) {
				var ss = j + ":" + k;
				goboattime.push(ss);

			}
		}
		for (var i = 0; i < morningtime.length; i++) {
			var newoption = document.createElement("option");
			newoption.value = goboattime[i];
			newoption.innerHTML = goboattime[i];
			choose.appendChild(newoption);
		}

	}

}

