/*by zhuhy*/
summerready = function() {
	// 构造控件Vue实例
	var listview = new Vue({
		el : '#index',
		data : {
			listDatas : [],
			timeOutEvent : 0,
			touchStatus : 0
		},
		mounted : function() {
			this.initModule();
			this.initSlider();
		},
		methods : {
			//初始化模块列表
			initModule : function() {
				this.listDatas = [{
					"moduleTitle" : "机房巡检",
					"moduleID" : "computerroombq",
					"modulePath" : "jf/computerroombq.html"
				}, {
					"moduleTitle" : "泵站运行",
					"moduleID" : "bzyxbq",
					"modulePath" : "bz/bzyxbq.html"
				}, {
					"moduleTitle" : "船闸运行",
					"moduleID" : "boatbq",
					"modulePath" : "cz/boatbq.html"
				}, {
					"moduleTitle" : "水闸运行",
					"moduleID" : "lockbq",
					"modulePath" : "sz/lockbq.html"
				}, {
					"moduleTitle" : "区管河道运行",
					"moduleID" : "qghd",
					"modulePath" : "qghd/qghd.html"
				}, {
					"moduleTitle" : "道路巡查",
					"moduleID" : "dlxc",
					"modulePath" : ""
				}, {
					"moduleTitle" : "河道巡查问题",
					"moduleID" : "hdxc",
					"modulePath" : "hd/hdxc.html"
				}, {
					"moduleTitle" : "突发应急事件",
					"moduleID" : "events",
					"modulePath" : "tf/events.html"
				}, {
					"moduleTitle" : "车站及轨行区养护",
					"moduleID" : "specialbq",
					"modulePath" : "cztz/specialbq.html"
				}];
			},
			//初始化轮播
			initSlider : function() {
				var list = [{
					content : "../../img/bg1.jpg"
				}, {
					content : "../../img/bg2.jpg"
				}, {
					content : "../../img/bg3.png"
				}];
				var islider = new iSlider({
					type : 'pic',
					data : list,
					dom : document.getElementById("iSlider-wrapper"),
					isLooping : true,
					animateType : 'default',
					isAutoplay : true,
					animateTime : 800
				});
				//islider.addDot();
			},
			tapHold : function(index) {
				// 这里编辑长按列表项逻辑
				this.touchStatus = 1;
			},
			moveTapHold : function(index) {
				this.touchStatus = 0;
			},
			cancelTapHold : function(index) {
				// 取消长按
				if (this.touchStatus)
					roads.openWin("maintain", this.listDatas[index]["moduleID"] + "", this.listDatas[index]["modulePath"] + "", {});
				this.touchStatus = 0;
			},
			//前往暂存列表
			goCacheList : function() {
				roads.openWin("maintain", "cacheList", "cache/cacheList.html", {});
			}
		}
	});
}



function execSaveSucc(){
   UM.toast({
       "title" : "数据保存：",
       "text" : "数据保存成功",
       "duration" : 3000
   });
}

