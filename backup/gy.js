summerready = function() {
	var list = [{
		content : "../../img/bg1.jpg"
	}, {
		content : "../../img/bg2.jpg"
	}, {
		content : "../../img/bg3.png"
	}];
	var islider = new iSlider({
		type : 'pic',
		data : list,
		dom : document.getElementById("iSlider-wrapper"),
		isLooping : true,
		animateType : 'default',
		isAutoplay : true,
		animateTime : 800
	});
	islider.addDot();
	registerClick();
}

// 注册点击事件
function registerClick() {
	$(".module-bg").on("click", function() {
		var tag = $(this).attr("data-tag");
		switch(tag) {
			case "computerroombq": 
			openNewPage(tag, "html/maintain/jf/computerroombq.html");
			break;
			case "bzyxbq": 
			openNewPage(tag, "html/maintain/bz/bzyxbq.html");
			break;
			case "boatbq2": 
			openNewPage(tag, "html/maintain/cz/boatbq2.html");
			break;
			case "lockbq": 
			openNewPage(tag, "html/maintain/sz/lockbq.html");
			break;
			case "qghd": 
			openNewPage(tag, "html/maintain/qghd/qghd.html");
			break;
			case "dlxc": 
			//openNewPage(tag);
			break;
			case "riverroadxc": 
			openNewPage(tag, "html/maintain/hd/riverroadxc.html");
			break;
			case "events": 
			openNewPage(tag, "html/maintain/tf/events.html");
			break;
			case "specialpartmentbq": 
			openNewPage(tag, "html/maintain/cztz/specialpartmentbq.html");
			break;
		}
	});
}

function openNewPage(winId, url) {
	console.log(winId);
	summer.openWin({
        "id" : winId,
        "url" : url
    });
}

/*
 var data = [{
	"name" : "机房巡检表",
	"vemo" : "",
	"img" : "../../img/jf.png",
	"url" : "html/computerroombq.html",
	"id" : "computerroombq"
}, {
	"name" : "泵站运行日报表",
	"vemo" : "",
	"img" : "../../img/bz.png",
	"url" : "html/bzyxbq.html",
	"id" : "bzyxbq"
}, {
	"name" : "船闸运行日报表",
	"vemo" : "",
	"img" : "../../img/czhd.png",
	"url" : "html/boatbq2.html",
	"id" : "boatbq2"
}, {
	"name" : "水闸运行月报表",
	"vemo" : "",
	"img" : "../../img/sz.png",
	"url" : "html/lockbq.html",
	"id" : "lockbq"
}, {
	"name" : "区管河道运行",
	"vemo" : "台账范本",
	"img" : "../../img/hd.png",
	"url" : "html/riverroad.html",
	"id" : "riverroad"
}, {
	"name" : "道路巡查日报表",
	"vemo" : "",
	"img" : "../../img/xc.png",
	"url" : "html/roadxcbq.html",
	"id" : "roadxcbq"
}, {
	"name" : "河道巡查问题登记表",
	"vemo" : "",
	"img" : "../../img/hdjd.png",
	"url" : "html/riverroadxc.html",
	"id" : "riverroadxc"
}, {
	"name" : "突发应急事件记录表",
	"vemo" : "",
	"img" : "../../img/yj.png",
	"url" : "html/events.html",
	"id" : "events"
}, {
	"name" : "车站特殊部位及轨",
	"vemo" : "行区质量养护台账",
	"img" : "../../img/cz.png",
	"url" : "html/specialpartmentbq.html",
	"id" : "specialpartmentbq"
}];
 * */
