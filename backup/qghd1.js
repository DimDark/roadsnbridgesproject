summerready = function() {
	initList();

}
initList = function() {
	returnbtn();
	initDB();
	initPage();
}
//回退监听,本身的回退按钮
function returnbtn() {
	document.addEventListener("backbutton", onBackKeyDown, false);
	function onBackKeyDown() {
		roads.closeWin();
	}

	var returnbtn = document.getElementById("returnbtn");
	$summer.addEvt(returnbtn, 'click', function() {
		onBackKeyDown();
	});
}

//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}
};

//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}

//初始化数据库
function initDB() {
	var sql = "select * from sqlite_master where name='riverbq'";
	var param = {
		"db" : 'hzlqzw.db', //数据库名称
		"sql" : sql, //查询条件
		"startIndex" : 0, //可选参数 起始记录索引号(含)
	}
	var list = summer.UMSqlite.query(param);
	var list1 = eval(list);
	if (list1 != null && list1.length != 0) {
		// 数据库名
	} else {
		var sql = "create table riverbq(pk_row_id VARCHAR PRIMARY KEY,river_name VARCHAR,problem_value VARCHAR,problem_dtl VARCHAR,location VARCHAR,scene_pic_src VARCHAR,modify_plan INTEGER,modify_pic_src VARCHAR,recordbz INTEGER);"

		var param1 = {
			"db" : 'hzlqzw.db',
			"sql" : sql
		};
		summer.UMSqlite.execSql(param1);
	}
}

//数据模型
var riverListModel = function() {
	this.loadJson = function(items) {
		this.data = ko.observableArray(items);
	}.bind(this);
	this.addItem = function(items) {
		this.data.push(items);
	}.bind(this);
	this.removeAll = function() {
		this.data.removeAll();
	}.bind(this);
};

var rModel = new riverListModel();

var jsonArray;

//json分析处理
function jsonProcess() {
	var rlist = document.getElementById('riverlist');
	if (jsonArray != null && jsonArray.length != 0) {
		rlist.style.display = 'block';
		var myDate = new Date();
		var mytime = myDate.toLocaleDateString();
		mytime = mytime.toString().trim().replace(/\//g, "");
		$.each(jsonArray, function(i, obj) {
			//过滤此条信息如果过期
			if (((obj.pk_row_id).split("i"))[0] != mytime) {
				jsonArray.splice(i, 1);
			}
			//伪序号生成
			obj.pass_id = i + 1;
			//根据完成标志控制勾的显示与隐藏
			if (obj.recordbz == "1") {
				obj.recordbz = true;
			} else {
				obj.recordbz = false;
			}
		});
	} else {
		//隐藏列表如果为空
		rlist.style.display = 'none';
	}
}

function dbQuery() {
	var shift = summer.pageParam.str;
	var querysql = "select pk_row_id,pass_time,recordbz from riverbq";
	var queryparam = {
		"db" : 'hzlqzw.db', //数据库名称
		"sql" : querysql, //查询条件
		"startIndex" : 0, //可选参数 起始记录索引号(含)
	}
	var querylist = summer.UMSqlite.query(queryparam);
	jsonArray = eval(querylist);
}

function initPage() {
	dbQuery();
	jsonProcess();

	rModel.loadJson(jsonArray);
	ko.applyBindings(rModel);

	var rlist = UM.listview('#riverlist');

	//初始化列表项事件
	rlist.on("itemClick", function(sender, args) {
		var item = rModel.data()[args.rowIndex];

		roads.openWin("maintain", "qghddetail", "qghd/qghddetail.html", {
			"realid" : item["pk_row_id"]
		});
	});

	rlist.on("itemDelete", function(sender, args) {
		//这是可以编写行删除逻辑，参数sender即为当前列表实例对象，args对象有2个属性，即rowIndex(行索引)和$target(目标行的jquery对象)
		var item = rModel.data()[args.rowIndex];
		var querysql = "delete from riverbq where pk_row_id='" + item["pk_row_id"] + "'";
		var queryparam = {
			"db" : 'hzlqzw.db', //数据库名称
			"sql" : querysql, //查询条件
			"startIndex" : 0, //可选参数 起始记录索引号(含)
		}
		summer.UMSqlite.execSql(queryparam);
		args.$target.slideUp(500, function() {
			rModel.data.remove(item);
		});
	});

	rlist.on("itemSwipeLeft", function(sender, args) {
		//这里可以处理行左滑事件，参数sender即为当前列表实例对象，args对象有2个属性，即rowIndex(行索引)和$target(目标行的jquery对象)
		sender.showItemMenu(args.$target);
	});
}

function reloadPage() {
	dbQuery();
	jsonProcess();

	//重载model
	rModel.removeAll();
	$.each(jsonArray, function(i, obj) {
		rModel.addItem(obj);
	});
}

function divclick(event) {
	var shift = summer.pageParam.str;

	roads.openWin("maintain", "qghddetail", "qghd/qghddetail.html", {
		"realid" : "new"
	});
}