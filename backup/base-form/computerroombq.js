
summerready = function() {
	var vue = new Vue({
		el : '#index',
		data : {
			popupVisible : false,
			curdate : new Date().format('yyyy-MM-dd'),
			// picker view的数据源
			deps : [{
				values : ['人力资源部', '集团总工会', '财务部', '采购部', '开发部', '销售部', '服务部'],
				flex : 1,
				className : 'aaa',
				textAlign : 'center'
			}],
			crdep : "集团总工会",
			morningClass : true,
			fillInTime : "",
			deviceCheckMark : "发电机设备故障",
			hygieneMark : "涵洞有垃圾残留"
		},
		methods : {
			// picker view 滚动时触发
			onValueChange : function(picker, value) {
				if (value[0]) {
					picker.setSlotValue(1, value[0]);
					//console.log(picker + '\n' + '选中的值: ' + value);
					this.crdep = value[0];
				}
			},
			open : function(picker) {
				this.$refs[picker].open();
			},
			handleChange : function(value) {
				var self = this;
				self.fillInTime = value;
				//self.$toast({
				//	message : '已选择 ' + value.toString(),
				//	position : 'bottom'
				//});
			},
			// 返回
			goback : function() {
				roads.closeWin();
			},

			// 提交表单
			commitForm : function() {
				var self = this;
				var data = JSON.parse(JSON.stringify(self.$data));
				self.$toast({
					message : data,
					position : "center",
					duration : 3000
				});
			}
		},
		mounted : function() {
			// 加载数据...

		}
	})
}