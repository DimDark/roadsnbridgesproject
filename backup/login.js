summerready = function() {
	model.initEvent();
}
//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}

}

//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}

//假装跳转
function jump(index) {
	switch(index) {
	case 0:
		roads.openWin("menu", "menu", "menu.html", {"dept":"maintain"});
		break;
	case 1:
		roads.openWin("menu", "menu", "menu.html", {"dept":"bridges"});
		break;
	case 2:
		roads.openWin("menu", "menu", "menu.html", {"dept":"qingchun"});
		break;
	case 3:
		roads.openWin("menu", "menu", "menu.html", {"dept":"zizhi"});
		break;
	case 4:
		roads.openWin("cs", "cs", "cs.html", {});
		break;
	}
}

var model = {
	//事件初始化
	initEvent : function() {
		var loginbtn = $summer.byId("logincheckbtn");
		$summer.addEvt(loginbtn, 'click', function() {
			var user = $summer.byId("user_text").value;
			var pwd = $summer.byId("pwd_text").value;

			if ((!isEmpty(user)) && (!isEmpty(pwd))) {
				var log = {
					"loginname" : user,
					"pwd" : pwd
				};
				logincheck(log);
			} else {
				UM.alert("账号或者密码不能为空");
			}
		});

		Getdata();
		remembermima();
	}
}

function logincheck(s) {
	summer.writeConfig({
		"host" : "192.168.186.112", //MA主机地址192.168.186.119陆桥192.168.0.100
		"port" : "8088" //MA主机端口
	});
	summer.callAction({
		"appid" : "hzlqzw", //当前应用id，即config.xml配置文件中的应用ID
		"viewid" : "com.yonyou.hzlqzw.loginController", //后台带包名的Controller名
		"action" : "checklogin", //方法名
		"params" : s, //自定义参数
		"callback" : "mycallback()", //请求回来后执行的js方法
		"error" : "myerror()" //失败回调的js方法
	})

}

function mycallback(args) {
	var bz = $("#remember").is(":checked");
	if (bz) {
		var user = $summer.byId("user_text").value;
		var pwd = $summer.byId("pwd_text").value;
		var sql1 = "update usersave set login_name='" + user + "',pwd='" + pwd + "',choosebz='1' where pk_row_id='0' ";
		var param2 = {
			"db" : 'hzlqzw.db',
			"sql" : sql1
		}

		summer.UMSqlite.execSql(param2);
	} else {
		var sql2 = "update usersave set login_name='" + user + "',pwd='" + pwd + "',choosebz='0' where pk_row_id='0' ";
		var param3 = {
			"db" : 'hzlqzw.db',
			"sql" : sql2
		}
		summer.UMSqlite.execSql(param3);
	}
	var code = args.code;
	var msg = args.msg;
	if (code == "1") {
		summer.openWin({
			"id" : "gy",
			"url" : "html/gy.html",
			"pageParam" : args
		});
	} else {
		UM.alert(msg);
	}

}

function myerror(args) {
	UM.alert("未知异常");
}

function remembermima() {
	var sql1 = "select * from usersave where pk_row_id='0'";
	var param = {
		"db" : 'hzlqzw.db',
		"sql" : sql1,
		"startIndex" : 0, //可选参数 起始记录索引号(含)
		"endIndex" : 0
	}
	var list = summer.UMSqlite.query(param);
	var list1 = eval(list);

	if (list1 != null && list1.length != 0) {
		var obj = list1[0];
		if (!objIsEmpty(obj["choosebz"])) {
			var bz = obj["choosebz"];
			if (bz == "1" || bz == 1) {
				if (!objIsEmpty(obj["login_name"])) {
					var logname = obj["login_name"];
					$summer.byId("user_text").value = logname;
				}
				if (!objIsEmpty(obj["pwd"])) {
					var pwd = obj["pwd"];
					$summer.byId("pwd_text").value = pwd;
				}
				$("#remember").prop("checked", true);
			}

		}
	}

}

function Getdata() {
	//数据库
	var param = {
		db : 'hzlqzw.db'
	}
	var database = summer.UMSqlite.exist(param);
	if (!database) {
		//建表
		var sql = "CREATE TABLE usersave(pk_row_id INTEGER PRIMARY KEY AUTOINCREMENT, login_name VARCHAR, pwd VARCHAR, choosebz VARCHAR);";
		var param1 = {
			"db" : 'hzlqzw.db',
			"sql" : sql
		};
		//插入数据
		var sql1 = "INSERT INTO usersave (pk_row_id) VALUES('0');";
		var param2 = {
			"db" : 'hzlqzw.db',
			"sql" : sql1
		};
		summer.UMSqlite.openDB(param);
		summer.UMSqlite.execSql(param1);
		summer.UMSqlite.execSql(param2);
	} else {
		var sql = "select * from sqlite_master where name='usersave'";
		var param = {
			"db" : 'hzlqzw.db', //数据库名称
			"sql" : sql, //查询条件
			"startIndex" : 0, //可选参数 起始记录索引号(含)
			"endIndex" : 0 //可选参数 结束记录索引号(含)
		}
		var list = summer.UMSqlite.query(param);
		var list1 = eval(list);
		if (list1 != null && list1.length != 0) {
		} else {
			var sql = "CREATE TABLE usersave(pk_row_id INTEGER PRIMARY KEY AUTOINCREMENT, login_name VARCHAR, pwd VARCHAR, choosebz VARCHAR);";
			var param1 = {
				"db" : 'hzlqzw.db',
				"sql" : sql
			};
			//插入数据
			var sql1 = "INSERT INTO usersave (pk_row_id) VALUES('0');";
			var param2 = {
				"db" : 'hzlqzw.db',
				"sql" : sql1
			};
			summer.UMSqlite.execSql(param1);
			summer.UMSqlite.execSql(param2);
		}

	}

}

