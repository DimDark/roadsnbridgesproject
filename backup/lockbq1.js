summerready = function() {
	initVue();
	initDB();
}
//天气静态数据
var weatherchoose = [{
	"value" : "0",
	"text" : "晴"
}, {
	"value" : "1",
	"text" : "雨"
}];

function initVue() {
	var vue = new Vue({
		el : '#index',
		data : {
			popupVisible : false,
			curdate : new Date().format('yyyy-MM-dd'),

			//设备名称的数据源
			equips : null,
			equipValue : "",
			equipName : "",
			weatherVisible : false,
			weathers : [{
				values : weatherchoose
			}],
			weatherValue : "",
			weatherName : "",
			upwater : "",
			opendegree : "",
			downwater : "",
			vemo : "",
			ondutyperson : ""
		},
		methods : {
			// picker view 滚动时触发
			onValueChange : function(picker, value) {
			},
			//滚动器确认
			confirmChange : function() {
				this.equipValue = this.$refs.equiPicker.getValues()[0]['value'];
				this.equipName = this.$refs.equiPicker.getValues()[0]['text'];
				this.popupVisible = false;
			},
			//天气滚动器确认
			weatherconfirmChange : function() {
				this.weatherValue = this.$refs.weatherPicker.getValues()[0]['value'];
				this.weatherName = this.$refs.weatherPicker.getValues()[0]['text'];
				this.weatherVisible = false;
			},
			// 返回
			goback : function() {
				roads.confirmClose();
			},
			//初始化设备选择器
			initEquips : function() {
				////////////////////////////////////////////假装获取equips/////////////////////////////////////////
				this.equips = [{
					values : [{
						"value" : "0",
						"text" : "胜利河水闸"
					}, {
						"value" : "1",
						"text" : "大关水闸"
					}, {
						"value" : "2",
						"text" : "小和山水闸"
					}]
				}];
			},
			// 暂存表单stringify转为json字符串
			saveForm : function() {
				var data = this.$data;
				var self = this;
				savedata(self, data);
			},
			loadingTest : function() {
				her.loading("加载中，请稍候..");
			},
			loadedTest : function() {
				her.loaded();
			},
			test : function() {
				loading();
			},
			loadData : function() {
				this.loadbackground();
			},
			getFileData : function() {
				var test = summer.readFile("zdy/test.txt");
				if (test != null && test != "") {
					if ($summer.isJSONObject(test)) {
						data = test;
						var ss = data.msg;
						var jsonArray = [];
						for (var i = 0; i < ss.length; i++) {
							var s = {
								"value" : ss[i].value,
								"text" : ss[i].text
							};
							jsonArray.push(s);
						}
						this.equips = [{
							values : jsonArray
						}]
					} else {
						data = JSON.parse(test);
						var ss = data.msg;
						var jsonArray = [];
						for (var i = 0; i < ss.length; i++) {
							var s = {
								"value" : ss[i].value,
								"text" : ss[i].text
							};
							jsonArray.push(s);
						}
						this.equips = [{
							values : jsonArray
						}]
					}
				} else {
					this.loadbackground();
				}
			},
			loadbackground : function() {
				her.loading("加载中，请稍候..");
				var param = {
					"type" : "1"
				};
				this.askbgdata(param);
			},
			askbgdata : function(param) {
				roads.ajaxRequest({
					type : "get",
					url : "/mob/mobAdd2",
					param : param,
					header : ""
				}, this.succaction, this.falseaction);
			},
			succaction : function(data) {
				her.loaded();
				if (data.code == "1" || data.code == 1) {
					var ss = data.msg;
					var jsonArray = [];
					for (var i = 0; i < ss.length; i++) {
						var s = {
							"value" : ss[i].value,
							"text" : ss[i].text
						};
						jsonArray.push(s);

					}
					this.equips = [{
						values : jsonArray
					}]
					summer.writeFile("zdy/test.txt", data);
				} else if (data.code == "0" || data.code == 0) {

					UM.toast({
						"title" : "加载失败",
						"text" : data.msg,
						"duration" : 3000
					});
				}
			},
			falseaction : function() {
				her.loaded();
				UM.toast({
					"title" : "加载失败",
					"text" : "请检查网络",
					"duration" : 3000
				});
			},

			loadData1 : function() {

				var test = summer.readFile("zdy/test.txt")
				summer.toast({
					"msg" : test
				});
			},

			// 提交表单
			submitForm : function() {
				var self = this;
				var data = this.$data;
				UM.confirm({
					title : "提交数据",
					text : "确认提交该数据",
					btnText : ["取消", "确定"],
					overlay : true,
					duration : 2000,
					cancle : function() {
					},
					ok : function() {
						datasubmit(self, data);
					}
				});
			}
		},
		mounted : function() {
			// 加载数据...
			//先走初始化,再走得到数据
			this.initEquips();
			this.getFileData();

		}
	})
}

function initDB() {
	var sql = "select * from sqlite_master where name='lockbq'";
	var param = {
		"db" : 'hzlqzw.db', //数据库名称
		"sql" : sql, //查询条件
		"startIndex" : 0, //可选参数 起始记录索引号(含)
		"endIndex" : 0 //可选参数 结束记录索引号(含)
	}
	var list = summer.UMSqlite.query(param);
	var list1 = eval(list);
	if (list1 != null && list1.length != 0) {
	} else {
		//建表
		var sql1 = "CREATE TABLE lockbq (pk_row_id INTEGER PRIMARY KEY AUTOINCREMENT, equip_value VARCHAR, equip_text VARCHAR, fill_date VARCHAR," + "weather_value VARCHAR, weather VARCHAR, onduty_person VARCHAR, up_water VARCHAR,water_level VARCHAR,down_water VARCHAR,vemo VARCHAR )";
		var param2 = {
			"db" : 'hzlqzw.db',
			"sql" : sql1
		}
		summer.UMSqlite.execSql(param2);
	}
}

//判断是否存在json的key
function objIsEmpty(obj) {
	if (obj != undefined && obj != null && obj != "") {
		return false;
	} else {
		return true;
	}

};
//判断是否为空
function isEmpty(obj) {
	if ( typeof obj == "undefined" || obj == null || obj == "" || obj.trim() == "") {
		return true;
	} else {
		return false;
	}
}

function datasubmit(self, data) {
	var ev = data.equipValue;
	var en = data.equipName;
	var wv = data.weatherValue;
	var wn = data.weatherName;
	var uw = data.upwater;
	var dw = data.downwater;
	var wl = data.opendegree;
	var fd = data.curdate;
	var op = data.ondutyperson;
	var vemo = data.vemo;

	if (isEmpty(ev) || isEmpty(wv) || isEmpty(uw) || isEmpty(dw) || isEmpty(wl) || isEmpty(fd) || isEmpty(op)) {
		self.$toast({
			message : "除备注外其他信息都需要填写",
			position : "center",
			duration : 3000
		});
	} else {
		var param = {};
		param.ev = ev;
		param.en = en;
		param.wv = wv;
		param.wn = wn;
		param.uw = uw;
		param.dw = dw;
		param.wl = wl;
		param.fd = fd;
		param.op = op;
		param.type = op;
		param.vemo = vemo;
		askweb(param)
	}

}

function askweb(param) {
	roads.ajaxRequest({
		type : "post",
		url : "/mob/mobAdd1",
		param : param,
		header : ""
	}, zdysucc, zdyerr);
}

function zdysucc() {
	UM.alert("成功");
}

function zdyerr() {
	UM.alert("失败");
}

function savedata(self, data) {
	var ev = data.equipValue;
	var en = data.equipName;
	var wv = data.weatherValue;
	var wn = data.weatherName;
	var uw = data.upwater;
	var dw = data.downwater;
	var wl = data.opendegree;
	var fd = data.curdate;
	var op = data.ondutyperson;
	var vemo = data.vemo;

	if (isEmpty(ev)) {
		self.$toast({
			message : "请选择水闸名称",
			position : "center",
			duration : 3000
		});
	} else {
		her.loading("加载中，请稍候..");
		var sql = "select * from lockbq where equip_value='" + ev + "' and fill_date='" + fd + "'";
		var param = {
			"db" : 'hzlqzw.db', //数据库名称
			"sql" : sql, //查询条件

		}
		var list = summer.UMSqlite.query(param);
		var data = eval(list);
		if (data != null && data != "") {
			var sql1 = "update lockbq set equip_text='" + en + "',weather_value='" + wv + "',weather='" + wn + "',onduty_person='" + op + "',up_water='" + uw + "',water_level='" + wl + "',down_water='" + dw + "',vemo='" + vemo + "' where equip_value='" + ev + "' and fill_date='" + fd + "'  ";
			var param2 = {
				"db" : 'hzlqzw.db',
				"sql" : sql1
			}
			summer.UMSqlite.execSql(param2);
			roads.closeWinExec("menu", "execSaveSucc", "");
		} else {
			var sql1 = "insert into lockbq(equip_value,equip_text,fill_date,weather_value,weather,onduty_person,up_water,water_level,down_water,vemo)values('" + ev + "','" + en + "','" + fd + "','" + wv + "','" + wn + "','" + op + "','" + uw + "','" + wl + "','" + dw + "','" + vemo + "')";
			var param2 = {
				"db" : 'hzlqzw.db',
				"sql" : sql1
			}
			summer.UMSqlite.execSql(param2);
			var sql2 = "select * from modulestab where module_id='lockbq' ";
			var param3 = {
				"db" : 'hzlqzw.db',
				"sql" : sql2
			}
			var list1 = summer.UMSqlite.query(param3);
			var data1 = eval(list1);
			var record_num = data1.record_num + 1;
			var sql3 = "update modulestab set record_num='" + record_num + "' where module_id='lockbq'";
			var param4 = {
				"db" : 'hzlqzw.db',
				"sql" : sql3
			}
			summer.UMSqlite.execSql(param4);
			roads.closeWinExec("menu", "execSaveSucc", "");
		}
	}
}