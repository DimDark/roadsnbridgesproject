summerready = function() {
	// 构造控件Vue实例
	var listview = new Vue({
		el : '#index',
		data : {
			listDatas : [],
			timeOutEvent : 0,
			touchStatus : 0
		},
		mounted : function() {
			this.initModule();
			this.initSlider();
		},
		methods : {
			//初始化模块列表
			initModule : function() {
				this.listDatas = [{
					"moduleTitle" : "泵站巡查",
					"moduleID" : "qspumpcheck",
					"modulePath" : "qspumpcheck/qspumpcheck.html"
				}, {
					"moduleTitle" : "泵站生产",
					"moduleID" : "qspump",
					"modulePath" : "qspump/qspump.html"
				}, {
					"moduleTitle" : "桥梁巡检",
					"moduleID" : "qsbridge",
					"modulePath" : "qsbridge/qsbridge.html"
				}, {
					"moduleTitle" : "隧道巡检",
					"moduleID" : "qstunnel",
					"modulePath" : "qstunnel/qstunnel.html"
				}, {
					"moduleTitle" : "复兴大桥",
					"moduleID" : "qsbridgecontrol",
					"modulePath" : "qsbridgecontrol/qsbridgecontrol.html"
				}, {
					"moduleTitle" : "隧道养护运行",
					"moduleID" : "qstunnelcare",
					"modulePath" : "qstunnelcare/qstunnelcare.html"
				}, {
					"moduleTitle" : "地道巡查",
					"moduleID" : "qsdpavement",
					"modulePath" : "qsdpavement/qsdpavement.html"
				}, {
					"moduleTitle" : "天桥巡查",
					"moduleID" : "qsoverpass",
					"modulePath" : "qsoverpass/qsoverpass.html"
				}, {
					"moduleTitle" : "照明巡查",
					"moduleID" : "qslight",
					"modulePath" : "qslight/qslight.html"
				}];
			},
			//初始化轮播
			initSlider : function() {
				var list = [{
					content : "../../img/bg1.jpg"
				}, {
					content : "../../img/bg2.jpg"
				}, {
					content : "../../img/bg3.png"
				}];
				var islider = new iSlider({
					type : 'pic',
					data : list,
					dom : document.getElementById("iSlider-wrapper"),
					isLooping : true,
					animateType : 'default',
					isAutoplay : true,
					animateTime : 800
				});
				//islider.addDot();
			},
			tapHold : function(index) {
				// 这里编辑长按列表项逻辑
				this.touchStatus = 1;
			},
			moveTapHold : function(index) {
				this.touchStatus = 0;
			},
			cancelTapHold : function(index) {
				// 取消长按
				if (this.touchStatus)
					roads.openWin("bridges", this.listDatas[index]["moduleID"] + "", this.listDatas[index]["modulePath"] + "", {});
				this.touchStatus = 0;
			},
			//前往暂存列表
			goCacheList : function() {
			}
		}
	});
}